# Usage


## Classification


## Segmentation
- ImageNetS
- ADE20K
- COCO
- SA-1B

### ImageNetS
We provide the validation set of [ImageNetS](https://github.com/LUSSeg/ImageNet-S). Plese follow the [instructions](https://github.com/LUSSeg/ImageNet-S#imagenet-s-dataset-preparation) to prepare the dataset before using it.

```python
from datasets import ImageNetSValidation
dataset = ImageNetSValidation(root='/path/to/imagenets', subset='50')
img, labels_gt , boundary_gt = dataset[0]
```
### SA-1B
We support [SA-1B](https://). Download it through SA-1B Download [link](https://). 

```python
from datasets.sa1b import SA1BDataset
data = SA1BDataset('/vol/research/datasets/still/SA-1B')
# load a subset of the dataset
data=SA1BDataset('/vol/research/datasets/still/SA-1B',['sa_258805','sa_2601447','sa_6313499','sa_2592075','sa_258406','sa_2601844','sa_6353015','sa_10958302','sa_6344773'])
img, mask = data[0]
# SA-1B does not have meaningful class labels. 
# mask: [H, W, number of instances]
```

## Fewshot            
- miniImageNet
