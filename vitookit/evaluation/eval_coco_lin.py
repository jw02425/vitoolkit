#!python
#%%
"""
for img in img_list:
   %run bin/viz_clusters.py --arch vit_base --pretrained_weights ../SiT/outputs/imagenet/sit-ViT_B/checkpoint.pth --img ../data/ImageNet/val/{img} --cluster_number=10 --output_img=outputs/pics/clusters/{img}-sit-10.jpg
"""
from PIL import Image
import time
import argparse
import cv2
from sklearn.cluster import KMeans
import torch,math,os,torchvision,random, einops
from torch import nn
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import torch.nn.functional as F
import torchvision.transforms as tfms
import torchvision.transforms.functional as tfF
from torchvision.utils import *
from vitookit.utils.helper import load_pretrained_weights
import time
import vitookit.models

from torchvision import datasets

def get_args(*args,):
    parser = argparse.ArgumentParser(add_help="--arch=vit_base --pretrained_weight <model>.pth")
    parser.add_argument('--threshold',type=float,help="select the features over the threshold as positive.")
    
    parser.add_argument('--patch_size', default=16, type=int, help='Patch resolution of the model.')
    parser.add_argument("--image_size", default=(480, 480), type=int, nargs="+", help="Resize image.")
    parser.add_argument('--arch', default='vit_base', type=str, help='Architecture.')
    parser.add_argument('-w', '--pretrained_weights', default='', type=str, help="""Path to pretrained 
        weights to evaluate. Set to `download` to automatically load the pretrained DINO from url.
        Otherwise the model is randomly initialized""")
    parser.add_argument("--checkpoint_key", default=None, type=str,
        help='Key to use in the checkpoint (example: "teacher")')
    
    parser.add_argument('--select_num',default=5,type=int)
    parser.add_argument(
        '--output_dir',
        type=str,
        default='',
        help='output path')
    
    parser.add_argument('--mode',default='class',type=str,choices=['ins','class'])
    
    parser.add_argument("--acc_itr",default=100000,type=int,help="the number of batches to accumulate distributions. ")
    
    parser.add_argument('--use-cuda', action='store_true', default=True,
                        help='Use NVIDIA GPU acceleration')
    
    args = parser.parse_args(*args)
    
    args.use_cuda = args.use_cuda and torch.cuda.is_available()
    if args.use_cuda:
        print('Using GPU for acceleration')
    else:
        print('Using CPU for computation')
        
    return args

from pycocotools import mask as maskUtils


def annToRLE(ann, height, width):
    """
    Convert annotation which can be polygons, uncompressed RLE to RLE.
    :return: binary mask (numpy 2D array)
    """
    segm = ann['segmentation']
    if isinstance(segm, list):
        # polygon -- a single object might consist of multiple parts
        # we merge all parts into one mask rle code
        rles = maskUtils.frPyObjects(segm, height, width)
        rle = maskUtils.merge(rles)
    elif isinstance(segm['counts'], list):
        # uncompressed RLE
        rle = maskUtils.frPyObjects(segm, height, width)
    else:
        # rle
        rle = ann['segmentation']
    return rle

def annToMask( ann, height, width):
    """
    Convert annotation which can be polygons, uncompressed RLE, or RLE to binary mask.
    :return: binary mask (numpy 2D array)
    """
    rle = annToRLE(ann, height, width)
    m = maskUtils.decode(rle)
    return m

class COCO(datasets.CocoDetection):
    def __getitem__(self, index: int):
        img,annotations= super().__getitem__(index)
        
        width,height = img.size
        
        class_masks = [np.zeros(args.image_size)]
        class_set = set()
        for annotation in annotations:
            m = annToMask(annotation, height,
                                width)
            m = cv2.resize(m,args.image_size)
            class_id = annotation['category_id']
            
            class_masks.append(m*class_id)
            class_set.add(class_id)
        
        # merge the masks
        class_mask = np.stack(class_masks).max(0)
        
        return tfms(img), class_mask.astype(np.int32)

class FPN(nn.Module):
    def __init__(self,embed_dim,patch_size,num_classes):
        super().__init__()
        self.deconv = nn.ConvTranspose2d(embed_dim,num_classes,patch_size,stride=patch_size)
    
    def forward(self,x): # (B,196,C)
        B,N,C = x.shape
        h = w = int(math.sqrt(N))
        x = x.reshape(B,C,h,w)
        return self.deconv(x)

import numpy as np

if __name__ == '__main__':
    import sys
    args = get_args()
    ## load model
    model = models.__dict__[args.arch](
        patch_size=args.patch_size, 
        num_classes=0)
    embed_dim = model.embed_dim
    model.eval().requires_grad_(False)
    
    load_pretrained_weights(model, args.pretrained_weights, args.checkpoint_key)
    
    # fix random seeds for reproducibility
    random.seed(0)
    np.random.seed(0)
    torch.manual_seed(0)
    
    
    from torchvision import datasets, transforms
    IMAGENET_DEFAULT_MEAN = (0.485, 0.456, 0.406)
    IMAGENET_DEFAULT_STD = (0.229, 0.224, 0.225)
    
    common_tf = transforms.Compose([
        # transforms.CenterCrop(args.image_size),
        transforms.Resize(args.image_size),
        ])
    
    tfms=transforms.Compose([
        common_tf,
        transforms.ToTensor(),
        transforms.Normalize(IMAGENET_DEFAULT_MEAN, IMAGENET_DEFAULT_STD),
    ])
    def denormalize(imagesToPrint):
        imagesToPrint = imagesToPrint.clone()
        imagesToPrint *= torch.Tensor(IMAGENET_DEFAULT_STD,device=imagesToPrint.device).reshape(3,1,1)
        imagesToPrint += torch.Tensor(IMAGENET_DEFAULT_MEAN,device=imagesToPrint.device).reshape(3,1,1)
        return imagesToPrint.clamp(0,1)
    
    #%% ## get representations
    
    data = COCO('/vol/research/datasets/still/MSCOCO/images/train2017','/vol/research/datasets/still/MSCOCO/annotations/instances_train2017.json',)
    
    nh,hw = args.image_size[0]//args.patch_size, args.image_size[1]//args.patch_size
    
    dl = torch.utils.data.DataLoader(data,5,num_workers=10,shuffle=True,drop_last=True)
    
    #%% load 
    
    import tqdm
    fpn = FPN(model.embed_dim,args.patch_size,92).cuda()
    model.cuda()
    opt = torch.optim.Adam(fpn.parameters(),lr=1e-4)
    
    os.makedirs(args.output_dir,exist_ok=True)
    
    for epoch in range(12):
        pbar = tqdm.tqdm(dl)
        for x, class_mask in pbar:
            class_mask =  class_mask.numpy()
            
            patch_tokens = model.forward_features(x.cuda())[:,1:] # B, N - 1, C
            ## normalize
            patch_tokens_mean = patch_tokens.mean(1,keepdim=True)
            patch_tokens_std = patch_tokens.std(1,keepdim=True)
            patch_tokens = (patch_tokens - patch_tokens_mean) / patch_tokens_std
            
            patch_tokens = nn.functional.normalize(patch_tokens, dim=-1, p=2)
            
            preds = fpn(patch_tokens) # B, 92, H, W
            preds = preds.permute(0,2,3,1).reshape(-1,92) # BHW, 92
            loss = nn.functional.cross_entropy(preds,torch.from_numpy(class_mask.flatten()).long().cuda())
            
            ### optimize
            loss.backward()
            opt.step()
            opt.zero_grad()
            
            pbar.set_description(f"loss: {loss.item():.3f}")
        
        torch.save(fpn.state_dict(),args.output_dir+f"/fpn_{epoch}.pth")