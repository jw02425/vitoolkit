#!python
#%%
"""

"""
from PIL import Image
import time
import argparse
import cv2
from sklearn.cluster import KMeans
import torch,math,os,torchvision,random, einops
from torch import nn
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import torch.nn.functional as F
import torchvision.transforms as tfms
import torchvision.transforms.functional as tfF
from torchvision.utils import *
from vitookit.datasets.ade20k import ADE20KSegmentation
from vitookit.utils.helper import load_pretrained_weights
import time
import vitookit.models

from torchvision import datasets

def get_args(*args,):
    parser = argparse.ArgumentParser(add_help="--arch=vit_base --pretrained_weight <model>.pth")
    parser.add_argument('--threshold',type=float,help="select the features over the threshold as positive.")
    
    parser.add_argument('--patch_size', default=16, type=int, help='Patch resolution of the model.')
    parser.add_argument("--image_size", default=(480, 480), type=int, nargs="+", help="Resize image.")
    parser.add_argument('--arch', default='vit_base', type=str, help='Architecture.')
    parser.add_argument('-w', '--pretrained_weights', default='', type=str, help="""Path to pretrained 
        weights to evaluate. Set to `download` to automatically load the pretrained DINO from url.
        Otherwise the model is randomly initialized""")
    parser.add_argument("--checkpoint_key", default=None, type=str,
        help='Key to use in the checkpoint (example: "teacher")')
    
    parser.add_argument('--select_num',default=5,type=int)
    parser.add_argument(
        '--output_file',
        type=str,
        default='',
        help='output image path')
    
    parser.add_argument('--mode',default='class',type=str,choices=['ins','class'])
    
    parser.add_argument("--acc_itr",default=100000,type=int,help="the number of batches to accumulate distributions. ")
    
    parser.add_argument('--use-cuda', action='store_true', default=True,
                        help='Use NVIDIA GPU acceleration')
    
    args = parser.parse_args(*args)
    
    args.use_cuda = args.use_cuda and torch.cuda.is_available()
    if args.use_cuda:
        print('Using GPU for acceleration')
    else:
        print('Using CPU for computation')
        
    return args

from pycocotools import mask as maskUtils


def annToRLE(ann, height, width):
    """
    Convert annotation which can be polygons, uncompressed RLE to RLE.
    :return: binary mask (numpy 2D array)
    """
    segm = ann['segmentation']
    if isinstance(segm, list):
        # polygon -- a single object might consist of multiple parts
        # we merge all parts into one mask rle code
        rles = maskUtils.frPyObjects(segm, height, width)
        rle = maskUtils.merge(rles)
    elif isinstance(segm['counts'], list):
        # uncompressed RLE
        rle = maskUtils.frPyObjects(segm, height, width)
    else:
        # rle
        rle = ann['segmentation']
    return rle

def annToMask( ann, height, width):
    """
    Convert annotation which can be polygons, uncompressed RLE, or RLE to binary mask.
    :return: binary mask (numpy 2D array)
    """
    rle = annToRLE(ann, height, width)
    m = maskUtils.decode(rle)
    return m

class COCO(datasets.CocoDetection):
    def __getitem__(self, index: int):
        img,annotations= super().__getitem__(index)
        
        width,height = img.size
        
        instance_masks = [np.zeros((hw,nh))]
        class_masks = [np.zeros((hw,nh))]
        ins_set = set()
        class_set = set()
        for annotation in annotations:
            m = annToMask(annotation, height,
                                width)
            m = cv2.resize(m,(hw,nh))
            
            # Some objects are so small that they're less than 3 patches
            # and end up rounded out. Skip those objects.
            
            ins_id = annotation['id']
            class_id = annotation['category_id']
            # The  id  field is a unique identifier assigned to each individual object in the dataset. 
            # On the other hand,  category_id  is the identifier for the category or class that the object belongs to.
            
            instance_masks.append(m*ins_id)
            class_masks.append(m*class_id)
            ins_set.add(ins_id)
            class_set.add(class_id)
        
        # merge the masks
        instance_mask = np.stack(instance_masks).max(0)
        class_mask = np.stack(class_masks).max(0)
        
        return tfms(img), class_mask


import numpy as np


### DocArray: vector database
if __name__ == '__main__':
    import sys
    args = get_args()
    if os.path.exists(args.output_file):
        print("output file already exists.")
        sys.exit(0)
    ## load model
    model = models.__dict__[args.arch](
        patch_size=args.patch_size, 
        num_classes=0)
    embed_dim = model.embed_dim
    model.eval().requires_grad_(False)
    
    load_pretrained_weights(model, args.pretrained_weights, args.checkpoint_key)
    
    # fix random seeds for reproducibility
    random.seed(0)
    np.random.seed(0)
    torch.manual_seed(0)
    
    
    from torchvision import datasets, transforms
    IMAGENET_DEFAULT_MEAN = (0.485, 0.456, 0.406)
    IMAGENET_DEFAULT_STD = (0.229, 0.224, 0.225)
    
    common_tf = transforms.Compose([
        # transforms.CenterCrop(args.image_size),
        
        transforms.Resize(args.image_size),
        ])
    
    tfms=transforms.Compose([
        # common_tf,
        transforms.ToTensor(),
        transforms.Normalize(IMAGENET_DEFAULT_MEAN, IMAGENET_DEFAULT_STD),
    ])
    def denormalize(imagesToPrint):
        imagesToPrint = imagesToPrint.clone()
        imagesToPrint *= torch.Tensor(IMAGENET_DEFAULT_STD,device=imagesToPrint.device).reshape(3,1,1)
        imagesToPrint += torch.Tensor(IMAGENET_DEFAULT_MEAN,device=imagesToPrint.device).reshape(3,1,1)
        return imagesToPrint.clamp(0,1)
    
    #%% ## get representations
    
    # data = COCO('/vol/research/datasets/still/MSCOCO/images/train2017','/vol/research/datasets/still/MSCOCO/annotations/instances_train2017.json',)
    data = ADE20KSegmentation(dataset_root='/user/HS502/jw02425/gent/data', mode='train',transform=tfms,crop_size=args.image_size[0])
    nh,hw = args.image_size[0]//args.patch_size, args.image_size[1]//args.patch_size
    
    dl = torch.utils.data.DataLoader(data,32,num_workers=10,shuffle=True,drop_last=True)
    
    #%% load 
    
    import tqdm
    model.cuda()
    
    acc_itr = 0
    
    res_dict={i:dict(mIoU=[],intersection=[],union=[],prediction=[]) for i in range(args.select_num)}
    
    pbar = tqdm.tqdm(dl)
    # Stack multiple documents in a Document Vector
    
    for x, class_mask in pbar:
        acc_itr+=1
        class_mask = class_mask.numpy()# 32,480,480
        
        
        ## calculate similarity
        tokens = model.forward_features(x.cuda()) # B, N, C
        patch_tokens = tokens[:,1:] # B, N - 1, C
        cls_tokens = tokens[:,0] # B, C
        
        ## normalize
        patch_tokens_mean = patch_tokens.mean(1,keepdim=True)
        patch_tokens_std = patch_tokens.std(1,keepdim=True)
        patch_tokens = (patch_tokens - patch_tokens_mean) / patch_tokens_std
        
        patch_tokens = nn.functional.normalize(patch_tokens, dim=-1, p=2)
        
        p_sim = patch_tokens.matmul(patch_tokens.transpose(2,1)).cpu() # B, N, N
        B,N,_ = p_sim.shape
        
        num_itr = 0
        
        batch_dict={i:dict(mIoU=[],intersection=[],union=[],prediction=[]) for i in range(args.select_num)}
        
        for i in range(B):
            ## hist similarity for class-wise objects   
            masks = class_mask[i].flatten() # N
            id_set = np.unique(masks)
            for mask_id in id_set:
                if mask_id==0:continue
                mask = masks == mask_id
                indices = np.where(mask)[0]
                num = args.select_num
                if len(indices)<= num: continue
                
                random_indices = np.random.choice(len(indices), size=num, replace=False)
                
                predict_mask = np.zeros_like(mask)
                
                # randomly select one point and mark positive tokens
                for j in range(num):
                    # the index of query feature
                    q_idx = indices[random_indices[j]]
                    predict_mask[p_sim[i,q_idx]>args.threshold]=1
                
                    # Calculate the mIoU
                    intersection  = (predict_mask & mask) 
                    union = (predict_mask| mask)
                    mIoU = intersection.sum() / union.sum()
                    
                    batch_dict[j]['mIoU'].append(mIoU)
                    batch_dict[j]['intersection'].append(intersection.sum())
                    batch_dict[j]['union'].append(union.sum())
                    batch_dict[j]['prediction'].append(predict_mask.sum())
        # update batch dict
        # 
        for j in range(num):            
            for k in batch_dict[j]:
                res_dict[j][k].append(np.mean(batch_dict[j][k]))
            if j==0:
                pbar.set_description(f"mIoU: {res_dict[j]['mIoU'][-1]:.4f}")
        if acc_itr>args.acc_itr: break
    
    summary_dict = dict()
    for j in range(num):            
        for k in res_dict[j]:
            summary_dict[f"{j}/{k}"]=np.mean(res_dict[j][k])
    print("Final mIoU: ", summary_dict)
    if args.output_file:
        outdir = os.path.dirname(args.output_file)
        os.makedirs(outdir,exist_ok=True)
        import json
        with open(args.output_file, 'w') as file:
            json.dump(res_dict,file)
            file.write('\n')
            json.dump(summary_dict,file)
            file.write('\n')
        