# Copyright (c) ByteDance, Inc. and its affiliates.
# All rights reserved.
#
# This source code is licensed under the license found in the
# LICENSE file in the root directory of this source tree.

"""
Copy-paste from DINO library.
https://github.com/facebookresearch/dino
"""

import json
import os
from pathlib import Path
import sys
import pickle
import argparse
import torch
import torch.distributed as dist
import torch.backends.cudnn as cudnn
import numpy as np
from vitookit.utils.helper import load_pretrained_weights
from vitookit.utils.metric import compute_map
import vitookit.utils.misc as misc

from vitookit.models import vision_transformer as vits, vision_transformer_rel as vits_rel

from torch import nn
from PIL import Image, ImageFile

from torchvision import transforms as pth_transforms
from eval_knn import extract_features, ReturnIndexDatasetWrap

class OxfordParisDataset(torch.utils.data.Dataset):
    def __init__(self, dir_main, dataset, split, transform=None, imsize=None):
        if dataset not in ['roxford5k', 'rparis6k']:
            raise ValueError('Unknown dataset: {}!'.format(dataset))

        # loading imlist, qimlist, and gnd, in cfg as a dict
        gnd_fname = os.path.join(dir_main, dataset, 'gnd_{}.pkl'.format(dataset))
        with open(gnd_fname, 'rb') as f:
            cfg = pickle.load(f)
        cfg['gnd_fname'] = gnd_fname
        cfg['ext'] = '.jpg'
        cfg['qext'] = '.jpg'
        cfg['dir_data'] = os.path.join(dir_main, dataset)
        cfg['dir_images'] = os.path.join(cfg['dir_data'], 'jpg')
        cfg['n'] = len(cfg['imlist'])
        cfg['nq'] = len(cfg['qimlist'])
        cfg['im_fname'] = config_imname
        cfg['qim_fname'] = config_qimname
        cfg['dataset'] = dataset
        self.cfg = cfg

        self.samples = cfg["qimlist"] if split == "query" else cfg["imlist"]
        self.transform = transform
        self.imsize = imsize

    def __len__(self):
        return len(self.samples)

    def __getitem__(self, index):
        path = os.path.join(self.cfg["dir_images"], self.samples[index] + ".jpg")
        ImageFile.LOAD_TRUNCATED_IMAGES = True
        with open(path, 'rb') as f:
            img = Image.open(f)
            img = img.convert('RGB')
        if self.imsize is not None:
            img.thumbnail((self.imsize, self.imsize), Image.Resampling.LANCZOS)
        if self.transform is not None:
            img = self.transform(img)
        # img, label, index
        return img, index


def config_imname(cfg, i):
    return os.path.join(cfg['dir_images'], cfg['imlist'][i] + cfg['ext'])


def config_qimname(cfg, i):
    return os.path.join(cfg['dir_images'], cfg['qimlist'][i] + cfg['qext'])


if __name__ == '__main__':
    parser = argparse.ArgumentParser('Image Retrieval on revisited Paris and Oxford')
    parser.add_argument('--n_last_blocks', default=1, type=int, help="""Concatenate [CLS] tokens
        for the `n` last blocks. We use `n=1` all the time for k-NN evaluation.""")
    parser.add_argument('--head_type', default=0, choices=[0, 1, 2], type=int,
        help="""Whether or not to use global average pooled features or the [CLS] token.
        We typically set this to 1 for BEiT and 0 for models with [CLS] token (e.g., DINO).
        we set this to 2 for base-size models with [CLS] token when doing linear classification.""")
    parser.add_argument('--data_location', default='/path/to/revisited_paris_oxford/', type=str)
    parser.add_argument('--bn', default=False, action='store_true')
    parser.add_argument('--data_set', default='roxford5k', type=str, choices=['roxford5k', 'rparis6k'])
    parser.add_argument('--multiscale', default=False, action='store_true')
    parser.add_argument('--imsize', default=224, type=int, help='Image size')
    parser.add_argument('-w', '--pretrained_weights', default='', type=str, help="Path to pretrained weights to evaluate.")
    parser.add_argument('--use_cuda', default=True, action='store_true')
    parser.add_argument('--arch', default='vit_small', type=str, choices=['vit_tiny', 'vit_small', 'vit_base', 
        'vit_large'], help='Architecture.')
    parser.add_argument('--patch_size', default=16, type=int, help='Patch resolution of the model.')
    parser.add_argument("--checkpoint_key", default=None, type=str,
        help='Key to use in the checkpoint (example: "teacher")')
    parser.add_argument('--num_workers', default=10, type=int, help='Number of data loading workers per GPU.')
    parser.add_argument("--dist_url", default="env://", type=str, help="""url used to set up
        distributed training; see https://pytorch.org/docs/stable/distributed.html""")
    parser.add_argument("--local_rank", default=0, type=int, help="Please ignore and do not set this argument.")
    parser.add_argument("--output_dir",type=str,default="")
    
    # args = parser.parse_args(["--data_location","../../data/revisited_paris_oxford/","--arch=vit_base", "-w=../outputs/models/SiT/checkpoint.pth"])
    args = parser.parse_args()

    misc.init_distributed_mode(args)
    print("git:\n  {}\n".format(misc.get_sha()))
    print("\n".join("%s: %s" % (k, str(v)) for k, v in sorted(dict(vars(args)).items())))
    cudnn.benchmark = True

    # ============ preparing data ... ============
    transform = pth_transforms.Compose([
        pth_transforms.ToTensor(),
        pth_transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225)),
    ])
    dataset_train = OxfordParisDataset(args.data_location, args.data_set, split="train", transform=transform, imsize=args.imsize)
    dataset_query = OxfordParisDataset(args.data_location, args.data_set, split="query", transform=transform, imsize=args.imsize)
    sampler = torch.utils.data.DistributedSampler(dataset_train, shuffle=False)
    data_loader_train = torch.utils.data.DataLoader(
        ReturnIndexDatasetWrap(dataset_train),
        sampler=sampler,
        batch_size=1,
        num_workers=args.num_workers,
        pin_memory=True,
        drop_last=False,
    )
    data_loader_query = torch.utils.data.DataLoader(
        ReturnIndexDatasetWrap(dataset_query),
        batch_size=1,
        num_workers=args.num_workers,
        pin_memory=True,
        drop_last=False,
    )
    print(f"train: {len(dataset_train)} imgs / query: {len(dataset_query)} imgs")

    # ============ building network ... ============
    if 'vit_rel' in args.arch:
        model = vits_rel.__dict__[args.arch](
            patch_size=args.patch_size, 
            num_classes=0)
        load_pretrained_weights(model, args.pretrained_weights, args.checkpoint_key,interpolate=False)
    elif 'deit' in args.output_dir:
        import timm
        model=timm.create_model('deit_base_patch16_224', pretrained=True)
        model.head = nn.Identity()
    else:
        model = vits.__dict__[args.arch](
            patch_size=args.patch_size, 
            head_type=0,
            num_classes=0)
        
        load_pretrained_weights(model, args.pretrained_weights, args.checkpoint_key,interpolate=True)
        
    if args.use_cuda:
        model.cuda()
    model.eval().requires_grad_(False)

    ############################################################################
    # Step 1: extract features
    train_features,train_labels = extract_features(model, data_loader_train, args.use_cuda, multiscale=args.multiscale)
    query_features,val_labels = extract_features(model, data_loader_query, args.use_cuda, multiscale=args.multiscale)

    if misc.get_rank() == 0:  # only rank 0 will work from now on
        # bn
        bn = nn.BatchNorm1d(train_features.size(1),affine=False).to(train_features.device)
        if args.bn:
            train_features = bn(train_features)
            query_features = bn(query_features)
        # normalize features
        train_features = nn.functional.normalize(train_features, dim=1, p=2)
        query_features = nn.functional.normalize(query_features, dim=1, p=2)

        ############################################################################
        # Step 2: similarity
        sim = torch.mm(train_features, query_features.T)
        ranks = torch.argsort(-sim, dim=0).cpu().numpy()

        ############################################################################
        # Step 3: evaluate
        gnd = dataset_train.cfg['gnd']
        # evaluate ranks
        ks = [1, 5, 10]
        # search for easy & hard
        # note: how to evaluate?
        gnd_t = []
        for i in range(len(gnd)):
            g = {}
            g['ok'] = np.concatenate([gnd[i]['easy'], gnd[i]['hard']])
            g['junk'] = np.concatenate([gnd[i]['junk']])
            gnd_t.append(g)
        mapM, apsM, mprM, prsM = compute_map(ranks, gnd_t, ks)
        # search for hard
        gnd_t = []
        for i in range(len(gnd)):
            g = {}
            g['ok'] = np.concatenate([gnd[i]['hard']])
            g['junk'] = np.concatenate([gnd[i]['junk'], gnd[i]['easy']])
            gnd_t.append(g)
        mapH, apsH, mprH, prsH = compute_map(ranks, gnd_t, ks)
        print('>> {}: mAP M: {}, H: {}'.format(args.data_set, np.around(mapM*100, decimals=2), np.around(mapH*100, decimals=2)))
        print('>> {}: mP@k{} M: {}, H: {}'.format(args.data_set, np.array(ks), np.around(mprM*100, decimals=2), np.around(mprH*100, decimals=2)))
        log_stats = {
            'mAP/mapM':mapM,
            'mAP/mapH':mapH,
        }
        for i in range(len(ks)):
            log_stats[f"mP@k{ks[i]}/mprM"] = mprM[i]
            log_stats[f"mP@k{ks[i]}/mprH"] = mprH[i]
            
        print(log_stats)
        if args.output_dir:
            job_name = os.path.basename(__file__).split(".")[0] 
            metadata = {
                "job_name":job_name,
                "bn":args.bn,
                "multiscale":args.multiscale,
                "arch":args.arch,
                "head_type":args.head_type,
                "pretrained_weights":args.pretrained_weights,
                "data_set":args.data_set,
                "data_location":args.data_location,
            }
            log_stats.update(metadata)
            
            output_dir=Path(args.output_dir)
            output_dir.mkdir(parents=True, exist_ok=True)
            
            with (output_dir / "log.txt").open("a") as f:
                f.write(json.dumps(log_stats) + "\n") 
        
    dist.barrier()