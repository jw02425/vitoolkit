# https://github.com/yaoyao-liu/mini-imagenet-tools
# --------------------------------------------------------
from PIL import Image
import argparse
import datetime
import json
import sys
import numpy as np
import os
import time
from pathlib import Path

import torch
import torch.backends.cudnn as cudnn
import wandb

import vitookit.utils
from vitookit.datasets import build_dataset

import vitookit.models.vision_transformer as models_vit
from torchvision.transforms import *
from vitookit.utils.helper import load_pretrained_weights
import torch.nn.functional as F
from torch import nn
from timm.utils import accuracy

import learn2learn as l2l

def get_args_parser():
    parser = argparse.ArgumentParser('Fewshot classification based on threshold', add_help=False)
    parser.add_argument('--batch_size', default=1, type=int,
                        help='Batch size per GPU ')
    parser.add_argument('--num_tasks', default=600, type=int)
    # Model parameters
    parser.add_argument('--arch', default='vit_small', type=str, metavar='MODEL',
                        help='Name of model to train')
    
    parser.add_argument('--head_type', default=0, choices=[0, 1 ,2], type=int,
        help="""How to aggress global information.
        We typically set this to 0 for models with [CLS] token (e.g., DINO), 1 for models encouraging patch semantics e.g. BEiT, 2 for combining mean pool and CLS. 2works well for all cases. """)

    parser.add_argument('--input_size', default=224, type=int,
                        help='images input size')

    parser.add_argument('--drop_path', type=float, default=0, metavar='PCT',
                        help='Drop path rate (default: 0.1)')

    # Optimizer parameters
    parser.add_argument('--clip_grad', type=float, default=None, metavar='NORM',
                        help='Clip gradient norm (default: None, no clipping)')
    parser.add_argument('--weight_decay', type=float, default=0.0,
                        help='weight decay (default: 0.0)')

    parser.add_argument('--lr', type=float, default=None, metavar='LR',
                        help='learning rate (absolute lr)')
    parser.add_argument('--blr', type=float, default=0.001, metavar='LR',
                        help='base learning rate: absolute_lr = base_lr * total_batch_size / 256')

    parser.add_argument('--min_lr', type=float, default=0, metavar='LR',
                        help='lower lr bound for cyclic schedulers that hit 0')

    parser.add_argument('--warmup_epochs', type=int, default=5, metavar='N',
                        help='epochs to warmup LR')

    # Augmentation parameters
    parser.add_argument('--color_jitter', type=float, default=None, metavar='PCT',
                        help='Color jitter factor (enabled only when not using Auto/RandAug)')
    parser.add_argument('--aa', type=str, default='rand-m9-mstd0.5-inc1', metavar='NAME',
                        help='Use AutoAugment policy. "v0" or "original". " + "(default: rand-m9-mstd0.5-inc1)'),
    parser.add_argument('--smoothing', type=float, default=0.1,
                        help='Label smoothing (default: 0.1)')

    # * Random Erase params
    parser.add_argument('--reprob', type=float, default=0.25, metavar='PCT',
                        help='Random erase prob (default: 0.25)')
    parser.add_argument('--remode', type=str, default='pixel',
                        help='Random erase mode (default: "pixel")')
    parser.add_argument('--recount', type=int, default=1,
                        help='Random erase count (default: 1)')
    parser.add_argument('--resplit', action='store_true', default=False,
                        help='Do not random erase first (clean) augmentation split')

    

    # * Finetuning params
    parser.add_argument('-w', '--pretrained_weights', default='',
                        help='finetune from checkpoint')
   

    # Dataset parameters
    parser.add_argument('--data_set',default='Folder',type=str)
    parser.add_argument('--data_location', default='./data', type=str,
                        help='dataset path')
    parser.add_argument('-k','--k_shot',default=1,type=int)
    parser.add_argument('-n','--nb_classes', default=5, type=int,
                        help='number of the classification types')

    parser.add_argument('--output_dir', default='./outputs/debug',
                        help='path where to save, empty for no saving')

    parser.add_argument('--device', default='cuda',
                        help='device to use for training / testing')
    parser.add_argument('--seed', default=0, type=int)

    parser.add_argument('--start_epoch', default=0, type=int, metavar='N',
                        help='start epoch')
    
    parser.add_argument('--num_workers', default=1, type=int)
    parser.add_argument('--pin_mem', action='store_true',
                        help='Pin CPU memory in DataLoader for more efficient (sometimes) transfer to GPU.')
    parser.add_argument('--no_pin_mem', action='store_false', dest='pin_mem')
    parser.set_defaults(pin_mem=True)


    return parser


def main(args):

    print('job dir: {}'.format(os.path.dirname(os.path.realpath(__file__))))
    print("{}".format(args).replace(', ', ',\n'))

    device = torch.device(args.device)

    # fix the seed for reproducibility
    seed = args.seed 
    torch.manual_seed(seed)
    np.random.seed(seed)

    cudnn.benchmark = True
    
    
    train_transform = Compose([
        RandomResizedCrop(224),
        RandomHorizontalFlip(),
        ToTensor(),
        Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225)),
    ])
    dataset_train,nb_classes = build_dataset(is_train=True, args=args,trnsfrm=train_transform)
    
    if args.nb_classes is None:
        args.nb_classes = nb_classes
    print("Dataset = ", str(dataset_train))
    print("len(Dataset), nb_classes = ", len(dataset_train), nb_classes)
    print(f"{args.nb_classes}-way {args.k_shot}-shot")
    
    dataset = l2l.data.MetaDataset(dataset_train)
    transforms = [
        l2l.data.transforms.NWays(dataset, n=args.nb_classes),
        l2l.data.transforms.KShots(dataset, k=args.k_shot+1),
        l2l.data.transforms.LoadData(dataset),
    ]
    taskset = l2l.data.TaskDataset(dataset, transforms, num_tasks=args.num_tasks)

    data_loader_val = torch.utils.data.DataLoader(
        taskset, 
        batch_size=args.batch_size,
        num_workers=args.num_workers,
        pin_memory=args.pin_mem,
        drop_last=False
    )
    
    model = models_vit.__dict__[args.arch](
        head_type=args.head_type,
        num_classes=0, # remove head
    )
    model.to(device)
    load_pretrained_weights(model, args.pretrained_weights)
    
    n_parameters = sum(p.numel() for p in model.parameters() if p.requires_grad)

    # print("Model = %s" % str(model_without_ddp))
    print('number of params (M): %.2f' % (n_parameters / 1.e6))
 
    thresholds  = np.linspace(0.1,0.9,9)
    log_stats = evaluate(data_loader_val, model, device,nway=args.nb_classes, thresholds=thresholds)
    
    print(log_stats)
    
    if args.output_dir:
        job_name = os.path.basename(__file__).split(".")[0]
        metadata = {
            "job_name":job_name,
            "arch":args.arch,
            "head_type":args.head_type,
            "pretrained_weights":args.pretrained_weights,
            "data_set":args.data_set,
            "data_location":args.data_location,
            "k_shot":args.k_shot,
        }
        # add prefix for stats
        log_stats.update(metadata)
        
        output_dir=Path(args.output_dir)
        output_dir.mkdir(parents=True, exist_ok=True)
        with (output_dir / "log.txt").open("a") as f:
            f.write(json.dumps(log_stats) + "\n")


import einops
@torch.no_grad()
def evaluate(data_loader, model, device, nway,thresholds=[0.5,0.6,0.7,0.8,0.9]):

    metric_logger = misc.MetricLogger(delimiter="  ")
    header = 'Test:'

    # switch to evaluation mode
    model.eval()
    for batch in metric_logger.log_every(data_loader, 10, header):
        X,Y = batch
        batch_size = X.shape[0]
        
        X = einops.rearrange(X, 'b (n k) c h w -> (b n) k c h w', n=nway).to(device)
        query_images, support_images = X[:,:1], X[:,1:]
        Y = einops.rearrange(Y, 'b (n k) -> b n k', n=nway).to(device)
        query_labels, support_labels = Y[:,:,:1].float(), Y[:,:,1:].float()
        

        # compute output
        q_x = support_images.flatten(0,1)
        
        support_features = model(q_x)
        query_features = model(query_images.flatten(0,1))
        support_features, query_features = F.normalize(support_features,p=2,dim=-1), F.normalize(query_features,p=2,dim=-1)
        
        query_features = einops.rearrange(query_features, '(b n) d -> b n d', b=batch_size, n=nway)
        support_features = einops.rearrange(support_features, '(b n k) d -> b (n k) d', b=batch_size, n=nway)
        
        
        sim = (query_features@support_features.transpose(-1,-2)) # b n n*k
        
        match = (query_labels.reshape(batch_size,nway,1) == support_labels.reshape(batch_size,1,-1))
            
        for t in thresholds:
            preds = sim > t
            TP = torch.diagonal((preds & match),dim1=1,dim2=2).float().sum()
            precision = (TP/(preds.sum())).item() if preds.sum()>0 else 0
            recall = (TP/match.sum()).item()
            metric_logger.meters[f'precision@{t:.1f}'].update(precision, n=batch_size)
            metric_logger.meters[f'recall@{t:.1f}'].update(recall, n=batch_size)
            
        # find the nearest neighbor
        kshot=sim.shape[-1]//nway
        preds = einops.rearrange(sim,"b n (n1 k)->(b n k) n1",n1=nway).argmax(-1)
        label = torch.arange(nway)[:,None].to(device).repeat(batch_size,kshot).flatten()
        acc = (preds == label).float().mean().item()
        metric_logger.meters[f'accuracy'].update(acc, n=batch_size)

    return {k: meter.global_avg for k, meter in metric_logger.meters.items()}


if __name__ == '__main__':
    parser = get_args_parser()
    args = parser.parse_args()
    main(args)
