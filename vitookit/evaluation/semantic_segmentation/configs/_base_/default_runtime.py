# yapf:disable
log_config = dict(
    interval=50,
    hooks=[
        dict(type='TextLoggerHook', by_epoch=False),
        dict(type='TensorboardLoggerHook'),
        # dict(type='WandbLoggerHook',)
        #             #  init_kwargs={
        #             #      'entity': "erow",
        #             #      'project': "mmseg"
        #             #  },
        #             #  interval=100,
        #             #  log_checkpoint=True,
        #              log_checkpoint_metadata=True,
        #             #  num_eval_images=100,
        #             #  bbox_score_thr=0.3
        #              )
    ])
# yapf:enable
dist_params = dict(backend='nccl')
log_level = 'INFO'
load_from = None
resume_from = None
workflow = [('train', 1)]
cudnn_benchmark = True
