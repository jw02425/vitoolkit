# Copyright (c) ByteDance, Inc. and its affiliates.
# All rights reserved.
#
# This source code is licensed under the license found in the
# LICENSE file in the root directory of this source tree.

"""
Mostly copy-paste from beit, timm, mmseg, setr, xcit and swin code bases
https://github.com/microsoft/unilm/tree/master/beit
https://github.com/rwightman/pytorch-image-models/tree/master/timm
https://github.com/fudan-zvg/SETR
https://github.com/facebookresearch/xcit/
https://github.com/microsoft/Swin-Transformer
"""

_base_ = [
    '../_base_/datasets/ade20k.py',
    '../_base_/default_runtime.py', '../_base_/schedules/schedule_160k.py'
]
crop_size = (512, 512)
find_unused_parameters = True

norm_cfg = dict(type='SyncBN', requires_grad=True)
model = dict(
    type='EncoderDecoder',
    pretrained=None,
    backbone=dict(
        type='VisionTransformer',
        img_size=512,
        patch_size=16,
        embed_dim=768,
        depth=12,
        num_heads=12,
        mlp_ratio=4,
        qkv_bias=True,
        drop_path_rate=0.1,
        with_fpn=False,
        frozen_stages=12,
        out_indices=[5, 7, 9, 11]
    ),
    decode_head=dict(
        type='FCNHead',
        in_channels=[768, 768, 768, 768],
        in_index=[0, 1, 2, 3],
        input_transform='resize_concat',
        channels=3072,
        num_convs=0,
        concat_input=False,
        dropout_ratio=0,
        num_classes=150,
        norm_cfg=norm_cfg,
        align_corners=False,
        loss_decode=dict(
            type='CrossEntropyLoss', use_sigmoid=False, loss_weight=1.0)),
    test_cfg = dict(mode='slide', crop_size=crop_size, stride=(341, 341))
)



optimizer = dict(_delete_=True, type='AdamW', lr=5e-4, betas=(0.9, 0.999), weight_decay=0.05)

lr_config = dict(_delete_=True, policy='poly',
                 warmup='linear',
                 warmup_iters=1500,
                 warmup_ratio=1e-6,
                 power=1.0, min_lr=0.0, by_epoch=False)

# By default, models are trained on 8 GPUs with 2 images per GPU
data=dict(samples_per_gpu=4)


# do not use mmdet version fp16
fp16 = dict(loss_scale=512.)
optimizer_config = dict(
    type="Fp16OptimizerHook",
    loss_scale=512.
)

