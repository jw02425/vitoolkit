import math
from torch import nn
import torch
from vitookit.utils.misc import trunc_normal_
from .vision_transformer import *
from .sit import CLSHead


class iBOT(nn.Module):
    def __init__(self, backbone, out_dim=8196):
        super(iBOT, self).__init__()

        # backbone = eval(backbone)
        embed_dim = backbone.embed_dim
        backbone.fc, backbone.head = nn.Identity(), nn.Identity()
        self.backbone = backbone
        self.head = CLSHead(embed_dim,out_dim)

    def forward(self, x):
        g_out = self.backbone.forward_features(x)
        B,N,C = g_out.shape
        
        cls_token = g_out[:, 0]
        patch_tokens = g_out[:, 1:]
        
        return self.head(cls_token), self.head(patch_tokens.flatten(0,1)).reshape(B,N-1,-1)
    

if __name__ == '__main__':
    from timm.models import create_model
    model = create_model('vit_base_patch16_224', pretrained=True)
    model = iBOT(model)
    x = torch.randn(2, 3, 224, 224)
    y = model(x)
    print(y[0].shape, y[1].shape)