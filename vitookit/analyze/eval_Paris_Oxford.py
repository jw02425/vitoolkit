# Copyright (c) ByteDance, Inc. and its affiliates.
# All rights reserved.
#
# This source code is licensed under the license found in the
# LICENSE file in the root directory of this source tree.

"""
Copy-paste from DINO library.
https://github.com/facebookresearch/dino
"""

import json
import os
from pathlib import Path
import sys
import pickle
import argparse
import torch
import torch.distributed as dist
import torch.backends.cudnn as cudnn
import numpy as np
from vitookit.utils.helper import load_pretrained_weights
import vitookit.utils

import vitookit.models

from torch import nn
from PIL import Image, ImageFile
from torchvision import vitookit.models as torchvision_models
from torchvision import transforms as pth_transforms
from eval_knn import extract_features
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('Agg')

class OxfordParisDataset(torch.utils.data.Dataset):
    def __init__(self, dir_main, dataset, split, transform=None, imsize=None):
        if dataset not in ['roxford5k', 'rparis6k']:
            raise ValueError('Unknown dataset: {}!'.format(dataset))

        # loading imlist, qimlist, and gnd, in cfg as a dict
        gnd_fname = os.path.join(dir_main, dataset, 'gnd_{}.pkl'.format(dataset))
        with open(gnd_fname, 'rb') as f:
            cfg = pickle.load(f)
        cfg['gnd_fname'] = gnd_fname
        cfg['ext'] = '.jpg'
        cfg['qext'] = '.jpg'
        cfg['dir_data'] = os.path.join(dir_main, dataset)
        cfg['dir_images'] = os.path.join(cfg['dir_data'], 'jpg')
        cfg['n'] = len(cfg['imlist'])
        cfg['nq'] = len(cfg['qimlist'])
        cfg['im_fname'] = config_imname
        cfg['qim_fname'] = config_qimname
        cfg['dataset'] = dataset
        self.cfg = cfg

        self.samples = cfg["qimlist"] if split == "query" else cfg["imlist"]
        self.transform = transform
        self.imsize = imsize

    def __len__(self):
        return len(self.samples)

    def __getitem__(self, index):
        path = os.path.join(self.cfg["dir_images"], self.samples[index] + ".jpg")
        ImageFile.LOAD_TRUNCATED_IMAGES = True
        with open(path, 'rb') as f:
            img = Image.open(f)
            img = img.convert('RGB')
        if self.imsize is not None:
            img.thumbnail((self.imsize, self.imsize), Image.Resampling.LANCZOS)
        if self.transform is not None:
            img = self.transform(img)
        # img, label, index
        return img, index


def config_imname(cfg, i):
    return os.path.join(cfg['dir_images'], cfg['imlist'][i] + cfg['ext'])


def config_qimname(cfg, i):
    return os.path.join(cfg['dir_images'], cfg['qimlist'][i] + cfg['qext'])


if __name__ == '__main__':
    parser = argparse.ArgumentParser('Image Retrieval on revisited Paris and Oxford')
    parser.add_argument('--n_last_blocks', default=1, type=int, help="""Concatenate [CLS] tokens
        for the `n` last blocks. We use `n=1` all the time for k-NN evaluation.""")
    parser.add_argument('--head_type', default=0, choices=[0, 1, 2], type=int,
        help="""Whether or not to use global average pooled features or the [CLS] token.
        We typically set this to 1 for BEiT and 0 for models with [CLS] token (e.g., DINO).
        we set this to 2 for base-size models with [CLS] token when doing linear classification.""")
    parser.add_argument('--data_location', default='/path/to/revisited_paris_oxford/', type=str)
    parser.add_argument('-t', '--threshold', default=0.8, type=float, help='Threshold for positive class.')
    parser.add_argument('--dataset', default='roxford5k', type=str, choices=['roxford5k', 'rparis6k'])
    parser.add_argument('--multiscale', default=False, type=utils.bool_flag)
    parser.add_argument('--imsize', default=224, type=int, help='Image size')
    parser.add_argument('-w', '--pretrained_weights', default='', type=str, help="Path to pretrained weights to evaluate.")
    parser.add_argument('--use_cuda', default=True, type=utils.bool_flag)
    parser.add_argument('--arch', default='vit_small', type=str, choices=['vit_tiny', 'vit_small', 'vit_base', 
        'vit_large'], help='Architecture.')
    parser.add_argument('--patch_size', default=16, type=int, help='Patch resolution of the model.')
    parser.add_argument("--checkpoint_key", default=None, type=str,
        help='Key to use in the checkpoint (example: "teacher")')
    parser.add_argument('--num_workers', default=10, type=int, help='Number of data loading workers per GPU.')
    parser.add_argument("--dist_url", default="env://", type=str, help="""url used to set up
        distributed training; see https://pytorch.org/docs/stable/distributed.html""")
    parser.add_argument("--local_rank", default=0, type=int, help="Please ignore and do not set this argument.")
    parser.add_argument("--output_dir",type=str,default="")
    
    # args = parser.parse_args(["--data_location","../../data/revisited_paris_oxford/","--arch=vit_base", "-w=../outputs/models/SiT/checkpoint.pth"])
    args = parser.parse_args()

    misc.init_distributed_mode(args)
    print("git:\n  {}\n".format(utils.get_sha()))
    print("\n".join("%s: %s" % (k, str(v)) for k, v in sorted(dict(vars(args)).items())))
    cudnn.benchmark = True

    # ============ preparing data ... ============
    transform = pth_transforms.Compose([
        pth_transforms.ToTensor(),
        pth_transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225)),
    ])
    dataset_train = OxfordParisDataset(args.data_location, args.dataset, split="train", transform=transform, imsize=args.imsize)
    dataset_query = OxfordParisDataset(args.data_location, args.dataset, split="query", transform=transform, imsize=args.imsize)
    sampler = torch.utils.data.DistributedSampler(dataset_train, shuffle=False)
    data_loader_train = torch.utils.data.DataLoader(
        dataset_train,
        sampler=sampler,
        batch_size=10,
        num_workers=args.num_workers,
        pin_memory=True,
        drop_last=False,
    )
    data_loader_query = torch.utils.data.DataLoader(
        dataset_query,
        batch_size=10,
        num_workers=args.num_workers,
        pin_memory=True,
        drop_last=False,
    )
    print(f"train: {len(dataset_train)} imgs / query: {len(dataset_query)} imgs")

    ############################################################################
    # Step 1: extract features
    if os.path.exists(os.path.join(args.output_dir,"sim.pt")):
        print("sim.pt already exists, skip")
        sim = torch.load(os.path.join(args.output_dir,"sim.pt"))
        
    else:
        # ============ building network ... ============
        if "vit" in args.arch:
            model = models.__dict__[args.arch](
                patch_size=args.patch_size, 
                num_classes=0,
                head_type=args.head_type)
            print(f"Model {args.arch} {args.patch_size}x{args.patch_size} built.")
        elif "xcit" in args.arch:
            model = torch.hub.load('facebookresearch/xcit', args.arch, num_classes=0)
        elif args.arch in torchvision_models.__dict__.keys():
            model = torchvision_models.__dict__[args.arch](num_classes=0)
        else:
            print(f"Architecture {args.arch} non supported")
            sys.exit(1)
        if args.use_cuda:
            model.cuda()
        model.eval()

        # load pretrained weights
        load_pretrained_weights(model,args.pretrained_weights, args.checkpoint_key)

        train_features,train_labels = extract_features(model, data_loader_train, args.use_cuda, multiscale=args.multiscale)
        query_features,query_labels = extract_features(model, data_loader_query, args.use_cuda, multiscale=args.multiscale)
        # normalize features
        train_features = nn.functional.normalize(train_features, dim=1, p=2)
        query_features = nn.functional.normalize(query_features, dim=1, p=2)

        ############################################################################
        # Step 2: similarity
        sim = torch.mm(train_features, query_features.T)

    if misc.get_rank() == 0:  # only rank 0 will work from now on
        
        ############################################################################
        # Step 3: evaluate
        gnd = dataset_train.cfg['gnd']

        cat_dist = {"easy":[],"hard":[],"negative":[]}
        for cat in ["easy","hard"]:
            for i in range(len(gnd)):
                
                if cat == "easy": 
                    positive = gnd[i]["easy"] + gnd[i]["hard"] # list
                    negative = [i for i in range(len(gnd)) if i not in positive and i not in gnd[i]["junk"]]
                    cat_dist["negative"]+=(sim[negative,i].tolist())
                
                cat_dist[cat]+=(sim[gnd[i][cat],i].tolist())
        
        for cat in ["easy","hard"]:
            acc = 0
            pos_set=torch.Tensor(cat_dist[cat])
            neg_set=torch.Tensor(cat_dist["negative"])
            
            tp = torch.sum(pos_set>args.threshold)
            fn = torch.sum(neg_set<args.threshold)
            fp = torch.sum(neg_set>args.threshold)
            
            acc = (tp)/(tp+fp)
            recall = tp/(tp+fn)
            print(f"acc: {acc}, recall:{recall} for {cat}")
            
        if args.output_dir:
            import seaborn as sns
            positive = cat_dist["easy"] + cat_dist["hard"]
            negative = cat_dist["negative"]
            fig, ax = plt.subplots()
            sns.kdeplot(positive, fill=True, label='Positive')
            sns.kdeplot(negative, fill=True, label='Negative')
            ax.legend(loc='upper right')
            ax.set_title('Distribution of Scores')
            ax.set_xlabel('Score')
            ax.set_ylabel('Density')
            fig.savefig(os.path.join(args.output_dir,"dist.png"))
        
    dist.barrier()