#%%
import torch,math,os,torchvision,random, einops
from torch import nn
import pytorch_lightning as pl
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import torch.nn.functional as F
import torchvision.transforms as tfms
import torchvision.transforms.functional as tfF
from torchvision.utils import *

from sklearn.cluster import KMeans

from torchvision import datasets, transforms
from model import *

IMAGENET_DEFAULT_MEAN = (0.485, 0.456, 0.406)
IMAGENET_DEFAULT_STD = (0.229, 0.224, 0.225)
tfms=transforms.Compose([
    transforms.CenterCrop(224),
    transforms.ToTensor(),
    transforms.Normalize(IMAGENET_DEFAULT_MEAN, IMAGENET_DEFAULT_STD),
])
data=datasets.OxfordIIITPet('../../data','test',transform=tfms)

def denormalize(imagesToPrint):
    imagesToPrint = imagesToPrint.clone()
    imagesToPrint *= torch.Tensor(IMAGENET_DEFAULT_STD,device=imagesToPrint.device).reshape(3,1,1)
    imagesToPrint += torch.Tensor(IMAGENET_DEFAULT_MEAN,device=imagesToPrint.device).reshape(3,1,1)
    return imagesToPrint.clamp(0,1)

imgs = torch.stack([data[i][0] for i in range(10)])
aug_imgs = imgs.clone()
nn.init.normal_(aug_imgs[:,:,112:])
pic = make_grid(torch.cat([imgs,aug_imgs]),5)
tfF.to_pil_image(denormalize(pic))

# %%
from model_cl import *
model = vit_small()
model.requires_grad_(False)
ckpt = torch.load('../outputs/cl/BT-recon/checkpoint.pth')
state_dict = ckpt['model']

model.load_state_dict(state_dict,strict=False)
# model(imgs,imgs)
# %%
q = model.predictor(model.base_encoder(imgs)[1])
k = model.predictor(model.base_encoder(aug_imgs)[1])
q = nn.functional.normalize(q, dim=-1)
k = nn.functional.normalize(k, dim=-1)

sqq = torch.einsum('bnc,vmc->bvnm',q,q)
sqk = torch.einsum('bnc,vmc->bvnm',q,k)
model.contrastive_loss(q,k)
# %%
logits = torch.einsum('bnc,pnc->bnp', [q, k])
s_logit=logits.data.sort(-1)[0]
gap = s_logit[:,:,-1]-s_logit[:,:,-2]
sns.heatmap(sqk[:,:,0,0])
gap.mean()
# %%
ts = torch.linspace(0.1,2,10)
loss=[]
for t in ts:
    model.T=t
    loss.append(model.contrastive_loss(q,k).item())
plt.plot(ts,loss)
# %% BT
q = (model.backbone(imgs)[1])
k = (model.backbone(aug_imgs)[1])
q = nn.functional.normalize(q, dim=-1)
k = nn.functional.normalize(k, dim=-1)

sqq = torch.einsum('bnc,vmc->bvnm',q,q)
sqk = torch.einsum('bnc,vmc->bvnm',q,k)
(sqk.sum(1)-1).mean()
# %%

z1 = model.bn(model.projector(model.backbone(imgs)[1].flatten(0,1)))
z2 = model.bn(model.projector(model.backbone(aug_imgs)[1].flatten(0,1)))

s = torch.einsum('bc,bp->cp',z1,z1)
# %%
