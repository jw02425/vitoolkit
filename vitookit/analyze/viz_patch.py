#!/bin/env python
from pathlib import Path
from typing import Any, Callable, Optional
from PIL import Image
import time
import argparse
import cv2
from sklearn.cluster import KMeans
import torch,math,os,torchvision,random, einops
from torch import nn
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import torch.nn.functional as F
import torchvision.transforms as tfms
import torchvision.transforms.functional as tfF
from torchvision.utils import *
from vitookit.datasets.ade20k import ADE20KSegmentation
from vitookit.datasets.coco import COCO
from vitookit.utils.helper import load_pretrained_weights
import time
import vitookit.models

from torchvision import datasets

def get_args(*args,):
    parser = argparse.ArgumentParser(add_help="--arch=vit_base --pretrained_weight <model>.pth")
    parser.add_argument('--threshold',type=float,help="select the features over the threshold as positive.")
    parser.add_argument('--output_dir',type=str,help="output directory")
    parser.add_argument('--data_location',type=str,default='/user/HS502/jw02425/gent/data',help="data location")
    parser.add_argument('--data_set',type=str,default='COCO',help="data set")
    
    parser.add_argument('--patch_size', default=16, type=int, help='Patch resolution of the model.')
    parser.add_argument("--image_size", default=(480, 480), type=int, nargs="+", help="Resize image.")
    parser.add_argument('--arch', default='vit_base', type=str, help='Architecture.')
    parser.add_argument('-w', '--pretrained_weights', default='', type=str, help="""Path to pretrained 
        weights to evaluate. Set to `download` to automatically load the pretrained DINO from url.
        Otherwise the model is randomly initialized""")
    parser.add_argument("--checkpoint_key", default=None, type=str,
        help='Key to use in the checkpoint (example: "teacher")')
    
    parser.add_argument('--select_num',default=5,type=int)
    parser.add_argument(
        '--output_file',
        type=str,
        default='',
        help='output image path')
    
    parser.add_argument('--mode',default='class',type=str,choices=['ins','class'])
    
    parser.add_argument("--acc_itr",default=100000,type=int,help="the number of batches to accumulate distributions. ")
    
    parser.add_argument('--use-cuda', action='store_true', default=True,
                        help='Use NVIDIA GPU acceleration')
    
    args = parser.parse_args(*args)
    
    args.use_cuda = args.use_cuda and torch.cuda.is_available()
    if args.use_cuda:
        print('Using GPU for acceleration')
    else:
        print('Using CPU for computation')
        
    return args

### DocArray: vector database
if __name__ == '__main__':
    import sys
    args = get_args()
    if os.path.exists(args.output_file):
        print("output file already exists.")
        sys.exit(0)
    ## load model
    model = models.__dict__[args.arch](
        patch_size=args.patch_size, 
        num_classes=0)
    embed_dim = model.embed_dim
    model.eval().requires_grad_(False)
    
    load_pretrained_weights(model, args.pretrained_weights, args.checkpoint_key)
    
    # fix random seeds for reproducibility
    random.seed(0)
    np.random.seed(0)
    torch.manual_seed(0)
    
    
    from torchvision import datasets, transforms
    IMAGENET_DEFAULT_MEAN = (0.485, 0.456, 0.406)
    IMAGENET_DEFAULT_STD = (0.229, 0.224, 0.225)
    
    common_tf = transforms.Compose([
        # transforms.CenterCrop(args.image_size),
        transforms.Resize(args.image_size),
        ])
    
    tfms=transforms.Compose([
        common_tf,
        transforms.ToTensor(),
        transforms.Normalize(IMAGENET_DEFAULT_MEAN, IMAGENET_DEFAULT_STD),
    ])
    def denormalize(imagesToPrint):
        imagesToPrint = imagesToPrint.clone()
        imagesToPrint *= torch.Tensor(IMAGENET_DEFAULT_STD,device=imagesToPrint.device).reshape(3,1,1)
        imagesToPrint += torch.Tensor(IMAGENET_DEFAULT_MEAN,device=imagesToPrint.device).reshape(3,1,1)
        return imagesToPrint.clamp(0,1)
    
    #%% ## get representations
    nh,hw = args.image_size[0]//args.patch_size, args.image_size[1]//args.patch_size
    data = COCO(
        '/vol/research/datasets/still/MSCOCO/images/train2017',
                '/vol/research/datasets/still/MSCOCO/annotations/instances_train2017.json',transform=tfms)
    # data = ADE20KSegmentation(dataset_root='/user/HS502/jw02425/gent/data', mode='train',transform=tfms,crop_size=args.image_size[0])
    
    data[0]
    dl = torch.utils.data.DataLoader(data,32,num_workers=10,shuffle=True,drop_last=True)
    
    #%% load 
    
    import tqdm
    model.cuda()
    
    
    res_dict={i:dict(mIoU=[],intersection=[],union=[],prediction=[]) for i in range(args.select_num)}
    
    pbar = tqdm.tqdm(dl)
    # Stack multiple documents in a Document Vector

    embeddings = []
    labels=     []
    acc_itr = 0
    for x, class_mask in pbar:
        
        class_mask = class_mask.numpy()# 32,480,480
        tokens = model.forward_features(x.cuda()) # B, N, C
        tokens = F.normalize(tokens,dim=-1,p=2)
        patch_tokens = tokens[:,1:] # B, N - 1, C
        cls_tokens = tokens[:,0] # B, C
        B = len(x)
        for i in range(B):
            ## hist similarity for class-wise objects   
            masks = cv2.resize(class_mask[i],(hw,nh))
            
            id_set = np.unique(masks)
            for mask_id in id_set:
                if mask_id==0:continue
                mask = masks == mask_id
                mask = mask.flatten()
                if mask.sum()<10:continue
                embed = patch_tokens[i][mask]
                embeddings.append(embed.cpu().numpy())
                labels+=([int(mask_id)]*len(embed))
                acc_itr+=1
        if acc_itr>1000:break
    embeddings=np.concatenate(embeddings)
    labels = np.array(labels)
    
    
    from tsnecuda import TSNE
    import matplotlib.pyplot as plt
    embeddings_tsne = TSNE(n_components=2, perplexity=15, learning_rate=10).fit_transform(embeddings)

    # plot the t-SNE embeddings
    plt.scatter(embeddings_tsne[:, 0], embeddings_tsne[:, 1],c=labels)
    if args.output_dir:
        output_dir=Path(args.output_dir)
        output_dir.mkdir(parents=True, exist_ok=True)
        plt.savefig(output_dir/"tsne_pat.png")
    else:
        plt.show()
