# https://github.com/yaoyao-liu/mini-imagenet-tools
# --------------------------------------------------------

import argparse
import datetime
import json
import sys
from matplotlib import pyplot as plt
import numpy as np
import os
import time
from pathlib import Path

import torch
import torch.backends.cudnn as cudnn
import tqdm
import wandb

import vitookit.utils
from vitookit.datasets import build_dataset

import vitookit.models.vision_transformer as models_vit
from torchvision.transforms import *
from vitookit.utils.helper import load_pretrained_weights
import torch.nn.functional as F
from torch import nn
from timm.utils import accuracy


def get_args_parser():
    parser = argparse.ArgumentParser('Show the variance of features', add_help=False)
    parser.add_argument('--batch_size', default=6, type=int,
                        help='Batch size per GPU ')
    
    # Model parameters
    parser.add_argument('--arch', default='vit_base', type=str, metavar='MODEL',
                        help='Name of model to train')
    
    parser.add_argument('--head_type', default=0, choices=[0, 1 ,2], type=int,
        help="""How to aggress global information.
        We typically set this to 0 for models with [CLS] token (e.g., DINO), 1 for models encouraging patch semantics e.g. BEiT, 2 for combining mean pool and CLS. 2works well for all cases. """)

    parser.add_argument('--input_size', default=224, type=int,
                        help='images input size')

    parser.add_argument('--drop_path', type=float, default=0, metavar='PCT',
                        help='Drop path rate (default: 0.1)')

    # Optimizer parameters
    parser.add_argument('--clip_grad', type=float, default=None, metavar='NORM',
                        help='Clip gradient norm (default: None, no clipping)')
    parser.add_argument('--weight_decay', type=float, default=0.0,
                        help='weight decay (default: 0.0)')

    parser.add_argument('--lr', type=float, default=None, metavar='LR',
                        help='learning rate (absolute lr)')
    parser.add_argument('--blr', type=float, default=0.001, metavar='LR',
                        help='base learning rate: absolute_lr = base_lr * total_batch_size / 256')

    parser.add_argument('--min_lr', type=float, default=0, metavar='LR',
                        help='lower lr bound for cyclic schedulers that hit 0')

    parser.add_argument('--warmup_epochs', type=int, default=5, metavar='N',
                        help='epochs to warmup LR')

    # Augmentation parameters
    parser.add_argument('--color_jitter', type=float, default=None, metavar='PCT',
                        help='Color jitter factor (enabled only when not using Auto/RandAug)')
    parser.add_argument('--aa', type=str, default='rand-m9-mstd0.5-inc1', metavar='NAME',
                        help='Use AutoAugment policy. "v0" or "original". " + "(default: rand-m9-mstd0.5-inc1)'),
    parser.add_argument('--smoothing', type=float, default=0.1,
                        help='Label smoothing (default: 0.1)')

    # * Random Erase params
    parser.add_argument('--reprob', type=float, default=0.25, metavar='PCT',
                        help='Random erase prob (default: 0.25)')
    parser.add_argument('--remode', type=str, default='pixel',
                        help='Random erase mode (default: "pixel")')
    parser.add_argument('--recount', type=int, default=1,
                        help='Random erase count (default: 1)')
    parser.add_argument('--resplit', action='store_true', default=False,
                        help='Do not random erase first (clean) augmentation split')

    

    # * Finetuning params
    parser.add_argument('-w', '--pretrained_weights', default='',
                        help='finetune from checkpoint')
   

    # Dataset parameters
    parser.add_argument('--data_set',default='Folder',type=str)
    parser.add_argument('--data_location', default='./data', type=str,
                        help='dataset path')
    parser.add_argument('--nb_classes', default=None, type=int,
                        help='number of the classification types')

    parser.add_argument('--output_dir', default='./outputs/debug',
                        help='path where to save, empty for no saving')

    parser.add_argument('--device', default='cuda',
                        help='device to use for training / testing')
    parser.add_argument('--seed', default=0, type=int)

    parser.add_argument('--start_epoch', default=0, type=int, metavar='N',
                        help='start epoch')
    
    parser.add_argument('--num_workers', default=1, type=int)
    parser.add_argument('--pin_mem', action='store_true',
                        help='Pin CPU memory in DataLoader for more efficient (sometimes) transfer to GPU.')
    parser.add_argument('--no_pin_mem', action='store_false', dest='pin_mem')
    parser.set_defaults(pin_mem=True)


    return parser


def main(args):

    print('job dir: {}'.format(os.path.dirname(os.path.realpath(__file__))))
    print("{}".format(args).replace(', ', ',\n'))

    device = torch.device(args.device)

    # fix the seed for reproducibility
    seed = args.seed 
    torch.manual_seed(seed)
    np.random.seed(seed)

    cudnn.benchmark = True
    
    
    train_transform = Compose([
        RandomResizedCrop((224,224)),
        ToTensor(),
        Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225)),
    ])
    dataset_train,nb_classes = build_dataset(is_train=False, args=args,trnsfrm=train_transform)
    
    if args.nb_classes is None:
        args.nb_classes = nb_classes
    print("Dataset = ", str(dataset_train))
    print("len(Dataset), nb_classes = ", len(dataset_train), nb_classes)
    

    data_loader_val = torch.utils.data.DataLoader(
        dataset_train, 
        batch_size=args.batch_size,
        num_workers=args.num_workers,
        pin_memory=args.pin_mem,
        drop_last=False
    )
    
    model = models_vit.__dict__[args.arch](
        head_type=args.head_type,
        num_classes=0, # remove head
    )
    model.to(device)
    load_pretrained_weights(model, args.pretrained_weights)
    
    n_parameters = sum(p.numel() for p in model.parameters() if p.requires_grad)

    # print("Model = %s" % str(model_without_ddp))
    print('number of params (M): %.2f' % (n_parameters / 1.e6))
    model.requires_grad_(False)
    model.eval()
    
    embeddings = []
    labels = []
    metric_logger = misc.MetricLogger(delimiter="  ")
    
    job_name = os.path.basename(__file__).split(".")[0]
    header = job_name + ' :'
    for X,Y in metric_logger.log_every(data_loader_val, 100, header):
        tokens = model.forward_features(X.to(device))
        # tokens = F.normalize(tokens,dim=-1,p=2)
        embeddings.append(tokens[:,0].cpu().numpy())
        labels.append(Y.cpu().numpy())
    
    embeddings=np.concatenate(embeddings)
    labels = np.concatenate(labels)
    
    
    for i in range(10):
        s = labels == i
        z = embeddings[s]
        var = z.std(axis=0)
        N = var.shape[0]
        plt.plot(var,label=str(i),alpha=0.5)
    plt.ylim(embeddings.std(),None)
    plt.legend()
    if args.output_dir:
        output_dir=Path(args.output_dir)
        output_dir.mkdir(parents=True, exist_ok=True)
        plt.savefig(output_dir/"rep_variance.png")
    else:
        plt.show()

if __name__ == '__main__':
    parser = get_args_parser()
    args = parser.parse_args()
    main(args)
