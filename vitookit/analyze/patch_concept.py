#%% import 
import torch,math,os,torchvision,random, einops
from torch import nn
import pytorch_lightning as pl
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import torch.nn.functional as F
import torchvision.transforms as tfms
import torchvision.transforms.functional as tfF
from torchvision.utils import *



# %%
def requires_attn(model,flag):
    for name, m in model.named_modules():
        if 'attn' == name[-4:]:
            m.requires_attn=flag
            # print(f'set attn {flag} to {name}')
def unormalize(img):
    std = torch.tensor((0.229, 0.224, 0.225)).reshape(3,1,1)
    mean=torch.tensor((0.485, 0.456, 0.406)).reshape(3,1,1)
    return img * std + mean
# %%load dataset
from PIL import Image
from vitookit.datasets.datasets_utils import DataAugmentationSiT,GMML_drop_rand_patches
from torchvision.transforms import *
tfm = Compose([
    Resize(256),
    CenterCrop((224,224)),
    ToTensor(),
    Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225))]
)
img = Image.open('../imgs/sample1.JPEG')
img = tfm(img)
aug_img, im_mask = GMML_drop_rand_patches(img.clone(), 
                                        max_replace=0.5, drop_type='noise', align=1)


# %%%%%%%%%%%%%%%%%%%%%%%%%%% Load CSSL %%%%%%%%%%%%%%%%%%%%%%%%%%%
from main import *
from vision_transformer import vit_small
embed_dim=384
out_dim = 256
model = FullPipline(vit_small(),CLSHead(embed_dim, out_dim), CLSHead(embed_dim, out_dim),RECHead(embed_dim))
model.requires_grad_(False)
model_dir = '../outputs/imagenet100/cssl-500/checkpoint.pth'
state = torch.load(model_dir,'cpu')
model_state = {k.replace('module.',''):v for k,v in state['teacher'].items()}
model.load_state_dict(model_state)

#%%

pic_list = [aug_img[None,:]]
model.eval();
for i in range(1,12):
    # by delete
    # model = FullPipline(vit_small(),CLSHead(embed_dim, out_dim), CLSHead(embed_dim, out_dim),RECHead(embed_dim))
    # model.eval().requires_grad_(False)
    # model.load_state_dict(model_state)
    # del model.backbone.blocks[i:]
    requires_attn(model.backbone.blocks,False)
    requires_attn(model.backbone.blocks[:i],True)
    
    x_cls1,x_patch1,x_recon1 = model(aug_img[None,:])
    pic_list.append(x_recon1)

tfF.to_pil_image(unormalize(make_grid(torch.cat(pic_list),6, normalize=False,pad=1)).clamp(0,1))

#%%
recons = torch.cat(pic_list)
diff = recons[2:] - recons[1:-1]

diff1 = make_grid(diff,5,pad=1,pad_value=2)
plt.imshow(diff1.abs().mean(0))
plt.axis('off')
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%% Load ibot %%%%%%%%%%%%%%%%%%%%%%%%%%%
from vitookit.models.ibot import vits, FullPipline, iBOTHead
model = FullPipline(vits.vit_small(),iBOTHead(embed_dim, out_dim,)).requires_grad_(False)

model_dir = '../outputs/imagenet100/ibot-500/checkpoint.pth'
state = torch.load(model_dir,'cpu')
model_state = {k.replace('module.',''):v for k,v in state['teacher'].items()}
model.load_state_dict(model_state)

# %% rebuild images

imgs = torch.stack([data[i][0] for i in range(8)])
pic_list = [imgs]

requires_attn(model,False) # forward
output = model(imgs,return_backbone_feat=True)[0]

x_cls,x_patch = output.split([1,196],1)
# pic_list.append(x_recon)

requires_attn(model,True)# forward
output = model(imgs,return_backbone_feat=True)[0]
x_cls1,x_patch1 = output.split([1,196],1)

# pic_list.append(x_recon1)

tfF.to_pil_image(make_grid(torch.cat(pic_list),len(pic_list[0]), normalize=True))


# %%%%%%%%%%%%%%%%%%%%%%%%%%%%% Load MAE %%%%%%%%%%%%%%%%%%%%%%%%%%%
from vitookit.models.mae import vitookit.models_mae
model = models_mae['vit_small']()
model.eval()
model.requires_grad_(False)
model_dir = '../outputs/imagenet100/mae-500/checkpoint.pth'
state = torch.load(model_dir,'cpu')
model_state = {k.replace('module.',''):v for k,v in state['model'].items()}
model.load_state_dict(model_state)

# %% rebuild images
requires_attn(model,True)

mask_ratio = 0.5
pic_list = [img[None,:], ]

latent, mask, ids_restore = model.forward_encoder(img[None,:], mask_ratio)
x_patch = model.patchify(img[None,:])

pic_list.append(model.unpatchify(x_patch*mask.float().unsqueeze(2)))
for i in [0,4,8,11]:
    # by delete
    model = models_mae['vit_small']()
    model.eval().requires_grad_(False)
    model.load_state_dict(model_state)
    del model.decoder_blocks[i:]
    # requires_attn(model.decoder_blocks,False)
    # requires_attn(model.decoder_blocks[:i],True)
    
    pred = model.forward_decoder(latent, ids_restore)  # [N, L, p*p*3]
    x_recon = model.unpatchify(pred)
    print(model.forward_loss(img[None,:], pred, mask))
    pic_list.append(x_recon)

tfF.to_pil_image(unormalize(make_grid(torch.cat(pic_list),len(pic_list))))

#%% %%%%%%%%%%%how attention machenism helps to rebuild. %%%%%%%%%%%%%%%%%%%%%%%%%%%
fig,axes = plt.subplots(1,4,figsize=(15,3))
plt.tight_layout()
for i in range(4):
    sns.heatmap((x_recon[i]-x_recon1[i]).abs().mean(0),ax=axes[i])
    axes[i].axis('off')

# %% Sobel
def unormalize(img):
    std = torch.tensor((0.229, 0.224, 0.225)).reshape(3,1,1)
    mean=torch.tensor((0.485, 0.456, 0.406)).reshape(3,1,1)
    return img * std + mean
    
import cv2

fig,axes = plt.subplots(1,4,figsize=(15,3))
plt.tight_layout()
for i in range(4):
    img = (unormalize(imgs[i]).permute(1,2,0)*255).to(torch.uint8).numpy()
    # Convert to graycsale
    img_gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    # Blur the image for better edge detection
    img_blur = cv2.GaussianBlur(img_gray, (3,3), 0) 
    sobelx = cv2.Sobel(src=img_blur, ddepth=cv2.CV_64F, dx=1, dy=0, ksize=5) # Sobel Edge Detection on the X axis
    sobely = cv2.Sobel(src=img_blur, ddepth=cv2.CV_64F, dx=0, dy=1, ksize=5) # Sobel Edge Detection on the Y axis
    sobelxy = cv2.Sobel(src=img_blur, ddepth=cv2.CV_64F, dx=1, dy=1, ksize=5) # Combined X and Y Sobel Edge Detection

    ax = axes[i]
    sns.heatmap(sobelxy,ax=ax)
    ax.axis('off')


######### generate samples


from vitookit.datasets.load_dataset import build_dataset
from argparse import Namespace

data,_=build_dataset(Namespace(data_set='ImageFolder',
                             input_size=224,
                        data_location='../../data/ImageNet100'),False,tfm)
print([data[i][1] for i in range(64)])
pic_list = [data[i][0] for i in range(64)]
tfF.to_pil_image(make_grid(torch.stack(pic_list),8, normalize=True))
