#%%
from PIL import Image
import torch,math,os,torchvision,random, einops
from torch import nn
import pytorch_lightning as pl
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import torch.nn.functional as F
import torchvision.transforms as tfms
import torchvision.transforms.functional as tfF
from torchvision.utils import *

import vision_transformer as vits


#  %% load dataset
trfms = tfms.Compose([
    tfms.RandomResizedCrop(224, scale=(0.2, 1.), interpolation=Image.BICUBIC),
    tfms.ToTensor(),
    tfms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225)),
])
pets  = torchvision.datasets.OxfordIIITPet('../../data',transform=trfms)

pets_imgs = torch.stack([pets[0][0] for i in range(64)])

imagenet = torchvision.datasets.ImageFolder('../../data/ImageNet/train/',transform=trfms)
imagenet_imgs = torch.stack([imagenet[100][0] for i in range(64)])

tfF.to_pil_image(make_grid(torch.cat([pets_imgs,imagenet_imgs]),8,normalize=True))

#%% set up model

model = vits.vit_base().cuda()
model.requires_grad_(False)

class Hook:
    def __init__(self,model,module='mlp.fc2') -> None:
        self.model = model
        self.module = module
    
    
    def register_hook(self):
        for name, m in self.model.named_modules():
            if self.module in name:
                yield m.register_forward_hook(self.hook)
    
    def hook(self,m,input,output):
        self.outputs.append(output)
        
    def __call__(self, *args,**kwargs):
        self.outputs=[]
        hook_handlers = list(self.register_hook())
        out=self.model(*args,**kwargs)
        for h in hook_handlers: h.remove()
        
        return out,self.outputs
    
hook = Hook(model)
 
_,outputs=hook(imagenet_imgs.cuda())

# %% load model

models_path=[os.path.join('../../saved_models',f) for f in ["MCGMML_base_patch16_224-1K.pth", "mae_pretrain_vit_base.pth", "deit_base_patch16_224-b5f2ef4d.pth",'dino_vitbase16_pretrain.pth']]



for p in models_path:
    name = p.split('/')[-1].split('_')[0]
    
    state = torch.load(p,'cuda')
    for k in ['model','student','SiT_model']:
        if k in state:
            state = state[k]
    state = {k.replace('backbone.',''):v for k,v in state.items()}
    
    msg =  model.load_state_dict(state,False)
    print(p,msg.missing_keys, set([ i.split('.')[0] for i in state.keys() ]))
    _,outputs=hook(imagenet_imgs.cuda())
    plt.plot([i.mean(1).std(0).mean().item() for i in outputs],label=name)
plt.legend()
    
# %%

cls_token = outputs[-1][0]
#%% MAE

state = torch.load(models_path[1],'cuda')
for k in ['model','student','SiT_model']:
    if k in state:
        state = state[k]
state = {k.replace('backbone.',''):v for k,v in state.items()}
msg =  model.load_state_dict(state,False)
    
x=model.prepare_tokens(imagenet_imgs[:1].repeat(64,1,1,1).cuda())
# drop 
selects = [[0]+(torch.randperm(196)[:49]+1).tolist() for i in range(64)]
sx = []
for i in range(64):
    sx.append(x[i][selects[i]])
out=torch.stack(sx)

# hook
hook.outputs=[]
hook_handlers = list(hook.register_hook())
for b in model.blocks:
    out = b(out)
for h in hook_handlers: h.remove()

plt.plot([i.std(2).std(0)[0].item() for i in hook.outputs],label=name)

#%% GMML

state = torch.load(models_path[0],'cuda')
for k in ['model','student','SiT_model']:
    if k in state:
        state = state[k]
state = {k.replace('backbone.',''):v for k,v in state.items()}
msg =  model.load_state_dict(state,False)

from vitookit.datasets.datasets_utils import GMML_drop_rand_patches
x = torch.stack([GMML_drop_rand_patches(imagenet_imgs[0].clone())[0] for i in range(64)])
_,outputs=hook(x.cuda())

plt.plot([i.std([0])[1:].mean().item() for i in outputs],label=name)
# %%
