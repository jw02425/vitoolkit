# Copyright (c) ByteDance, Inc. and its affiliates.
# All rights reserved.
#
# This source code is licensed under the license found in the
# LICENSE file in the root directory of this source tree.

"""
Copy-paste from DINO library:
https://github.com/facebookresearch/dino
"""

import os
import argparse
import cv2
from PIL import Image
import random
import colorsys
import matplotlib
import matplotlib.pyplot as plt
import torch
import torch.nn as nn
import torchvision
import numpy as np
import vitookit.utils
import vitookit.models


from skimage.measure import find_contours
from matplotlib.patches import Polygon
from torch.utils.data import DataLoader
from torchvision import transforms as pth_transforms
from vitookit.datasets import InstanceWraper
from tqdm import tqdm

# matplotlib.use('Agg')

company_colors = [
    (0,160,215), # blue
    (220,55,60), # red
    (245,180,0), # yellow
    (10,120,190), # navy
    (40,150,100), # green
    (135,75,145), # purple
]
company_colors = [(float(c[0]) / 255.0, float(c[1]) / 255.0, float(c[2]) / 255.0) for c in company_colors]

mapping = {
    2642: (0, 1, 2),
    700: (4, 5),
    1837: (2, 4),
    1935: (0, 1, 4),
    2177: (0,1,2),
    3017: (0,1,2),
    3224: (1, 2, 5),
    3340: (2, 1, 3),
    3425: (0, 1, 2),
    1954: (0,1,2),
    2032: (4,0,5),
    3272: (0,1,2),
    3425: (0,1,2),
    3695: (1,5),
    1791:(1,2,5),
    385 : (1, 5),
    4002: (0,1,2,3)
}

def random_colors(N, bright=True):
    """
    Generate random colors.
    """
    brightness = 1.0 if bright else 0.7
    hsv = [(i / N, 1, brightness) for i in range(N)]
    colors = list(map(lambda c: colorsys.hsv_to_rgb(*c), hsv))
    random.shuffle(colors)
    return colors


def apply_mask(image, mask, color, alpha=0.5):
    """
    Apply a mask to an image by replacing the masked area with a specified color.

    Parameters:
        image: a numpy array representing an image
        mask: a numpy array representing a binary mask of the same shape as the image
        color: a tuple representing the RGB color to replace the masked area with
        alpha: an optional float representing the transparency of the mask (default is 0.5)
    Returns:
        a numpy array representing the masked image with the specified color
    """
    for c in range(3):
        image[:, :, c] = image[:, :, c] * (1 - alpha * mask) + alpha * mask * color[c] * 255
    return image



def apply_mask(image, mask, color, alpha=0.5, threshold=0.2):
    """
    Apply a mask to an image.
    
    Parameters:
        image (numpy.ndarray): Input image.
        mask (numpy.ndarray): Mask to apply on the image.
        color (tuple): RGB color tuple.
        alpha (float): Tracks the transparency of the color.
        threshold (float): Values above this threshold won't be affected by the masking
        
    Returns:
        numpy.ndarray: Masked image.
    """
    mask = (mask - mask.min()) / (mask.max() - mask.min()) # Normalize the mask.
    
    mask= np.sqrt(mask) # Use a exponential function for each pixel to emphasize high values.
    mask = mask * (mask > threshold) # Create a new image where low values are set to zero.
    
    mask = np.repeat(mask[:, :, np.newaxis], 3, axis=2) # Convert from single channel to three channel.
    color = tuple([int(c * 255) for c in color]) # Get the color tuple in range 0-255.

    masked = np.multiply((1 - alpha), image) + np.multiply(alpha, mask * color) # Replace pixels in the original image with the required color.
    
    masked = masked.astype(np.uint8) # Convert back to unit8 dtype
    
    return masked




def display_instances(image, color_scale, blur=False, contour=True, alpha=0.5, ax=None):
    """
    Display an image with color_scale masks overlaid, optionally showing contours.

    Args:
        image (np.array): The input image with shape (height, width, channels) -- [0,255].
        color_scale (np.array): The masks to overlay on the image with shape (num_masks, height, width). [0,1]
        blur (bool, optional): Whether to apply a blur effect to the masks. Defaults to False.
        contour (bool, optional): Whether to display the contour lines around the masks. Defaults to True.
        alpha (float, optional): The transparency level of the masks, ranging from 0 (transparent) to 1 (opaque). Defaults to 0.5.
        ax (matplotlib.axes.Axes, optional): The axes object to plot the image on. Defaults to None.

    Returns:
        np.array: The image with masks and contours overlaid.
    """
    # Create a new figure and axes if not provided
    if ax is None:
        fig = plt.figure(figsize=(5, 5), frameon=False)
        ax = plt.Axes(fig, [0., 0., 1., 1.])
        fig.add_axes(ax)
        ax = plt.gca()

    # Remove the axes from the plot
    ax.set_axis_off()

    # Get the number of masks
    N = color_scale.shape[0]
    
    colors = company_colors[:N]
    # Generate random colors for each mask
    # colors = random_colors(N)

    # Set plot boundaries
    height, width = image.shape[:2]
    margin = 0
    ax.set_ylim(height + margin, -margin)
    ax.set_xlim(-margin, width + margin)
    ax.axis('off')

    # Make a copy of the image to apply masks
    masked_image = image.astype(np.uint8).copy()
    color_map = np.zeros_like(masked_image)

    # Apply masks and contours to the image
    for i in range(N):
        color = colors[i]
        _mask = color_scale[i]

        # Optionally blur the mask
        if blur:
            _mask = cv2.blur(_mask, (10, 10))

        # Apply the mask to the image
        # masked_image = apply_mask(masked_image, _mask, color, alpha)
        for c in range(3):
            color_map[:, :, c] = color_map[:, :, c] +  _mask * color[c] * 255

        # Optionally draw contours around the mask
        if contour:
            padded_mask = np.zeros((_mask.shape[0] + 2, _mask.shape[1] + 2))
            padded_mask[1:-1, 1:-1] = _mask
            contours = find_contours(padded_mask, )
            
            for verts in contours:
                # Subtract the padding and flip (y, x) to (x, y)
                verts = np.fliplr(verts) - 1
                p = Polygon(verts, facecolor="none", edgecolor=color)
                ax.add_patch(p)
    color_map = color_map / N
    masked_image = masked_image * (1 - alpha ) + alpha *  color_map
    # Display the image with masks and contours
    ax.imshow(masked_image/255)
    return masked_image



def show_attn(img, attn, threshold=0, patch_size=16):
    """
    Visualize attention weights from an attention-based model on an input image.

    Args:
        img (torch.Tensor): The input image tensor with shape (C, H, W).
        attn (torch.Tensor): The attention tensor with shape (num_heads, num_patches + 1, num_patches + 1).
        threshold (float, optional): The percentage of total attention mass to keep for visualization. Defaults to 0.
        patch_size (int, optional): The size of each image patch. Defaults to 16.

    Returns:
        tuple: A tuple containing two NumPy arrays:
            - attentions (np.array): The upsampled attention maps with shape (num_heads, H, W).
            - th_attn (np.array): The upsampled thresholded attention maps with shape (num_heads, H, W), 
                                 containing only the top attention values that account for the specified
                                 percentage of the total attention mass.
    """
    w_featmap = img.shape[-2] // patch_size
    h_featmap = img.shape[-1] // patch_size

    attentions = attn

    nh = attentions.shape[0] # number of head

    # we keep only the output patch attention for the CLS token
    attentions = attentions[:, 0, 1:].reshape(nh, -1) # nh, N-1
    
    if threshold is not None:
        # we keep only a certain percentage of the mass
        # Sort the attentions and normalize them by the sum of each head's attention values
        val, idx = torch.sort(attentions)
        val /= torch.sum(val, dim=1, keepdim=True)
        
        # Compute the cumulative sum of normalized attention values
        cumval = torch.cumsum(val, dim=1)
        
        # Create a boolean mask to keep only the top attention values accounting for the specified threshold
        th_attn = cumval > (1 - threshold)
        
        idx2 = torch.argsort(idx)
        
        for head in range(nh):
            th_attn[head] = th_attn[head][idx2[head]]
            
        th_attn = th_attn.reshape(nh, w_featmap, h_featmap).float()
        # interpolate
        th_attn = nn.functional.interpolate(th_attn.unsqueeze(0), scale_factor=patch_size, mode="nearest")[0].cpu().numpy()

    attentions = attentions.reshape(nh, w_featmap, h_featmap)
    attentions = nn.functional.interpolate(attentions.unsqueeze(0), scale_factor=patch_size, mode="nearest")[0].cpu().numpy()

    return attentions, th_attn

def show_attn_color(image, attentions, th_attn, head=None, ax=None,
                    blur = False, contour = False,  alpha = 1):
    image_size = image.shape[1:]
    
    if ax is None:
        figsize = tuple([i / 100 for i in image_size])
        fig = plt.figure(figsize=figsize, frameon=False, dpi=100)
        ax = plt.Axes(fig, [0., 0., 1., 1.])
        ax.set_axis_off()
        fig.add_axes(ax)
        ax = plt.gca()
    if head is None:
        head = list(range(len(attentions)))
        
    image = (image.permute(1,2,0)*255).numpy().astype(np.uint32)
    
    
    attentions1 = attentions.copy()
    select_mask = attentions.argmax(0)
    for j in head:
        attn = attentions[j]
        v_min,v_max = attn.min(),attn.max()
        attn = (attn-v_min)/(v_max-v_min)
        m = attn * th_attn[j] * (select_mask==j).astype(float)
        attentions1[j] = m        
    
    masked_image = display_instances(image, attentions1,ax=ax,contour=contour,alpha=alpha,blur=blur) 
    # ax.imshow(masked_image.astype(np.uint8), aspect='auto')
    # ax.axis('image')
    return masked_image
if __name__ == '__main__':
    parser = argparse.ArgumentParser('Visualize Self-Attention maps')
    parser.add_argument('--arch', default='vit_small', type=str, choices=['vit_tiny', 'vit_small', 'vit_base', 
        'vit_large', 'swin_tiny','swin_small', 'swin_base', 'swin_large'], help='Architecture.')
    parser.add_argument('--patch_size', default=8, type=int, help='Patch resolution of the model.')
    parser.add_argument('-w', '--pretrained_weights', default='', type=str, help="""Path to pretrained 
        weights to evaluate. Set to `download` to automatically load the pretrained DINO from url.
        Otherwise the model is randomly initialized""")
    parser.add_argument("--checkpoint_key", default="teacher", type=str,
        help='Key to use in the checkpoint (example: "teacher")')
    parser.add_argument("--image_path", default=None, type=str, help="Path of the single image.")
    parser.add_argument('--data_location', default='/path/to/imagenet/val/', type=str, help='Path of the images\' folder.')
    parser.add_argument("--batch_size", type=int, default=32, help="batch size")
    parser.add_argument("--image_size", default=(480, 480), type=int, nargs="+", help="Resize image.")
    parser.add_argument('--output_dir', default='.', help='Path where to save visualizations.')
    parser.add_argument("--show_pics", type=int, default=100)
    parser.add_argument("--threshold", type=float, default=0.6, help="""We visualize masks
        obtained by thresholding the self-attention maps to keep xx% of the mass.""")
    args = parser.parse_args()

    misc.fix_random_seeds(0)

    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
    # build model
    model = models.__dict__[args.arch](patch_size=args.patch_size, num_classes=0)
    for p in model.parameters():
        p.requires_grad = False
    model.eval()
    model.to(device)
    if os.path.isfile(args.pretrained_weights):
        state_dict = torch.load(args.pretrained_weights, map_location="cpu")
        if args.checkpoint_key is not None and args.checkpoint_key in state_dict:
            print(f"Take key {args.checkpoint_key} in provided checkpoint dict")
            state_dict = state_dict[args.checkpoint_key]
        # remove `module.` prefix
        state_dict = {k.replace("module.", ""): v for k, v in state_dict.items()}
        # remove `backbone.` prefix induced by multicrop wrapper
        state_dict = {k.replace("backbone.", ""): v for k, v in state_dict.items()}
        msg = model.load_state_dict(state_dict, strict=False)
        print('Pretrained weights found at {} and loaded with msg: {}'.format(args.pretrained_weights, msg))
    else:
        print("Please use the `--pretrained_weights` argument to indicate the path of the checkpoint to evaluate.")
        url = None
        if args.arch == "vit_small" and args.patch_size == 16:
            url = "dino_deitsmall16_pretrain/dino_deitsmall16_pretrain.pth"
        elif args.arch == "vit_small" and args.patch_size == 8:
            url = "dino_deitsmall8_300ep_pretrain/dino_deitsmall8_300ep_pretrain.pth"  # model used for visualizations in our paper
        elif args.arch == "vit_base" and args.patch_size == 16:
            url = "dino_vitbase16_pretrain/dino_vitbase16_pretrain.pth"
        elif args.arch == "vit_base" and args.patch_size == 8:
            url = "dino_vitbase8_pretrain/dino_vitbase8_pretrain.pth"
        if url is not None:
            print("Since no pretrained weights have been provided, we load the reference pretrained DINO weights.")
            state_dict = torch.hub.load_state_dict_from_url(url="https://dl.fbaipublicfiles.com/dino/" + url)
            model.load_state_dict(state_dict, strict=True)
        else:
            print("There is no reference weights available for this model => We use random weights.")

    transform = pth_transforms.Compose([
        pth_transforms.Resize(args.image_size),
        pth_transforms.ToTensor(),
        pth_transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225)),
    ])


    # open image
    # if args.image_path is None:
    #     # user has not specified any image - we use our own image
    #     print("Please use the `--image_path` argument to indicate the path of the image you wish to visualize.")
    #     print("Since no image path have been provided, we take the first image in our paper.")
    #     response = requests.get("https://dl.fbaipublicfiles.com/dino/img.png")
    #     img = Image.open(BytesIO(response.content))
    #     img = img.convert('RGB')
    if args.image_path is not None and os.path.isfile(args.image_path):
        with open(args.image_path, 'rb') as f:
            img = Image.open(f)
            img = img.convert('RGB')

        img = transform(img)

        # make the image divisible by the patch size
        w, h = img.shape[1] - img.shape[1] % args.patch_size, img.shape[2] - img.shape[2] % args.patch_size
        img = img[:, :w, :h].unsqueeze(0)
        attentions, th_attn, pic_i, pic_attn = show_attn(img)
        pic_attn_color = show_attn_color(img.permute(1, 2, 0).cpu().numpy(), attentions, th_attn)
        final_pic = Image.new('RGB', (pic_i.size[1] * 2 + pic_attn.size[0], pic_i.size[1]))
        final_pic.paste(pic_i, (0, 0))
        final_pic.paste(pic_attn_color, (pic_i.size[1], 0))
        final_pic.paste(pic_attn, (pic_i.size[1] * 2, 0))
        final_pic.save(os.path.join(args.output_dir, f"attn.png"))
    
    else:
        train_dataloader = DataLoader(
            InstanceWraper(args.data_location, transform=transform), 
            batch_size=args.batch_size, 
            shuffle=True, 
            num_workers=4, 
            pin_memory=True)

        cnt = 0
        for data in tqdm(train_dataloader):
            img, _, idx = data
            for i in range(img.size(0)):
                attentions, th_attn, pic_i, pic_attn = show_attn(img[i:i+1], index=idx[i].item())
                pic_attn_color = show_attn_color(img[i].permute(1, 2, 0).cpu().numpy(), attentions, th_attn, index=idx[i].item())
                final_pic = Image.new('RGB', (pic_i.size[1] * 2 + pic_attn.size[0], pic_i.size[1]))
                final_pic.paste(pic_i, (0, 0))
                final_pic.paste(pic_attn_color, (pic_i.size[1], 0))
                final_pic.paste(pic_attn, (pic_i.size[1] * 2, 0))
                final_pic.save(os.path.join(args.output_dir, f"idx{idx[i].item()}_attn.png"))
                cnt += 1
                if cnt == args.show_pics:
                    exit()