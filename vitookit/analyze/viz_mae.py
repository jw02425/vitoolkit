#%%
import torch,math,os,torchvision,random, einops
from torch import nn
import pytorch_lightning as pl
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import torch.nn.functional as F
import torchvision.transforms as tfms
import torchvision.transforms.functional as tfF
from torchvision.utils import *

from sklearn.cluster import KMeans

from torchvision import datasets, transforms
from model import *

IMAGENET_DEFAULT_MEAN = (0.485, 0.456, 0.406)
IMAGENET_DEFAULT_STD = (0.229, 0.224, 0.225)
tfms=transforms.Compose([
    transforms.CenterCrop(224),
    transforms.ToTensor(),
    transforms.Normalize(IMAGENET_DEFAULT_MEAN, IMAGENET_DEFAULT_STD),
])
data=datasets.OxfordIIITPet('../../data','test',transform=tfms)

def denormalize(imagesToPrint):
    imagesToPrint = imagesToPrint.clone()
    imagesToPrint *= torch.Tensor(IMAGENET_DEFAULT_STD,device=imagesToPrint.device).reshape(3,1,1)
    imagesToPrint += torch.Tensor(IMAGENET_DEFAULT_MEAN,device=imagesToPrint.device).reshape(3,1,1)
    return imagesToPrint.clamp(0,1)

imgs = torch.stack([data[i][0] for i in range(10)])

pic = make_grid(imgs,5)
tfF.to_pil_image(denormalize(pic))

# %%
from model_scl import *
model = vit_small()
ckpt = torch.load('../outputs/mae_pet/checkpoint.pth')
model.requires_grad_(False)
model.load_state_dict(ckpt['model'],strict=False)

# %%

###### encoder ######
B=5
selects = (torch.arange(B*196)%196).reshape(B,196)[:,:]
visible = selects[:,:98]
masked =  selects[:,98:]
aug_x = imgs[:B].clone()
aug_x[:,:,112:] = imgs[:B,:,112:]
# z = torch.cat([model.head_token(B),],1)
z = model.forward_encoder(aug_x,selects)

############# dencode ####################

# append head token, N + 1 paches
recon = model.forward_decoder(z[:,:99],visible,masked)
recon = recon[:,1:]

img_patches = model.patchify(imgs)
loss = (select_by_index(img_patches,masked)-recon[:,(visible.size(1)):]).abs().mean()
loss
# %%
recon = select_by_index(recon,torch.tensor([inverse_select(s) for s in selects]))
recon_img = model.unpatchify(recon)
pic = make_grid(recon_img,5)
tfF.to_pil_image(denormalize(pic))
# %%

loss,log = model(imgs)
loss
#%% mix two images

##########original images ###################
##########  mix images  ###################
perm = torch.tensor([list(range(196))]*5)
x1 = model.patchify(imgs[:5])
x2=model.patchify(torch.randn_like(imgs[:5]))
x3=model.patchify(imgs[5:])
N1,N2,N3=model.split
x_aug = torch.cat([
    select_by_index(x1,perm[:,:N1]),
    select_by_index(x2,perm[:,N1:N1+N2]),
    select_by_index(x3,perm[:,-N3:]),
    ],1)
inv_perm=torch.tensor([inverse_select(s) for s in perm])

x_aug = model.unpatchify(select_by_index(x_aug,inv_perm))

recon_list=[x_aug]


##########  forward     ##########

z = model.forward_encoder(x_aug, perm)

z1 = z[:,1:N1+1]

recon1 = model.forward_decoder(
    torch.cat([z[:,:1],z1],1),perm[:,:N1],perm[:,N1:])

z3 = z[:,-N3:]
recon3 = model.forward_decoder(
    torch.cat([z[:,:1],z3],1),perm[:,-N3:],perm[:,:-N3])

recon1 = recon1[:,1:]
recon3 = recon3[:,1:]
##########recon images 1###################

recon_list+=[model.unpatchify(select_by_index(recon1,inv_perm))]
##########recon images 3###################

inv_x3 = torch.tensor([inverse_select(s) for s in (torch.cat([perm[:,-N3:],perm[:,:-N3]],1))]) 
recon_list+=[model.unpatchify(select_by_index(recon3,inv_x3))]

pic = make_grid(torch.cat(recon_list),5)
tfF.to_pil_image(denormalize(pic))
# %%
z=log['z']
s=z.matmul(z.transpose(1,2))
plt.imshow(s[1])
# %%
selects = (torch.arange(5*196)%196).reshape(5,196)
inv_perm=torch.tensor([inverse_select(s) for s in selects])

x_aug = torch.cat([
    select_by_index(x1,selects[:,:N1]),
    select_by_index(x2,selects[:,N1:-N3]),
    select_by_index(x3,selects[:,-N3:]),
    ],1)
img_aug = model.unpatchify(
    select_by_index(x_aug,inv_perm))
x = model.patch_embed(img_aug)
x = x + model.pos_embed[:,1:] # B, N, C

# z = torch.cat([model.head_token(B),],1)
z = model.forward_encoder(img_aug,selects)

############# dencode ####################
# append head token, N + 1 paches
recon = model.forward_decoder(z[:,:N1*2+1], 
                               selects[:,:N1*2],
                               selects,)

recon_list=[img_aug,
    model.unpatchify(
    select_by_index(recon[:,-196:],
                    selects
            # torch.tensor([ inverse_select(s) for s in 
            #              torch.cat([selects[:,-N3:],selects[:,:-N3]],1)])
            ))]

pic = make_grid(torch.cat(recon_list),5)
tfF.to_pil_image(denormalize(pic))

# %%

s = z.matmul(z.transpose(1,2))
plt.imshow(s[0])
# %%
plt.imshow(s[0,:N1+1,:N1+1])
# %%
