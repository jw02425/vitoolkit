#%%
import torch,math,os,torchvision,random, einops
from torch import nn
import pytorch_lightning as pl
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import torch.nn.functional as F
import torchvision.transforms as tfms
import torchvision.transforms.functional as tfF
from torchvision.utils import *

from sklearn.cluster import KMeans

from torchvision import datasets, transforms
from model import *

IMAGENET_DEFAULT_MEAN = (0.485, 0.456, 0.406)
IMAGENET_DEFAULT_STD = (0.229, 0.224, 0.225)
tfms=transforms.Compose([
    transforms.CenterCrop(224),
    transforms.ToTensor(),
    transforms.Normalize(IMAGENET_DEFAULT_MEAN, IMAGENET_DEFAULT_STD),
])
data=datasets.OxfordIIITPet('../../data','test',transform=tfms)

def denormalize(imagesToPrint):
    imagesToPrint *= torch.Tensor(IMAGENET_DEFAULT_STD,device=imagesToPrint.device).reshape(3,1,1)
    imagesToPrint += torch.Tensor(IMAGENET_DEFAULT_MEAN,device=imagesToPrint.device).reshape(3,1,1)
    return imagesToPrint.clamp(0,1)

x = torch.stack([data[i][0] for i in range(10)])

pic = make_grid(x,10)
tfF.to_pil_image(denormalize(pic))


# %%
from vgmml_tail import *
model = vit_small(skip_cnnt=0,num_latent=384)
model.head_recons = model.head_recon
del model.head_recon
ckpt = torch.load("../outputs/gmml/checkpoint.pth")
model.requires_grad_(False)
model_state={ k.replace('module.',''):v for k,v in ckpt['SiT_model'].items()}
model.load_state_dict(model_state,strict=False)
#%% half masking
recon_list = []
visible_ratio = 0.5
mask_below = int(224*visible_ratio)

x_origin = x.clone()
x_origin[:,:,mask_below:] *=0.3

x_aug = x.clone()
x_aug[:,:,mask_below:]=torch.randn_like(x_aug[:,:,mask_below:])

masks = torch.zeros_like(x)
masks[:,:,mask_below:]=1
######forward#######
z = model.backbone(x_aug,'6-8-10-12')
recons = model.head_recons(z)

loss = ((recons-x_origin).abs()*masks).mean()
######forward#######
recons[:,:,:mask_below] *= 0.3

recon_list=[x,x_aug,recons]

imagesToPrint = torch.cat(recon_list, dim=0).data.cpu()

pic = make_grid(imagesToPrint,10,normalize=True,range=(-1,1))
tfF.to_pil_image(pic)

# %%
clean_crops = x
recon_list=[]


masks_crops = torch.zeros_like(x)
masks_crops[:,:,112:]=1
corrupted_crops = clean_crops.clone()
corrupted_crops[:,:,112:]=torch.randn_like(corrupted_crops[:,:,112:])
loss2,log = model(clean_crops,corrupted_crops,masks_crops,)
recon_list += [clean_crops,corrupted_crops,log['recons']]

# right masking

masks_crops = torch.zeros_like(masks_crops)
masks_crops[:,:,:,112:]=1
corrupted_crops = clean_crops.clone()
corrupted_crops[:,:,:,112:]=torch.randn_like(corrupted_crops[:,:,:,112:])
loss3,log = model(clean_crops,corrupted_crops,masks_crops)
recon_list += [clean_crops,corrupted_crops,log['recons']]

imagesToPrint = torch.cat(recon_list, dim=0).data.cpu()

pic = make_grid(imagesToPrint,10,normalize=True,range=(-1,1))
tfF.to_pil_image(pic)
# %% manipulate local and global 

# half
corrupted_crops = x.clone()
corrupted_crops[:,:,:,112:]=torch.randn_like(corrupted_crops[:,:,:,112:])

features = model.backbone(corrupted_crops,"6-8-10-12") # B, N, C
mu, logvar = model.va_layer.distribution(features)
z = sample_from_latent(mu,logvar)

recon_list=[x,corrupted_crops]
# local + global
recon = model.decode(mu,x)
recon_list+=[recon]

# local only
recon = model.decode(torch.randn_like(mu),x)
recon_list+=[recon]

# global only

recon = [model.decode(mu,torch.randn_like(x)) for i in range(100)]
recon = torch.stack(recon).mean(0)
recon_list+=[recon]

# local + permute global
recon = model.decode(mu[[i%10 for i in range(1,11)]],x)
recon_list+=[recon]

imagesToPrint = torch.cat(recon_list, dim=0).data.cpu()

pic = make_grid(imagesToPrint,10,normalize=True,range=(-1,1))
tfF.to_pil_image(pic)
# %% find the informative dimensions
_,indices = mu.std([0,1]).sort()
info_indices = indices[-10:]

#%%
idx = -2
features = model.backbone(x[idx][None,:],"6-8-10-12") # B, N, C
mu, logvar = model.va_layer.distribution(features)

recon_list=[]
for i in range(len(info_indices)):
    z = mu.repeat(8,1,1)
    z[:,:,info_indices[i]] = torch.linspace(-2,2,8).unsqueeze(-1)
    recon = model.decode(z,x[idx][None,:].repeat(8,1,1,1))
    recon_list.append(recon)
    
imagesToPrint = torch.cat(recon_list, dim=0).data.cpu()

pic = make_grid(imagesToPrint,10,normalize=True,range=(-1,1))

plt.imshow(einops.rearrange(imagesToPrint.reshape(10,8,3,224,224).std([1,2]),"b h w-> h (b w)"))

tfF.to_pil_image(pic)
# %%
