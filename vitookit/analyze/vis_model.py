
# %% k means
import torch,math,os,torchvision,random, einops
from torch import nn
import pytorch_lightning as pl
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import torch.nn.functional as F
import torchvision.transforms as tfms
import torchvision.transforms.functional as tfF
from torchvision.utils import *

from sklearn.cluster import KMeans

from torchvision import datasets, transforms
from model import *

IMAGENET_DEFAULT_MEAN = (0.485, 0.456, 0.406)
IMAGENET_DEFAULT_STD = (0.229, 0.224, 0.225)
tfms=transforms.Compose([
    transforms.CenterCrop(224),
    transforms.ToTensor(),
    transforms.Normalize(IMAGENET_DEFAULT_MEAN, IMAGENET_DEFAULT_STD),
])
data=datasets.OxfordIIITPet('../../data','test',transform=tfms)

def denormalize(imagesToPrint):
    imagesToPrint = imagesToPrint.clone()
    imagesToPrint *= torch.Tensor(IMAGENET_DEFAULT_STD,device=imagesToPrint.device).reshape(3,1,1)
    imagesToPrint += torch.Tensor(IMAGENET_DEFAULT_MEAN,device=imagesToPrint.device).reshape(3,1,1)
    return imagesToPrint.clamp(0,1)

x = torch.stack([data[i][0] for i in range(10)])

pic = make_grid(x,10)
tfF.to_pil_image(denormalize(pic))


#%%

class VatiationalAutoencoderViT(nn.Module):
    """ Masked Autoencoder with VisionTransformer backbone
    """
    def __init__(self, img_size=224, patch_size=16, in_chans=3,
                 embed_dim=1024, depth=24, num_heads=16,
                 decoder_embed_dim=512, decoder_depth=8, decoder_num_heads=16,
                 mlp_ratio=4., norm_layer=nn.LayerNorm, norm_pix_loss=False,beta=1,k=0,num_latent=32,):
        super().__init__()

        self.beta=beta
        self.beta2 = 1
        self.k=k
        self.num_latent = num_latent
        # --------------------------------------------------------------------------
        # MAE encoder specifics
        self.patch_embed = PatchEmbed(img_size, patch_size, in_chans, embed_dim)
        num_patches = self.patch_embed.num_patches

        self.cls_token = nn.Parameter(torch.zeros(1, 1, embed_dim))
        self.pos_embed = nn.Parameter(torch.zeros(1, num_patches + 1, embed_dim), requires_grad=False)  # fixed sin-cos embedding

        self.blocks = nn.ModuleList([
            Block(embed_dim, num_heads, mlp_ratio, qkv_bias=True,  norm_layer=norm_layer)
            for i in range(depth)])
        self.norm = norm_layer(embed_dim)
        
        self.va_layer = VALayer(embed_dim,num_latent)
        self.local_proj = DisInvariant(num_latent)
        self.local_decoder = RECHead(num_latent)
        # --------------------------------------------------------------------------

        # --------------------------------------------------------------------------
        # MAE decoder specifics
        self.decoder_embed = nn.Linear(num_latent, decoder_embed_dim, bias=True)

        self.mask_token = nn.Parameter(torch.zeros(1, 1, decoder_embed_dim))

        self.decoder_pos_embed = nn.Parameter(torch.zeros(1, num_patches + 1, decoder_embed_dim), requires_grad=False)  # fixed sin-cos embedding

        self.decoder_blocks = nn.ModuleList([
            Block(decoder_embed_dim, decoder_num_heads, mlp_ratio, qkv_bias=True,  norm_layer=norm_layer)
            for i in range(decoder_depth)])

        self.decoder_norm = norm_layer(decoder_embed_dim)
        self.decoder_pred = nn.Linear(decoder_embed_dim, patch_size**2 * in_chans, bias=True) # decoder to patch
        # --------------------------------------------------------------------------

        self.norm_pix_loss = norm_pix_loss

        self.initialize_weights()

    def initialize_weights(self):
        # initialization
        # initialize (and freeze) pos_embed by sin-cos embedding
        pos_embed = get_2d_sincos_pos_embed(self.pos_embed.shape[-1], int(self.patch_embed.num_patches**.5), cls_token=True)
        self.pos_embed.data.copy_(torch.from_numpy(pos_embed).float().unsqueeze(0))

        decoder_pos_embed = get_2d_sincos_pos_embed(self.decoder_pos_embed.shape[-1], int(self.patch_embed.num_patches**.5), cls_token=True)
        self.decoder_pos_embed.data.copy_(torch.from_numpy(decoder_pos_embed).float().unsqueeze(0))

        # initialize patch_embed like nn.Linear (instead of nn.Conv2d)
        w = self.patch_embed.proj.weight.data
        torch.nn.init.xavier_uniform_(w.view([w.shape[0], -1]))

        # timm's trunc_normal_(std=.02) is effectively normal_(std=0.02) as cutoff is too big (2.)
        torch.nn.init.normal_(self.cls_token, std=.02)
        torch.nn.init.normal_(self.mask_token, std=.02)

        # initialize nn.Linear and nn.LayerNorm
        self.apply(self._init_weights)

    def _init_weights(self, m):
        if isinstance(m, nn.Linear):
            # we use xavier_uniform following official JAX ViT:
            torch.nn.init.xavier_uniform_(m.weight)
            if isinstance(m, nn.Linear) and m.bias is not None:
                nn.init.constant_(m.bias, 0)
        elif isinstance(m, nn.LayerNorm):
            nn.init.constant_(m.bias, 0)
            nn.init.constant_(m.weight, 1.0)

    def patchify(self, imgs):
        """
        imgs: (N, 3, H, W)
        x: (N, L, patch_size**2 *3)
        """
        p = self.patch_embed.patch_size[0]
        assert imgs.shape[2] == imgs.shape[3] and imgs.shape[2] % p == 0

        h = w = imgs.shape[2] // p
        x = imgs.reshape(shape=(imgs.shape[0], 3, h, p, w, p))
        x = torch.einsum('nchpwq->nhwpqc', x)
        x = x.reshape(shape=(imgs.shape[0], h * w, p**2 * 3))
        return x

    def unpatchify(self, x):
        """
        x: (N, L, patch_size**2 *3)
        imgs: (N, 3, H, W)
        """
        p = self.patch_embed.patch_size[0]
        h = w = int(x.shape[1]**.5)
        assert h * w == x.shape[1]
        
        x = x.reshape(shape=(x.shape[0], h, w, p, p, 3))
        x = torch.einsum('nhwpqc->nchpwq', x)
        imgs = x.reshape(shape=(x.shape[0], 3, h * p, h * p))
        return imgs

    def random_masking(self, x, mask_ratio):
        """
        Perform per-sample random masking by per-sample shuffling.
        Per-sample shuffling is done by argsort random noise.
        x: [N, L, D], sequence
        """
        N, L, D = x.shape  # batch, length, dim
        len_keep = int(L * (1 - mask_ratio))
        
        noise = torch.rand(N, L, device=x.device)  # noise in [0, 1]
        
        # sort noise for each sample
        ids_shuffle = torch.argsort(noise, dim=1)  # ascend: small is keep, large is remove
        ids_restore = torch.argsort(ids_shuffle, dim=1)

        # keep the first subset
        ids_keep = ids_shuffle[:, :len_keep]
        x_masked = torch.gather(x, dim=1, index=ids_keep.unsqueeze(-1).repeat(1, 1, D))

        # generate the binary mask: 0 is keep, 1 is remove
        mask = torch.ones([N, L], device=x.device)
        mask[:, :len_keep] = 0
        # unshuffle to get the binary mask
        mask = torch.gather(mask, dim=1, index=ids_restore)

        return x_masked, mask, ids_restore

    def forward_encoder(self, x, mask_ratio):
        # embed patches
        x = self.patch_embed(x)

        # add pos embed w/o cls token
        x = x + self.pos_embed[:, 1:, :]

        # append cls token
        cls_token = self.cls_token + self.pos_embed[:, :1, :]
        cls_tokens = cls_token.expand(x.shape[0], -1, -1)
        x = torch.cat((cls_tokens, x), dim=1)

        # apply Transformer blocks
        for blk in self.blocks:
            x = blk(x)
        x = self.norm(x)

        return x

    def forward_decoder(self, x, mask):
        
        # embed tokens
        x = self.decoder_embed(x)
        B, N_1, C = x.shape
        N = N_1 -1
        
        mask_tokens = self.mask_token.expand(B, N, -1)
        # print(mask_tokens.shape, x.shape,mask.shape)
        w = mask.unsqueeze(-1)
        x_ = x[:, 1:, :]
        x_ = x_ * (1. - w) + mask_tokens * w
        x = torch.cat([x[:, :1, :], x_], dim=1)  # append cls token
        
        # add pos embed
        x = x + self.decoder_pos_embed

        # apply Transformer blocks
        for blk in self.decoder_blocks:
            x = blk(x)
        x = self.decoder_norm(x)

        # predictor projection
        x = self.decoder_pred(x)

        # remove cls token
        x = x[:, 1:, :]

        return x

    def forward_loss(self, imgs, pred, mask):
        """
        imgs: [N, 3, H, W]
        pred: [N, L, p*p*3]
        mask: [N, L], 0 is keep, 1 is remove, 
        """
        target = self.patchify(imgs)
        if self.norm_pix_loss:
            mean = target.mean(dim=-1, keepdim=True)
            var = target.var(dim=-1, keepdim=True)
            target = (target - mean) / (var + 1.e-6)**.5

        loss = (pred - target).square() # note: l2 or l1
        loss = loss.mean(dim=-1)  # [N, L], mean loss per patch

        loss = (loss * mask).sum() / mask.sum()  # mean loss on removed patches
        return loss

    def forward(self, imgs, mask_ratio=0.75):
        latent = self.forward_encoder(imgs, mask_ratio) # B, N+1, C
        
        mu,logvar = self.va_layer.distribution(latent)
        z = sample_from_latent(mu, logvar)
        kl = compute_gaussian_kl(mu,logvar)
        
        B, N_1, C = z.shape
        N = N_1 -1
        
        # masking
        mask = (torch.rand(B,N,device=imgs.device)<mask_ratio)
        
        # global view
        pred = self.forward_decoder(z, mask.type_as(z))  # [N, L, p*p*3]
        recon_loss = self.forward_loss(imgs, pred, mask.type_as(z))
        kl_loss = kl[:,1:][mask].mean()
        
        loss_global = recon_loss + kl_loss*self.beta
        
        # local view
        mu,logvar = self.local_proj(mu,logvar)
        z_local = sample_from_latent(mu,logvar)
        kl_local = compute_gaussian_kl(mu,logvar)
        
        pred_local = self.local_decoder(z_local[:,1:])
        
        recon_loss_local = F.mse_loss(pred_local,imgs)
        kl_loss_local = kl_local.mean()
        loss_local = recon_loss_local + kl_loss_local*self.beta2
        
        # all
        loss = loss_global + loss_local*self.k
        log = {
            'recon_loss':recon_loss.item(),
            'kl_loss': kl_loss.item(),
            'recon_loss_local':recon_loss_local.item(),
            'kl_loss_local': kl_loss_local.item(),
            'beta':self.beta,
            'beta2':self.beta2,
        }
        return loss, pred, mask,log
#%% 
from model import VatiationalAutoencoderViT
model=VatiationalAutoencoderViT(
    patch_size=16, embed_dim=512, depth=8, num_heads=16,
    decoder_embed_dim=384, decoder_depth=12, decoder_num_heads=6,
    mlp_ratio=4, norm_layer=partial(nn.LayerNorm, eps=1e-6),num_latent=64)
ckpt = torch.load('../outputs/tfae-64/tfae-0.1-1/checkpoint.pth')
model.requires_grad_(False)
model.load_state_dict(ckpt['model'],strict=False)
# %% mask and predict
x = torch.stack([data[i][0] for i in range(10)])
B,N,C = 10, 196, 384
visible_ratio = 0.5
mask_below = int(14*visible_ratio)*14+1


latent = model.forward_encoder(x)
mu,logvar = model.va_layer.distribution(latent)
patch_tokens = mu

mask = torch.zeros(B,N)
mask[:,mask_below:]=1
nn.init.normal_(mu[:,0])
# mask = (torch.rand(B,N)>visible_ratio).float()

_,pred = model.forward_decoder(mu, mask)  # [N, L, p*p*3]
mu_local,logvar_local = model.local_proj(mu,logvar)

pred_local = model.local_decoder(mu_local)
recon_loss = model.forward_loss(x, pred, mask)
print(recon_loss)

x_inputs = x.clone()
# x_inputs[:,:,int(224*(visible_ratio)):] *=0.3

recon_list = torch.cat([x_inputs,model.unpatchify(pred),pred_local])
recon_list[:,:,:int(224*visible_ratio)] *= 0.3
pic = make_grid(denormalize(recon_list),10)
tfF.to_pil_image(pic)

#%% Kmeans 
import cv2
def scale(im, nR, nC):
    nR0 = len(im)     # source number of rows 
    nC0 = len(im[0])  # source number of columns 
    return np.array([[ im[int(nR0 * r / nR)][int(nC0 * c / nC)]  
                for c in range(nC)] for r in range(nR)])

def cluster_segmentation(x, patch_tokens, num=3,alpha=0.5):
    bs = len(patch_tokens)
    segs = []
    mix = []
    for idx in range(bs):
        kmeans=KMeans(num)
        c_cluster = kmeans.fit_predict(patch_tokens[idx])
        c_cluster = c_cluster/(num-1)*255
        c_cluster = scale(c_cluster.reshape(14,14),224,224)
        
        img = denormalize(x)[idx]
        heatmap = cv2.applyColorMap(c_cluster.astype(np.uint8), cv2.COLORMAP_JET)
        # plt.imshow(heatmap)
        heatmap = torch.Tensor(heatmap).permute(2,0,1)/255
        mix.append(img * alpha + heatmap * (1-alpha)) 
        segs.append(c_cluster)
    return segs,mix

_,t=cluster_segmentation(x,mu,alpha=0.6)
pic = make_grid(torch.stack(t),10)
tfF.to_pil_image(pic)
# %%

variance = mu.std([0,1])
_,info_indices = variance.sort()
info_indices = info_indices[-10:]
info_indices
# %%
idx=-2
recon_list = []
visible_ratio = 0.5
mask_below = int(196*visible_ratio)
for i, z_idx in enumerate(info_indices):
    z = mu[idx][None,:].repeat(8,1,1)
    z[:8,:mask_below,z_idx] = torch.linspace(-2,2,8).reshape(8,1)
    
    # see the top half
    mask = torch.zeros(8,N)
    mask[:,mask_below:] =1
    
    _,pred = model.forward_decoder(z,mask)
    
    origin = model.patchify(x[idx:idx+1]).clone()
    # origin[:,mask_below:]*=0.3
    
    _,pred_noaug = model.forward_decoder(mu[idx][None,:],mask[:1])
    
    recon_list.append(model.unpatchify(torch.cat([pred,origin,pred_noaug]).data))
recon_list = torch.cat(recon_list)
recon_list[:,:,:int(224*visible_ratio)] *= 0.3
pic = make_grid(recon_list,10,normalize=True)
tfF.to_pil_image(pic)
    
# %%
fig,axes = plt.subplots(1,5,figsize=(20,3))
for i in range(5,10):
    ax = axes[i-5]
    sns.heatmap(recon_list[10*i:10*i+8].std([0,1]),ax=ax)
    ax.axis('off')
# %%


from visualization import LayerHook
layer_hook = LayerHook('attn_drop')

# no masking
mask = torch.zeros(10,196)
outputs,layer_outputs  = layer_hook(model,x,mask)
len(layer_outputs)
# (loss, log) = outputs
# %%


pic = make_grid(torch.stack(sim_list),10,normalize=True)
tfF.to_pil_image(pic)
# %%
from model import VatiationalAutoencoderViT
model=VatiationalAutoencoderViT(
    patch_size=16, embed_dim=512, depth=8, num_heads=16,
    decoder_embed_dim=384, decoder_depth=12, decoder_num_heads=6,
    mlp_ratio=4, norm_layer=partial(nn.LayerNorm, eps=1e-6),num_latent=32)

x = torch.rand(10,3,224,224)
msk = torch.rand(10,196)<0.5
model(x,msk)
# %%
from visualization import monitor_model
from argparse import Namespace
monitor_model(model,x,0,Namespace(device='cpu',output_dir='out'))
# %%
