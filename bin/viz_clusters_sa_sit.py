#!python
#%%
"""
for img in img_list:
   %run bin/viz_clusters.py --arch vit_base --pretrained_weights ../SiT/outputs/imagenet/sit-ViT_B/checkpoint.pth --img ../data/ImageNet/val/{img} --cluster_number=10 --output_img=outputs/pics/clusters/{img}-sit-10.jpg
"""
from PIL import Image
import time
import argparse
import cv2
from sklearn.cluster import KMeans
import torch,math,os,torchvision,random, einops
from torch import nn
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import torch.nn.functional as F
import torchvision.transforms as tfms
import torchvision.transforms.functional as tfF
from torchvision.utils import *
from vitookit.datasets.sa import SA1BDataset
from vitookit.utils.helper import load_pretrained_weights
import time
import vitookit.models
from analyze.attention_map.visualize_attention import *
from misc.helper import patchify
from misc.hook import Hook
from torchvision import datasets

def get_args(*args,):
    parser = argparse.ArgumentParser(add_help="bin/clusters.py --arch vit_base --pretrained_weights ../SiT/outputs/imagenet/sit-ViT_B/checkpoint.pth --img imgs/sample4.png")
    parser.add_argument('--patch_size', default=16, type=int, help='Patch resolution of the model.')
    parser.add_argument("--image_size", default=(480, 480), type=int, nargs="+", help="Resize image.")
    parser.add_argument('--arch', default='vit_small', type=str, help='Architecture.')
    parser.add_argument('-w', '--pretrained_weights', default='', type=str, help="""Path to pretrained 
        weights to evaluate. Set to `download` to automatically load the pretrained DINO from url.
        Otherwise the model is randomly initialized""")
    parser.add_argument("--checkpoint_key", default=None, type=str,
        help='Key to use in the checkpoint (example: "teacher")')

    parser.add_argument("--key")

    parser.add_argument(
        '--img',
        type=str,
        default='./imgs/sample1.JPEG',
        help='Input image path')
    
    parser.add_argument(
        '--output_img',
        type=str,
        default='',
        help='output image path')
    
    
    parser.add_argument('--use-cuda', action='store_true', default=True,
                        help='Use NVIDIA GPU acceleration')
    
    
    parser.add_argument('--cluster_number',type=int, default=3)
    
    args = parser.parse_args(*args)
    
    args.use_cuda = args.use_cuda and torch.cuda.is_available()
    if args.use_cuda:
        print('Using GPU for acceleration')
    else:
        print('Using CPU for computation')
        
    return args


if __name__ == '__main__':
    
    args = get_args([])
    ## load model
    model = models.__dict__[args.arch](
        patch_size=args.patch_size, 
        num_classes=0)
    embed_dim = model.embed_dim
    
    from vitookit.models.sit import SiT
    model = SiT(model,)
    t_model = nn.Identity()
    model.eval().requires_grad_(False)
    t_model.module = model
    misc.restart_from_checkpoint("../../SiT/outputs/imagenet/sit_iBOT/checkpoint.pth",teacher=t_model)
    # misc.restart_from_checkpoint("../../SiT/outputs/imagenet100/ablation200/CSSL-1-1-1/checkpoint.pth",teacher=t_model)
    
    from torchvision import datasets, transforms
    IMAGENET_DEFAULT_MEAN = (0.485, 0.456, 0.406)
    IMAGENET_DEFAULT_STD = (0.229, 0.224, 0.225)
    tfms=transforms.Compose([
        transforms.Resize(args.image_size),
        transforms.ToTensor(),
        transforms.Normalize(IMAGENET_DEFAULT_MEAN, IMAGENET_DEFAULT_STD),
    ])
    def denormalize(imagesToPrint):
        imagesToPrint = imagesToPrint.clone()
        imagesToPrint *= torch.Tensor(IMAGENET_DEFAULT_STD,device=imagesToPrint.device).reshape(3,1,1)
        imagesToPrint += torch.Tensor(IMAGENET_DEFAULT_MEAN,device=imagesToPrint.device).reshape(3,1,1)
        return imagesToPrint.clamp(0,1)
    #%% ## get representations
    # list ['sa_10001804','sa_10124407','sa_10300129','sa_10319504','sa_1329708']
    data = SA1BDataset('/vol/research/datasets/still/SA-1B',['sa_258805',
        'sa_2601447',
        'sa_6313499',
        'sa_2592075',
        'sa_258406',
        'sa_2601844',
        'sa_6353015',
        'sa_10958302',
        'sa_6344773'])
    data.min_object = 10*16**2
    
    img, mask, class_ids = data[1]
    masks = cv2.resize(mask,args.image_size)
    masks = masks[None,:].transpose(3,0,1,2)
    mask_patches  = patchify(masks)
    
    
    img = tfms(img)
    image = (denormalize(img).permute(1,2,0)*255).numpy().astype(np.uint8)
    x = img[None,:]
    #%%
    select_point= (140,164)
    
    cls_token,patch_tokens,recon = model(x)
    patch_tokens = nn.functional.normalize(patch_tokens, dim=-1, p=2)[0]
    
    x_patches=patch_tokens.numpy()
    
    num_clusters = args.cluster_number
    
    #################### clusters ########################
    k_means = KMeans(num_clusters)
    cluster_assignments = k_means.fit_predict(x_patches)
    
    nh,hw = args.image_size[0]//args.patch_size, args.image_size[1]//args.patch_size
    
    cluster_assignments_2d = np.reshape(cluster_assignments, (nh,hw))
    #################### similarity ########################
    
    obj_mask = np.matmul(mask_patches,mask_patches.transpose(0,2,1)) #np
    sim = patch_tokens.mm(patch_tokens.T).numpy()
    
    inter_sim = sim[None,:] * (obj_mask>16)
    
    s_x,s_y = select_point[0]//args.patch_size,select_point[1]//args.patch_size
    s = sim[s_x+s_y*hw].reshape(nh,hw) # center object
    s = cv2.resize(s.astype(float),args.image_size,)
    
    #################### visualization ########################
    #%%
    # Create a figure with subplots for each cluster
    
    from misc.visualize import show_points, show_mask
    fig, axes = plt.subplots(1, 6, figsize=(18, 3))
    plt.tight_layout()

    
    ### input image
    ax = axes[0]
    ax.axis('off')
    ax.imshow(image)
    for i in range(len(class_ids)):
        show_mask(masks[i,0,],ax,True)
    
    show_points(np.array([select_point]),
                np.array([1]),ax,)
    ## cluster
    ax = axes[1]
    overlay = []
    for cluster_idx in range(cluster_assignments.max()+1):
        mask = (cluster_assignments_2d == cluster_idx)
        mask = cv2.resize(mask.astype(float),image.shape[:2], interpolation=cv2.INTER_NEAREST)
        # mask = (cv2.pyrUp(mask.astype(float),image.shape[:2]))
        overlay.append(mask)
    
    overlay = np.stack(overlay)
    display_instances(image, overlay, ax=ax,alpha=0.7)
    
    
    ## distribution
    ax = axes[2]
    
    sns.distplot(inter_sim[inter_sim>0].flatten(), label='inter',ax=ax)
    sns.distplot(sim.flatten(), hist=False,label='img',ax=ax)
    ax.legend()
    
    # s[s<0.9]=0 # threshold for background    
    # display_instances(image, s[None,:], ax=ax,alpha=0.7)
    
    ## show the visible patches
    # thresholds = [0.8,0.85,0.9]
    thresholds = np.linspace(0.9,0.95,3)
    for i in range(3):
        ax = axes[i+3]
        ax.axis('off')
        thr = thresholds[i]
        ax.set_title(thr)
        ax.imshow(image*(s[:,:,None]>thr))
    
    if args.output_img:
        outdir = os.path.dirname(args.output_img)
        os.makedirs(outdir,exist_ok=True)
        fig.savefig(args.output_img,pad_inches=0)
        pass
    else:
        import matplotlib.pyplot as plt
        plt.show()
        
        
        
# %%
