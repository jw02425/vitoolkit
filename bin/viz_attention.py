# Copyright (c) ByteDance, Inc. and its affiliates.
# All rights reserved.
#
# This source code is licensed under the license found in the
# LICENSE file in the root directory of this source tree.

"""
Copy-paste from DINO library:
https://github.com/facebookresearch/dino
"""

import os
import argparse
import sys
import cv2
import random
import colorsys
import matplotlib
import matplotlib.pyplot as plt
import torch
import torch.nn as nn
import torchvision
import numpy as np
from vitookit.models.build_model import build_model
from vitookit.utils import misc
from vitookit.utils.helper import load_pretrained_weights
import vitookit.utils
import vitookit.models

from PIL import Image
from skimage.measure import find_contours
from matplotlib.patches import Polygon
from torch.utils.data import DataLoader
from torchvision import transforms as pth_transforms
from torchvision.datasets import ImageFolder
from PIL import Image

from tqdm import tqdm

# matplotlib.use('Agg')

company_colors = [
    (0,160,215), # blue
    (220,55,60), # red
    (245,180,0), # yellow
    (10,120,190), # navy
    (40,150,100), # green
    (135,75,145), # purple
]
company_colors = [(float(c[0]) / 255.0, float(c[1]) / 255.0, float(c[2]) / 255.0) for c in company_colors]

mapping = {
    2642: (0, 1, 2),
    700: (4, 5),
    1837: (2, 4),
    1935: (0, 1, 4),
    2177: (0,1,2),
    3017: (0,1,2),
    3224: (1, 2, 5),
    3340: (2, 1, 3),
    3425: (0, 1, 2),
    1954: (0,1,2),
    2032: (4,0,5),
    3272: (0,1,2),
    3425: (0,1,2),
    3695: (1,5),
    1791:(1,2,5),
    385 : (1, 5),
    4002: (0,1,2,3)
}


def apply_mask(image, mask, color, alpha=0.5):
    for c in range(3):
        image[:, :, c] = image[:, :, c] * (1 - alpha * mask) + alpha * mask * color[c] * 255
    return image

def apply_mask2(image, mask, color, alpha=0.5):
    """Apply the given mask to the image.
    """
    t= 0.2
    mi = np.min(mask)
    ma = np.max(mask)
    mask = (mask - mi) / (ma - mi)
    for c in range(3):
        image[:, :, c] = image[:, :, c] * (1 - alpha * np.sqrt(mask) * (mask>t))+ alpha * np.sqrt(mask) * (mask>t) * color[c] * 255
    return image

def random_colors(N, bright=True):
    """
    Generate random colors.
    """
    brightness = 1.0 if bright else 0.7
    hsv = [(i / N, 1, brightness) for i in range(N)]
    colors = list(map(lambda c: colorsys.hsv_to_rgb(*c), hsv))
    random.shuffle(colors)
    return colors


def display_instances(image, mask, fname="test", figsize=(5, 5), blur=False, contour=True, alpha=0.5):
    fig = plt.figure(figsize=figsize, frameon=False)
    ax = plt.Axes(fig, [0., 0., 1., 1.])
    ax.set_axis_off()
    fig.add_axes(ax)
    ax = plt.gca()

    N = 1
    mask = mask[None, :, :]
    # Generate random colors
    colors = random_colors(N)

    # Show area outside image boundaries.
    height, width = image.shape[:2]
    margin = 0
    ax.set_ylim(height + margin, -margin)
    ax.set_xlim(-margin, width + margin)
    ax.axis('off')
    masked_image = image.astype(np.uint32).copy()
    for i in range(N):
        color = colors[i]
        _mask = mask[i]
        if blur:
            _mask = cv2.blur(_mask,(10,10))
        # Mask
        masked_image = apply_mask(masked_image, _mask, color, alpha)
        # Mask Polygon
        # Pad to ensure proper polygons for masks that touch image edges.
        if contour:
            padded_mask = np.zeros((_mask.shape[0] + 2, _mask.shape[1] + 2))
            padded_mask[1:-1, 1:-1] = _mask
            contours = find_contours(padded_mask, 0.5)
            for verts in contours:
                # Subtract the padding and flip (y, x) to (x, y)
                verts = np.fliplr(verts) - 1
                p = Polygon(verts, facecolor="none", edgecolor=color)
                ax.add_patch(p)
    try:
        ax.imshow(masked_image.astype(np.uint8), aspect='auto')
    except:
        pass
    fig.savefig(fname)
    # print(f"{fname} saved.")
    return

def show_attn(img, attn_outputs, args,index=None):
    w_featmap = img.shape[-2] // args.patch_size
    h_featmap = img.shape[-1] // args.patch_size
    
    for l in range(len(attn_outputs)):
        attentions =attn_outputs[l] # last layer

        nh = attentions.shape[1] # number of head

        # we keep only the output patch attention
        attentions = attentions[0, :, 0, 1:].reshape(nh, -1)

        if args.threshold is not None:
            # we keep only a certain percentage of the mass
            val, idx = torch.sort(attentions)
            val /= torch.sum(val, dim=1, keepdim=True)
            cumval = torch.cumsum(val, dim=1)
            th_attn = cumval > (1 - args.threshold)
            idx2 = torch.argsort(idx)
            for head in range(nh):
                th_attn[head] = th_attn[head][idx2[head]]
            th_attn = th_attn.reshape(nh, w_featmap, h_featmap).float()
            # interpolate
            th_attn = nn.functional.interpolate(th_attn.unsqueeze(0), scale_factor=args.patch_size, mode="nearest")[0].cpu().numpy()

        attentions = attentions.reshape(nh, w_featmap, h_featmap)
        attentions = nn.functional.interpolate(attentions.unsqueeze(0), scale_factor=args.patch_size, mode="nearest")[0].cpu().numpy()

        if args.output_dir:
            prefix = str(index) if index else ''
            attns = Image.new('RGB', (attentions.shape[2] * nh, attentions.shape[1]))
            for j in range(nh):
                fname = "/tmp/attn-head.png"
                plt.imsave(fname=fname, arr=attentions[j], format='png')
                attns.paste(Image.open(fname), (j * attentions.shape[2], 0))
        
            fname = os.path.join(args.output_dir, prefix + f"attns-{l}.png")
            attns.save(fname)
    
    return attentions, th_attn, attn_outputs[-1]

def show_attn_color(image, attentions, th_attn, index=None, head=[0,1,2,3,4,5]):
    M = image.max()
    m = image.min()
    span = 64
    image = ((image - m) / (M-m)) * span + (256 - span)
    image = image.mean(axis=2)
    image = np.repeat(image[:, :, np.newaxis], 3, axis=2)
    
    for j in head:
        m = attentions[j]
        m *= th_attn[j]
        attentions[j] = m
    mask = np.stack([attentions[j] for j in head])
    
    blur = False
    contour = False
    alpha = 1
    figsize = tuple([i / 100 for i in args.image_size])
    fig = plt.figure(figsize=figsize, frameon=False, dpi=100)
    ax = plt.Axes(fig, [0., 0., 1., 1.])
    ax.set_axis_off()
    fig.add_axes(ax)
    ax = plt.gca()

    if len(mask.shape) == 3:
        N = mask.shape[0]
    else:
        N = 1
        mask = mask[None, :, :]    
    # AJ
    for i in range(N):
        mask[i] = mask[i] * ( mask[i] == np.amax(mask, axis=0))
    a = np.cumsum(mask, axis=0)
    for i in range(N):
        mask[i] = mask[i] * (mask[i] == a[i])
    
    # if imid == 3340:
    #     tmp = company_colors[2]
    #     company_colors[2] = company_colors[1]
    #     company_colors[1] = tmp
    colors = company_colors[:N]
    # colors = random_colors(N)

    # Show area outside image boundaries.
    height, width = image.shape[:2]
    margin = 0
    ax.set_ylim(height + margin, -margin)
    ax.set_xlim(-margin, width + margin)
    ax.axis('off')
    masked_image = 0.1*image.astype(np.uint32).copy()
    for i in range(N):
        color = colors[i]
        _mask = mask[i]
        if blur:
            _mask = cv2.blur(_mask,(10,10))
        # Mask
        masked_image = apply_mask2(masked_image, _mask, color, alpha)
        # Mask Polygon
        # Pad to ensure proper polygons for masks that touch image edges.
        if contour:
            padded_mask = np.zeros(
                (_mask.shape[0] + 2, _mask.shape[1] + 2))#, dtype=np.uint8)
            padded_mask[1:-1, 1:-1] = _mask
            contours = find_contours(padded_mask, 0.5)
            for verts in contours:
                # Subtract the padding and flip (y, x) to (x, y)
                verts = np.fliplr(verts) - 1
                p = Polygon(verts, facecolor="none", edgecolor=color)
                ax.add_patch(p)
    ax.imshow(masked_image.astype(np.uint8), aspect='auto')
    ax.axis('image')
    #fname = os.path.join(output_dir, 'bnw-{:04d}'.format(imid))
    prefix = f'id{index}_' if index is not None else ''
    if args.output_dir:
        fname = os.path.join(args.output_dir, prefix+"attn_color.png")
        fig.savefig(fname)
    return masked_image

from torchvision.transforms.functional import to_pil_image
from torchvision.utils import make_grid
if __name__ == '__main__':
    parser = argparse.ArgumentParser('Visualize Self-Attention maps')
    parser.add_argument('--arch', default='vit_small', type=str, help='Architecture.')
    parser.add_argument('--patch_size', default=16, type=int, help='Patch resolution of the model.')
    parser.add_argument('-w', '--pretrained_weights', default='', type=str, help="""Path to pretrained 
        weights to evaluate. Set to `download` to automatically load the pretrained DINO from url.
        Otherwise the model is randomly initialized""")
    parser.add_argument("--checkpoint_key", default="teacher", type=str,
        help='Key to use in the checkpoint (example: "teacher")')
    parser.add_argument("--image_path", default=None, type=str, help="Path of the single image.")
    parser.add_argument('--data_path', default='imgs', type=str, help='Path of the images\' folder.')
    parser.add_argument("--batch_size", type=int, default=32, help="batch size")
    parser.add_argument("--image_size", default=(480, 480), type=int, nargs="+", help="Resize image.")
    parser.add_argument('--output_dir', default='', help='Path where to save visualizations.')
    parser.add_argument("--show_pics", type=int, default=100)
    parser.add_argument("--threshold", type=float, default=0.6, help="""We visualize masks
        obtained by thresholding the self-attention maps to keep xx% of the mass.""")
    args = parser.parse_args()

    misc.fix_random_seeds(0)

    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
    # build model
    model = build_model(
        patch_size=args.patch_size, 
        num_classes=0)
    embed_dim = model.embed_dim
    model.requires_grad_(False)
    model.eval()
    model.to(device)
    
    
    load_pretrained_weights(model, args.pretrained_weights, args.checkpoint_key)

    transform = pth_transforms.Compose([
        pth_transforms.Resize(args.image_size),
        pth_transforms.ToTensor(),
        pth_transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225)),
    ])
    
    def denormalize(imagesToPrint):
        imagesToPrint = imagesToPrint.clone()
        imagesToPrint *= torch.Tensor((0.229, 0.224, 0.225),device=imagesToPrint.device).reshape(3,1,1)
        imagesToPrint += torch.Tensor((0.485, 0.456, 0.406),device=imagesToPrint.device).reshape(3,1,1)
        return imagesToPrint.clamp(0,1)

    
    from utils import hook
    attn_hook = hook.Hook(model)
    
    if args.output_dir:
        os.makedirs(args.output_dir,exist_ok=True)
    if args.image_path is not None and os.path.isfile(args.image_path):
        
        with open(args.image_path, 'rb') as f:
            img = Image.open(f)
            img = img.convert('RGB')

        img = transform(img)
        # make the image divisible by the patch size
        w, h = img.shape[1] - img.shape[1] % args.patch_size, img.shape[2] - img.shape[2] % args.patch_size
        img = img[:, :w, :h].unsqueeze(0)
        
        _,attn_outputs = attn_hook(img.to(device))

        attentions, th_attn, pic_attn = show_attn(img,attn_outputs,args)
        pic_i =((denormalize(img[0]).permute(1,2,0)*255).numpy().astype(np.uint8))
        
        show_attn_color(pic_i, attentions, th_attn,)
        
    
    else:
        train_dataloader = DataLoader(
            ImageFolder(args.data_path, transform=transform), 
            batch_size=args.batch_size, 
            shuffle=True, 
            num_workers=4, 
            pin_memory=True)

        cnt = 0
        for data in tqdm(train_dataloader):
            imgs,_ = data
            
            for i in range(imgs.shape(0)):
                idx = cnt
                img = imgs[i:i+1]
                _,attn_outputs = attn_hook(img.to(device))
                pic_i = (denormalize(img[0]).permute(2,0,1)*255).numpy().astype(np.uint8)
                attentions, th_attn, pic_i, pic_attn = show_attn(img, index=idx)
                pic_attn_color = show_attn_color(img[0].permute(1, 2, 0).cpu().numpy(), attentions, th_attn, index=idx)
                cnt += 1
                if cnt == args.show_pics:
                    sys.exit(0)