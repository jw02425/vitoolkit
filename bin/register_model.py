import json
import os
import wandb
import random
import torch
import argparse
import re

def drop_key(state_dict, dkey):
    for key in list(state_dict.keys()):
        if re.match(dkey, key):
            state_dict.pop(key)
    return state_dict

def strip_weights(pretrained_weights, drop_keys=[],cmds=[]):
    """load vit weights"""
    if pretrained_weights == '':
        return
    elif pretrained_weights.startswith('https'):
        state_dict = torch.hub.load_state_dict_from_url(
            pretrained_weights, map_location='cpu', check_hash=True)
    else:
        state_dict = torch.load(pretrained_weights, map_location='cpu')
    
    while True:
        for dkey in drop_keys:
            drop_key(state_dict, dkey)
                
        print("Available keys: ", state_dict.keys(), )
        if cmds:
            cmd = cmds.pop(0)
        else:
            cmd = input("Enter command: u (upload), e:key (extract key), q (quit), d:key (delete key), c:key (cut prefix)\n")
        if cmd == "q":
            return None
        elif cmd == "u":
            return state_dict
        elif cmd.startswith("e"):
            key = cmd[2:]
            if key in state_dict:
                state_dict = state_dict[key]
            else:
                print("Key not found")
        elif cmd.startswith("d"):
            dkey = cmd[2:]
            drop_key(state_dict, dkey)
        
        elif cmd.startswith("c"):
            prefix = cmd[2:]
            state_dict = {k[len(prefix):]:v for k,v in state_dict.items() if k.startswith(prefix)}
    return state_dict
            
class StoreDictKeyPair(argparse.Action):
     def __init__(self, option_strings, dest, nargs=None, **kwargs):
         self._nargs = nargs
         super(StoreDictKeyPair, self).__init__(option_strings, dest, nargs=nargs, **kwargs)
     def __call__(self, parser, namespace, values, option_string=None):
         my_dict = {}
         for kv in values:
             k,v = kv.split("=")
             my_dict[k] = v
         setattr(namespace, self.dest, my_dict)
         
if __name__ == '__main__':

    parser = argparse.ArgumentParser('Register Model')
    parser.add_argument("name", type=str, help='Model name.')
    parser.add_argument('pretrained_weights', type=str, help="""Path to pretrained 
        weights to evaluate. Set to `download` to automatically load the pretrained DINO from url.""")            
    parser.add_argument("--description", type=str, help='Model description.')
    parser.add_argument("--metadata",  dest="metadata", action=StoreDictKeyPair, help='Model metadata.',  nargs="+", metavar="KEY=VAL")
    parser.add_argument("--drop_list", default=["^head"], nargs='+', help='Key to drop in checkpoint.')
    parser.add_argument("--cmds", default=[], nargs='+', help='Command to execute in order.')
    parser.add_argument("--output_dir", default=".", type=str, help='register model in wandb by wandb://<project>/<model_name>, save weights in local by <path>.')
    
    args = parser.parse_args()
    print(args.__dict__)
    
    import glob
        
    state_dict = strip_weights(args.pretrained_weights, args.drop_list,cmds=args.cmds)
    if state_dict is None:
        print("No state dict found")
        exit(0)
    print("Parameter size: {:,}".format(sum(p.numel() for p in state_dict.values())))
    
    # Start a new W&B run
    if args.output_dir.startswith("wandb://"):
        with wandb.init(job_type='model',name=f"model-{args.name}") as run:

            # Save the dummy model to W&B
            artifact = wandb.Artifact(args.name, type='model', description=args.description, metadata=args.metadata)
            file_path = "/tmp/weights.pth"
            torch.save(state_dict, file_path)
            artifact.add_file(file_path)
            run.log_artifact(artifact)

            # Link the model to the Model Registry
            run.link_artifact(artifact, args.output_dir)
            run.finish()
    else:        
        os.makedirs(args.output_dir, exist_ok=True)
        torch.save(state_dict, os.path.join(args.output_dir,"weights.pth"))
        meta = dict(description=args.description, 
                    metadata=args.metadata,
                    name=args.name,)
        json.dump(meta, open(os.path.join(args.output_dir, "model.json"), "w"))
        print(f"Model saved to {args.output_dir}")