# Copyright (c) Facebook, Inc. and its affiliates.
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import json
import os
from pathlib import Path
import argparse
import pandas as pd

import torch
from torch import nn
import torch.distributed as dist
import torch.backends.cudnn as cudnn
from torchvision import transforms as pth_transforms
from vitookit.datasets import build_dataset

from vitookit.utils.helper import load_pretrained_weights
import vitookit.utils
import vitookit.models


def extract_feature_pipeline(args):
    # ============ preparing data ... ============
    transform = pth_transforms.Compose([
        pth_transforms.Resize(256, interpolation=3),
        pth_transforms.CenterCrop(224),
        pth_transforms.ToTensor(),
        pth_transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225)),
    ])
    dataset_train = ReturnIndexDatasetWrap( build_dataset(args,True,transform)[0])
    dataset_val = ReturnIndexDatasetWrap(build_dataset(args,False,transform)[0])
    sampler = torch.utils.data.DistributedSampler(dataset_train, shuffle=False)

    data_loader_val = torch.utils.data.DataLoader(
        dataset_val,
        batch_size=args.batch_size_per_gpu,
        num_workers=args.num_workers,
        pin_memory=True,
        drop_last=False,
    )
    print(f"Data loaded with {len(dataset_train)} train and {len(dataset_val)} val imgs.")

    # ============ building network ... ============
    if 'swin' in args.arch:
        args.patch_size = 4
        model = models.__dict__[args.arch](
            window_size=args.window_size,
            patch_size=args.patch_size,
            num_classes=0)
    else:
        model = models.__dict__[args.arch](
            patch_size=args.patch_size, 
            head_type=args.head_type,
            num_classes=0)
    
    print(f"Model {args.arch} {args.patch_size}x{args.patch_size} built.")
    model.cuda()
    load_pretrained_weights(model, args.pretrained_weights, args.checkpoint_key )
    model.eval()

    # ============ extract features ... ============
    print("Extracting features for val set...")
    test_features = extract_features(model, data_loader_val,True)

    # if misc.get_rank() == 0:
    #     train_features = nn.functional.normalize(train_features, dim=1, p=2)
    #     test_features = nn.functional.normalize(test_features, dim=1, p=2)

    test_labels = torch.tensor(list(dataset_val.get_labels())).long()

    return test_features, test_labels


@torch.no_grad()
def extract_features(model, data_loader, use_cuda=True, multiscale=False):
    metric_logger = misc.MetricLogger(delimiter="  ")
    features = None
    for samples, index in metric_logger.log_every(data_loader, 10):
        samples = samples.cuda(non_blocking=True)
        index = index.cuda(non_blocking=True)
        if multiscale:
            feats = misc.multi_scale(samples, model)
        else:
            feats = model(samples).clone()

        # init storage feature matrix
        if dist.get_rank() == 0 and features is None:
            features = torch.zeros(len(data_loader.dataset), feats.shape[-1])
            if use_cuda:
                features = features.cuda(non_blocking=True)
            print(f"Storing features into tensor of shape {features.shape}")

        # get indexes from all processes
        y_all = torch.empty(dist.get_world_size(), index.size(0), dtype=index.dtype, device=index.device)
        y_l = list(y_all.unbind(0))
        y_all_reduce = torch.distributed.all_gather(y_l, index, async_op=True)
        y_all_reduce.wait()
        index_all = torch.cat(y_l)

        # share features between processes
        feats_all = torch.empty(
            dist.get_world_size(),
            feats.size(0),
            feats.size(1),
            dtype=feats.dtype,
            device=feats.device,
        )
        output_l = list(feats_all.unbind(0))
        output_all_reduce = torch.distributed.all_gather(output_l, feats, async_op=True)
        output_all_reduce.wait()

        # update storage feature matrix
        if dist.get_rank() == 0:
            if use_cuda:
                features.index_copy_(0, index_all, torch.cat(output_l))
            else:
                features.index_copy_(0, index_all.cpu(), torch.cat(output_l).cpu())
    return features

class ReturnIndexDatasetWrap():
    def __init__(self,data) -> None:
        self.data=data
    
    def get_labels(self):
        for i in range(len(self.data)):
            yield self.data[i][1]
                
    def __len__(self):
        return len(self.data)
    
    def __getitem__(self, idx):
        img, lab = self.data[idx]
        return img, idx

def get_parser():
    parser = argparse.ArgumentParser('Evaluation with weighted k-NN on ImageNet')
    parser.add_argument('--batch_size_per_gpu', default=128, type=int, help='Per-GPU batch-size')
    parser.add_argument('--nb_knn', default=[1, 10,20,50,100], nargs='+', type=int,
        help='Number of NN to use. 10 is usually working the best for small datasets and 20 for large datasets.')
    parser.add_argument('--temperature', default=0.07, type=float,
        help='Temperature used in the voting coefficient')
    parser.add_argument('-w', '--pretrained_weights', default='', type=str, help="Path to pretrained weights to evaluate.")
    parser.add_argument('--use_cuda', default=True, type=utils.bool_flag,
        help="Should we store the features on GPU? We recommend setting this to False if you encounter OOM")
    parser.add_argument('--arch', default='vit_small', type=str, help='Architecture')
    parser.add_argument('--patch_size', default=16, type=int, help='Patch resolution of the model.')
    parser.add_argument("--checkpoint_key", default=None, type=str,
        help='Key to use in the checkpoint (example: "teacher")')
    parser.add_argument('--dump_features', default=None,
        help='Path where to save computed features, empty for no saving')
    parser.add_argument('--load_features', default=None, help="""If the features have
        already been computed, where to find them.""")
    parser.add_argument('--num_workers', default=10, type=int, help='Number of data loading workers per GPU.')
    parser.add_argument("--dist_url", default="env://", type=str, help="""url used to set up
        distributed training; see https://pytorch.org/docs/stable/distributed.html""")
    parser.add_argument("--local_rank", default=0, type=int, help="Please ignore and do not set this argument.")
    parser.add_argument('--data_location', default='./data', type=str)
    parser.add_argument('--data_set', default='Pets', type=str)
    parser.add_argument('--output_dir',default='',type=str,help='path where to save, empty for no saving')
    parser.add_argument('--dis_fn',default='cosine', type=str, 
                        choices=['cosine','euclidean'])
    
    parser.add_argument('--bn',default=False,action='store_true', help="Apply batch normalization after extracting features. This is neccessary for MAE.")
    parser.add_argument('--head_type', default=0, choices=[0, 1, 2], type=int,
        help="""How to aggress global information.
        We typically set this to 0 for models with [CLS] token (e.g., DINO), 1 for models encouraging patch semantics e.g. BEiT, 2 for combining mean pool and CLS. 2works well for all cases. """)
    
    return parser
if __name__ == '__main__':
    
    
    parser = get_parser()
    
    args = aug_parse(parser)

    misc.init_distributed_mode(args)
    print("git:\n  {}\n".format(utils.get_sha()))
    print("\n".join("%s: %s" % (k, str(v)) for k, v in sorted(dict(vars(args)).items())))
    cudnn.benchmark = True

    test_features, test_labels = extract_feature_pipeline(args)

    
    if misc.get_rank() == 0:
        test_features, test_labels = test_features.cpu().numpy(), test_labels.cpu().numpy()
        if args.bn:
            bn = torch.nn.BatchNorm1d(test_features.shape[1], affine=False, eps=1e-6).cuda()
            test_features = bn(test_features)
        
        print("Features are ready!")
        
        import numpy as np
            
        from sklearn.manifold import TSNE
        import matplotlib.pyplot as plt

        # Apply t-SNE to reduce the dimensions to 2D
        tsne = TSNE(n_components=2, random_state=42)
        reduced_features = tsne.fit_transform(test_features)

        # Assuming you have a list or array of labels called 'labels'
        
        unique_labels = np.unique(test_labels)
        colors = plt.cm.jet(np.linspace(0, 1, len(unique_labels)))

        for i, label in enumerate(unique_labels):
            plt.scatter(reduced_features[test_labels == label, 0], reduced_features[test_labels == label, 1], color=colors[i], label=label)

        plt.xlabel('Component x')
        plt.ylabel('Component y')
        plt.title('t-SNE Dimensionality Reduction')
        
        if args.output_dir:
            output_dir=Path(args.output_dir)
            output_dir.mkdir(parents=True, exist_ok=True)
        else:
            output_dir = None
        
        # test_features, test_labels = test_features[:100], test_labels[:100]
        if output_dir:
            job_name = os.path.basename(__file__)
            plt.savefig(output_dir / "tsne.pdf")
        
            try:
                import wandb
                logger = wandb.init(dir=args.output_dir, resume=True)
            except:
                
                print("fail to init wandb")
                        
            # Create a "target" column
            df = pd.DataFrame(test_features,columns=[f"f{i}" for i in range(test_features.shape[1])])
            df["label"] = pd.Series(test_labels.astype(str))
            cols = df.columns.tolist()
            df = df[cols[-1:] + cols[:-1]]
            
            wandb.summary[f"projection/{args.data_set}"]= wandb.Table(data=df)
        else:
            plt.show()    
            


            
