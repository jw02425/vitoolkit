#!python
import time
import argparse
from sklearn.cluster import KMeans
import torch,math,os,torchvision,random, einops
from torch import nn
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import torch.nn.functional as F
import torchvision.transforms as tfms
import torchvision.transforms.functional as tfF
from torchvision.utils import *
from vitookit.utils.helper import load_pretrained_weights
import time
import cv2
import vitookit.models
from analyze.attention_map.visualize_attention import show_attn_color, show_attn, display_instances


if __name__ == '__main__':
    parser = argparse.ArgumentParser('visualize the Attention Map')
    parser.add_argument('--patch_size', default=16, type=int, help='Patch resolution of the model.')
    parser.add_argument('--arch', default='vit_small', type=str, help='Architecture.')
    parser.add_argument('-w', '--pretrained_weights', default='', type=str, help="""Path to pretrained 
        weights to evaluate. Set to `download` to automatically load the pretrained DINO from url.
        Otherwise the model is randomly initialized""")
    parser.add_argument("--checkpoint_key", default=None, type=str,
        help='Key to use in the checkpoint (example: "teacher")')
    parser.add_argument("--cluster_number", default=6,type=int)
    parser.add_argument('--img', help="The target image.")
    
    args = parser.parse_args()
    
    
    ## load model
    model = models.__dict__[args.arch](
        patch_size=args.patch_size, 
        num_classes=0)
    embed_dim = model.embed_dim
    model.requires_grad_(False)
    model.eval()
    
    load_pretrained_weights(model, args.pretrained_weights, args.checkpoint_key)
    ## load data
    from PIL import Image
    img = Image.open(args.img)
    
    from torchvision import datasets, transforms
    IMAGENET_DEFAULT_MEAN = (0.485, 0.456, 0.406)
    IMAGENET_DEFAULT_STD = (0.229, 0.224, 0.225)
    tfms=transforms.Compose([
        transforms.Resize(224),
        transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize(IMAGENET_DEFAULT_MEAN, IMAGENET_DEFAULT_STD),
    ])
    def denormalize(imagesToPrint):
        imagesToPrint = imagesToPrint.clone()
        imagesToPrint *= torch.Tensor(IMAGENET_DEFAULT_STD,device=imagesToPrint.device).reshape(3,1,1)
        imagesToPrint += torch.Tensor(IMAGENET_DEFAULT_MEAN,device=imagesToPrint.device).reshape(3,1,1)
        return imagesToPrint.clamp(0,1)


    ## get representations
    img = tfms(img)
    image = denormalize(img)
    image_np = (image.permute(1,2,0)*255).numpy().astype(np.uint8)
    x = img[None,:]

    from utils import hook
    attn_hook = hook.Hook(model)
    _,attns = attn_hook(x)
    block_hook = hook.Hook(model,'blocks.11')
    _,blocks = block_hook(x)

    patch_tokens = blocks[-1][:,1:]
    
    ## normalize patch tokens
    patch_tokens = model.norm(patch_tokens).flatten(0,1)
    patch_tokens = nn.functional.normalize(patch_tokens, dim=-1, p=2)
    
    s = patch_tokens.matmul(patch_tokens.T)
    s = einops.rearrange(s,'(l1 l2) (p1 p2)->l1 l2 p1 p2',l1=14,p1=14)

    
    
    attn= attns[-1][0] # num_head, N, N
    num_heads = attn.shape[0]
    cls_attn = attn[:,0,1:].reshape(-1, 14,14)
    
    attn = attn[:,1:,1:]
    attn = einops.rearrange(attn,'n (l1 l2) (p1 p2)->n (l1 l2) p1 p2',l1=14,p1=14)
    attn=attn.mean(0)
    print(attn.shape)


    x_origin = image.permute(1,2,0)
    
    fig, axes = plt.subplots(2,3,figsize=(12,9))
    axes=axes.flatten()
    for ax in axes:
        ax.axis('off')
    implot = axes[0].imshow(x_origin)
    axes[1].imshow(attn[0])

    axes[0].set_title('input')
    axes[1].set_title('attn to')
    axes[2].set_title('attn from')
    axes[3].set_title('similarity')
    plt.tight_layout()
    
    
    ## colorized attention maps for head
    #################### multi head colorization ##############
    threathold=0.8
    attentions, th_attn = show_attn(img,attns[-1][0],threathold)
    show_attn_color(image, attentions, th_attn,ax=axes[4])
    
    #################### feature cluster ####################
    
    x_patches=patch_tokens.numpy()
    
    k_means = KMeans(args.cluster_number)
    clusters = k_means.fit_predict(x_patches)
    x_features = k_means.transform(x_patches)
    
    
    vmin,vmax =x_features.min(),x_features.max()
    x_features = (x_features-vmin)/(vmax-vmin)        
    x_features = x_features.T
    
    select_f = x_features.argmax(0)
    overlay = []
    th_attn = []
    for i in range(len(x_features)):
        attn = cv2.resize(x_features[i].reshape(14,14),image.shape[1:])
        mask =  cv2.resize((select_f==i).astype(float).reshape(14,14),image.shape[1:])
        overlay.append(attn*mask)
    
    overlay = np.stack(overlay)
    display_instances(image_np, overlay, ax=axes[5],alpha=0.5)
    
    
    
    def onclick(event):
        pi = int(event.xdata)//16
        pj = int(event.ydata)//16
        
        print('%s click: button=%d, pi=%d, pj=%d, xdata=%f, ydata=%f' %
            ('double' if event.dblclick else 'single', event.button,
            pi, pj, event.xdata, event.ydata))
        x_img = x_origin.clone()
        x_img[pj*16:pj*16+16, pi*16:pi*16+16]*=1.5
        
        axes[0].imshow(x_img.clamp(0,1))
        axes[1].imshow(attn[:, pj, pi].reshape(14,14))
        axes[2].imshow(attn[pj*14+pi])
        axes[3].clear()
        sns.heatmap((s[pj,pi]*100).int(),ax=axes[3],cbar=False,annot=True)
        plt.show()

    cid =  implot.figure.canvas.mpl_connect('button_press_event', onclick)
    plt.show()
    
    
model_list=['ibot-ViT_S','cssl-ViT_S','dino-ViT_S','mae-800']