#!python
#%%
"""
for img in img_list:
   %run bin/viz_clusters.py --arch vit_base --pretrained_weights ../SiT/outputs/imagenet/sit-ViT_B/checkpoint.pth --img ../data/ImageNet/val/{img} --cluster_number=10 --output_dir=outputs/pics/clusters/{img}-sit-10.jpg
"""
from PIL import Image
import time
import argparse
import cv2
import pandas as pd
from sklearn.cluster import KMeans
import torch,math,os,torchvision,random, einops
from torch import nn
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import torch.nn.functional as F
import torchvision.transforms as tfms
import torchvision.transforms.functional as tfF
from torchvision.utils import *
from vitookit.utils.helper import load_pretrained_weights
import time
import vitookit.models

from torchvision import datasets

min_object = 0
def get_args(*args,):
    parser = argparse.ArgumentParser(add_help="--arch=vit_base --pretrained_weight <model>.pth")
    parser.add_argument('--patch_size', default=16, type=int, help='Patch resolution of the model.')
    parser.add_argument("--image_size", default=(480, 480), type=int, nargs="+", help="Resize image.")
    parser.add_argument('--arch', default='vit_small', type=str, help='Architecture.')
    parser.add_argument('-w', '--pretrained_weights', default='', type=str, help="""Path to pretrained 
        weights to evaluate. Set to `download` to automatically load the pretrained DINO from url.
        Otherwise the model is randomly initialized""")
    parser.add_argument("--checkpoint_key", default=None, type=str,
        help='Key to use in the checkpoint (example: "teacher")')
    
    parser.add_argument("--acc_itr",default=1000,type=int,help="the number of batches to accumulate distributions. -1 for all dataset.")
    parser.add_argument('--bn', action='store_true', default=False,)
    
    parser.add_argument(
        '--output_dir',
        type=str,
        default='',
        help='output image path')
    
    parser.add_argument('--use-cuda', action='store_true', default=True,
                        help='Use NVIDIA GPU acceleration')
    
    args = parser.parse_args(*args)
    
    args.use_cuda = args.use_cuda and torch.cuda.is_available()
    if args.use_cuda:
        print('Using GPU for acceleration')
    else:
        print('Using CPU for computation')
        
    return args

from pycocotools import mask as maskUtils


def annToRLE(ann, height, width):
    """
    Convert annotation which can be polygons, uncompressed RLE to RLE.
    :return: binary mask (numpy 2D array)
    """
    segm = ann['segmentation']
    if isinstance(segm, list):
        # polygon -- a single object might consist of multiple parts
        # we merge all parts into one mask rle code
        rles = maskUtils.frPyObjects(segm, height, width)
        rle = maskUtils.merge(rles)
    elif isinstance(segm['counts'], list):
        # uncompressed RLE
        rle = maskUtils.frPyObjects(segm, height, width)
    else:
        # rle
        rle = ann['segmentation']
    return rle

def annToMask( ann, height, width):
    """
    Convert annotation which can be polygons, uncompressed RLE, or RLE to binary mask.
    :return: binary mask (numpy 2D array)
    """
    rle = annToRLE(ann, height, width)
    m = maskUtils.decode(rle)
    return m

class COCO(datasets.CocoDetection):
    def __getitem__(self, index: int):
        img,annotations= super().__getitem__(index)
        
        width,height = img.size
        
        instance_masks = [np.zeros((hw,nh))]
        class_masks = [np.zeros((hw,nh))]
        ins_set = set()
        class_set = set()
        for annotation in annotations:
            m = annToMask(annotation, height,
                                width)
            m = cv2.resize(m,(hw,nh))
            
            # Some objects are so small that they're less than 3 patches
            # and end up rounded out. Skip those objects.
            ins_id = annotation['id']
            class_id = annotation['category_id']
            # The  id  field is a unique identifier assigned to each individual object in the dataset. 
            # On the other hand,  category_id  is the identifier for the category or class that the object belongs to.
            
            instance_masks.append(m*ins_id)
            class_masks.append(m*class_id)
            ins_set.add(ins_id)
            class_set.add(class_id)
        
        # merge the masks
        instance_mask = np.stack(instance_masks).max(0)
        class_mask = np.stack(class_masks).max(0)
        
        return tfms(img), instance_mask.astype(np.int32), class_mask


import numpy as np

def get_density_histograms(p_sim, mask, mask_id, bin_edges, hist_intra, hist_inter):
    """
    Computes histograms of intra- and inter-class density values from input similarity data.
    Parameters:
        p_sim (numpy array): similarity data of shape (N, N), where N is the number of instances
        mask (numpy array): mask indicating class labels of each instance
        bin_edges (numpy array): bin edges for histogram computation
    """
    # Get unique values from the mask
    
    label = mask == mask_id
    if label.sum() < min_object: return
    
    l1 = label.reshape(N, 1)
    l2 = label.reshape(1, N)
    l_intra = l1 & l2
    l_inter = l1 & (~l2)
        # Calculate the histogram for the intra-mask elements
    hist, _ = np.histogram(p_sim[l_intra], bin_edges)
    hist_intra += hist
    hist, _ = np.histogram(p_sim[l_inter], bin_edges)
    hist_inter += hist
     # Return the calculated histograms
    return  hist_intra, hist_inter
        
if __name__ == '__main__':
    import sys
    args = get_args()
    ## load model
    if 'sam' in args.arch:
        import vitookit.models.image_encoder as models
        args.image_size=(1024,1024)
        model = models.__dict__[args.arch](image_size=args.image_size[0] )
        state = torch.load(args.pretrained_weights,"cpu")
        state = {k.replace("image_encoder.","") :v for k,v in state.items() if "image_encoder" in k }
        print(model.load_state_dict(state))
    else:
        model = models.__dict__[args.arch](
            patch_size=args.patch_size, 
            num_classes=0)
        load_pretrained_weights(model, args.pretrained_weights, args.checkpoint_key)
    
    model.eval().requires_grad_(False)
    
    
    mmc = models.__dict__[args.arch](
            patch_size=args.patch_size, 
            num_classes=0)
    load_pretrained_weights(mmc, "outputs/models/MMC/checkpoint.pth", args.checkpoint_key)
    mmc.eval().requires_grad_(False).cuda()
    
    from torchvision import datasets, transforms
    IMAGENET_DEFAULT_MEAN = (0.485, 0.456, 0.406)
    IMAGENET_DEFAULT_STD = (0.229, 0.224, 0.225)
    
    common_tf = transforms.Compose([
        # transforms.CenterCrop(args.image_size),
        transforms.Resize(args.image_size),
        ])
    
    tfms=transforms.Compose([
        common_tf,
        transforms.ToTensor(),
        transforms.Normalize(IMAGENET_DEFAULT_MEAN, IMAGENET_DEFAULT_STD),
    ])
    def denormalize(imagesToPrint):
        imagesToPrint = imagesToPrint.clone()
        imagesToPrint *= torch.Tensor(IMAGENET_DEFAULT_STD,device=imagesToPrint.device).reshape(3,1,1)
        imagesToPrint += torch.Tensor(IMAGENET_DEFAULT_MEAN,device=imagesToPrint.device).reshape(3,1,1)
        return imagesToPrint.clamp(0,1)
    
    #%% ## get representations
    

    from scipy.stats import mode
    data = COCO('/vol/research/datasets/still/MSCOCO/images/train2017','/vol/research/datasets/still/MSCOCO/annotations/instances_train2017.json',)
    
    nh,hw = args.image_size[0]//args.patch_size, args.image_size[1]//args.patch_size
    
    dl = torch.utils.data.DataLoader(data,3,num_workers=10,shuffle=False,drop_last=True)
    
    #%% load 
    import tqdm
    model.cuda()
    bin_edges = np.linspace(-0.5,0.99,101)
    hist_dict= {'inter_density_class':np.zeros((100,100)),
                'intra_density_class':np.zeros((100,100)),
                'inter_density_ins':np.zeros((100,100)),
                'intra_density_ins':np.zeros((100,100)),
                'bin_edges':bin_edges,}
    
    acc_itr = 0
    
    min_overlap_gap=1
    for x, instance_mask, class_mask in tqdm.tqdm(dl):
        
        instance_mask, class_mask = instance_mask.numpy(), class_mask.numpy()
        
        ## calculate similarity
        patch_tokens = model.forward_features(x.cuda())[:,1:] # B, N - 1, C
        patch_tokens = nn.functional.normalize(patch_tokens, dim=-1, p=2)
        
        p_sim = patch_tokens.matmul(patch_tokens.transpose(2,1)).cpu() # B, N, N
        B,N,_ = p_sim.shape
        
        ## MMC
        patch_tokens = mmc.forward_features(x.cuda())[:,1:] # B, N - 1, C
        patch_tokens = nn.functional.normalize(patch_tokens, dim=-1, p=2)
        
        p_sim_mmc = patch_tokens.matmul(patch_tokens.transpose(2,1)).cpu() # B, N, N
        
        for i in range(B):
            ## hist similarity for class-wise objects      
            mask = class_mask[i].flatten() # N
            tset = np.unique(mask)
            for mask_id in tset:
                mask_id = int(mask_id)
                if mask_id == 0:
                    continue
                
                hist_intra, hist_inter = get_density_histograms(p_sim[i],mask,mask_id , bin_edges, 
                                        hist_dict['intra_density_class'][mask_id],hist_dict['inter_density_class'][mask_id])
                p_intra = hist_intra/(1e-6+hist_intra.sum())
                p_inter = hist_inter/(1e-6+hist_inter.sum())
                overlap = np.stack([p_intra,p_inter]).min(0).sum()
                
                ## MMC
                hist_intra, hist_inter = get_density_histograms(p_sim_mmc[i],mask,mask_id , bin_edges, 
                                        hist_dict['intra_density_class'][0],hist_dict['inter_density_class'][0])
                p_intra = hist_intra/(1e-6+hist_intra.sum())
                p_inter = hist_inter/(1e-6+hist_inter.sum())
                overlap_mmc = np.stack([p_intra,p_inter]).min(0).sum()
                
                if (overlap_mmc-overlap) < -0.20:
                    min_overlap_gap = overlap_mmc-overlap
                    
                    s = f"overlap_mmc={overlap_mmc:.3f}, overlap={overlap:.3f}, min_overlap_gap={min_overlap_gap:.3f},img_idx={acc_itr}, mask_id={mask_id}\n"
                    print(s)
                    with open(os.path.join(args.output_dir,"min_overlap_gap.txt"),"a") as f:
                        f.write(s)
                
        acc_itr+=1
        # if acc_itr>args.acc_itr: break
        
        
    #################### visualization ########################
    #%%
    # Create a figure with subplots for each cluster
    plt.style.use('seaborn')
    fig,axes = plt.subplots(1,2,figsize=(6.4,3))
    bin_centers = (bin_edges[:-1] + bin_edges[1:]) / 2
    span = (bin_edges.max()-bin_edges.min())
    step = -(bin_edges[:-1] - bin_edges[1:]).mean()
    # Plot the PDF
    # for k,v in hist_dict.items():
    #     hist_dict[k] = v.astype(np.float64)/(1e-6+v.sum(-1))/step
        
    overlap_ins = np.stack([hist_dict['intra_density_ins'],hist_dict['inter_density_ins']]).min(0)
    overlap_class = np.stack([hist_dict['intra_density_class'],hist_dict['inter_density_class']]).min(0) 
    
    
    #%%
    
    # Distribution of similarity between inter-object patches vs. intra-object patches
    if args.output_dir:
        os.makedirs(args.output_dir,exist_ok=True)
        np.save(os.path.join(args.output_dir,"hist_dict.npy"),hist_dict,allow_pickle=True)
        
    else:
        print(overlap_class.sum(-1))
        print(overlap_ins.sum(-1))
