#!python
"""
for img in img_list:
   %run bin/viz_clusters.py --arch vit_base --pretrained_weights ../SiT/outputs/imagenet/sit-ViT_B/checkpoint.pth --img ../data/ImageNet/val/{img} --cluster_number=10 --output_img=outputs/pics/clusters/{img}-sit-10.jpg
"""
from PIL import Image
import time
import argparse
import cv2
from sklearn.cluster import KMeans
import torch,math,os,torchvision,random, einops
from torch import nn
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import torch.nn.functional as F
import torchvision.transforms as tfms
import torchvision.transforms.functional as tfF
from torchvision.utils import *
from vitookit.utils.helper import load_pretrained_weights
import time
import vitookit.models
from analyze.attention_map.visualize_attention import *
from misc.hook import Hook

def get_args(*args,):
    parser = argparse.ArgumentParser(add_help="bin/clusters.py --arch vit_base --pretrained_weights ../SiT/outputs/imagenet/sit-ViT_B/checkpoint.pth --img imgs/sample4.png")
    parser.add_argument('--patch_size', default=16, type=int, help='Patch resolution of the model.')
    parser.add_argument("--image_size", default=(480, 480), type=int, nargs="+", help="Resize image (h w).")
    parser.add_argument('--arch', default='vit_small', type=str, help='Architecture.')
    parser.add_argument('-w', '--pretrained_weights', default='', type=str, help="""Path to pretrained 
        weights to evaluate. Set to `download` to automatically load the pretrained DINO from url.
        Otherwise the model is randomly initialized""")
    parser.add_argument("--checkpoint_key", default=None, type=str,
        help='Key to use in the checkpoint (example: "teacher")')

    parser.add_argument("--key")

    parser.add_argument(
        '--img',
        type=str,
        default='./imgs/sample1.JPEG',
        help='Input image path')
    
    parser.add_argument(
        '--output_img',
        type=str,
        default='',
        help='output image path')
    
    
    parser.add_argument('--use-cuda', action='store_true', default=True,
                        help='Use NVIDIA GPU acceleration')
    
    
    parser.add_argument('--cluster_number',type=int, default=3)
    
    args = parser.parse_args(*args)
    
    args.use_cuda = args.use_cuda and torch.cuda.is_available()
    if args.use_cuda:
        print('Using GPU for acceleration')
    else:
        print('Using CPU for computation')
        
    return args

if __name__ == '__main__':
    
    args = get_args()
    ## load model
    model = models.__dict__[args.arch](
        patch_size=args.patch_size, 
        num_classes=0)
    embed_dim = model.embed_dim
    model.eval().requires_grad_(False)
    
    load_pretrained_weights(model, args.pretrained_weights, args.checkpoint_key)
    ## load data
    from PIL import Image
    img = Image.open(args.img)
    
    
    from torchvision import datasets, transforms
    IMAGENET_DEFAULT_MEAN = (0.485, 0.456, 0.406)
    IMAGENET_DEFAULT_STD = (0.229, 0.224, 0.225)
    tfms=transforms.Compose([
        transforms.Resize(args.image_size),
        transforms.ToTensor(),
        transforms.Normalize(IMAGENET_DEFAULT_MEAN, IMAGENET_DEFAULT_STD),
    ])
    def denormalize(imagesToPrint):
        imagesToPrint = imagesToPrint.clone()
        imagesToPrint *= torch.Tensor(IMAGENET_DEFAULT_STD,device=imagesToPrint.device).reshape(3,1,1)
        imagesToPrint += torch.Tensor(IMAGENET_DEFAULT_MEAN,device=imagesToPrint.device).reshape(3,1,1)
        return imagesToPrint.clamp(0,1)


    ## get representations
    img = tfms(img)
    image = (denormalize(img).permute(1,2,0)*255).numpy().astype(np.uint8)
    x = img[None,:]
    
    
    patch_tokens = model.forward_features(x)[0,1:] # N - 1, C
    patch_tokens = nn.functional.normalize(patch_tokens, dim=-1, p=2)
    
    x_patches=patch_tokens.numpy()
    
    num_clusters = args.cluster_number
    
    #################### clusters ########################
    k_means = KMeans(num_clusters)
    cluster_assignments = k_means.fit_predict(x_patches)
    
    nh,hw = args.image_size[0]//args.patch_size, args.image_size[1]//args.patch_size
    
    cluster_assignments_2d = np.reshape(cluster_assignments, (nh,hw))
    #################### similarity ########################

    sim = patch_tokens.mm(patch_tokens.T).numpy()
    
    px,py = 13,15
    s = sim[px+hw*py].reshape(nh,hw) # center object
    s = cv2.resize(s.astype(float),image.shape[1::-1],3)
    

    # Create a figure with subplots for each cluster
    
    fig, axes = plt.subplots(1, 5, figsize=(15, 3))
    plt.tight_layout()

    ax = axes[0]
    ax.axis('off')
    ax.imshow(image)
    
    overlay = []
    for cluster_idx in range(cluster_assignments.max()+1):
        mask = (cluster_assignments_2d == cluster_idx)
        mask = cv2.resize(mask.astype(float),image.shape[1::-1], interpolation=cv2.INTER_NEAREST)
        # mask = (cv2.pyrUp(mask.astype(float),image.shape[:2]))
        overlay.append(mask)
    
    overlay = np.stack(overlay)
    display_instances(image, overlay, ax=axes[1],alpha=0.7)
    
   
    
    ## show the visible patches
    thresholds = [0.8,0.82,0.9] # sit
    # thresholds = np.linspace(0.2,0.3,3)
    for i in range(3):
        ax = axes[i+2]
        ax.axis('off')
        thr = thresholds[i]
        ax.set_title(thr)
        ax.imshow(image*(s[:,:,None]>thr))
    
    if args.output_img:
        outdir = os.path.dirname(args.output_img)
        os.makedirs(outdir,exist_ok=True)
        fig.savefig(args.output_img,pad_inches=0)
        pass
    else:
        import matplotlib.pyplot as plt
        plt.show()
        
        
        