#!python
#%%
"""
for img in img_list:
   %run bin/viz_clusters.py --arch vit_base --pretrained_weights ../SiT/outputs/imagenet/sit-ViT_B/checkpoint.pth --img ../data/ImageNet/val/{img} --cluster_number=10 --output_img=outputs/pics/clusters/{img}-sit-10.jpg
"""
from PIL import Image
import time
import argparse
import cv2
from sklearn.cluster import KMeans
import torch,math,os,torchvision,random, einops
from torch import nn
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import torch.nn.functional as F
import torchvision.transforms as tfms
import torchvision.transforms.functional as tfF
from torchvision.utils import *
from vitookit.utils.helper import load_pretrained_weights
import time
import vitookit.models

from torchvision import datasets

from misc.visualize import show_mask, show_points

def get_args(*args,):
    parser = argparse.ArgumentParser(add_help="--arch=vit_base --pretrained_weight <model>.pth")
    parser.add_argument('--patch_size', default=16, type=int, help='Patch resolution of the model.')
    parser.add_argument("--image_size", default=(480, 480), type=int, nargs="+", help="Resize image.")
    parser.add_argument('--arch', default='vit_small', type=str, help='Architecture.')
    parser.add_argument('-w', '--pretrained_weights', default='', type=str, help="""Path to pretrained 
        weights to evaluate. Set to `download` to automatically load the pretrained DINO from url.
        Otherwise the model is randomly initialized""")
    parser.add_argument("--checkpoint_key", default=None, type=str,
        help='Key to use in the checkpoint (example: "teacher")')
    
    parser.add_argument('--mode',default='ins',type=str,choices=['ins','class'])
    parser.add_argument('--threshold',type=float,help="select the features over the threshold as positive.")
    
    parser.add_argument("--sample_idx",default=0,type=int,help="the index of the sample to visualize.")
    
    parser.add_argument(
        '--output_dir',
        type=str,
        default='',
        help='output image path')
    
    parser.add_argument('--use-cuda', action='store_true', default=True,
                        help='Use NVIDIA GPU acceleration')
    
    args = parser.parse_args(*args)
    
    args.use_cuda = args.use_cuda and torch.cuda.is_available()
    if args.use_cuda:
        print('Using GPU for acceleration')
    else:
        print('Using CPU for computation')
        
    return args

from pycocotools import mask as maskUtils


def annToRLE(ann, height, width):
    """
    Convert annotation which can be polygons, uncompressed RLE to RLE.
    :return: binary mask (numpy 2D array)
    """
    segm = ann['segmentation']
    if isinstance(segm, list):
        # polygon -- a single object might consist of multiple parts
        # we merge all parts into one mask rle code
        rles = maskUtils.frPyObjects(segm, height, width)
        rle = maskUtils.merge(rles)
    elif isinstance(segm['counts'], list):
        # uncompressed RLE
        rle = maskUtils.frPyObjects(segm, height, width)
    else:
        # rle
        rle = ann['segmentation']
    return rle

def annToMask( ann, height, width):
    """
    Convert annotation which can be polygons, uncompressed RLE, or RLE to binary mask.
    :return: binary mask (numpy 2D array)
    """
    rle = annToRLE(ann, height, width)
    m = maskUtils.decode(rle)
    return m

class COCO(datasets.CocoDetection):
    def __getitem__(self, index: int):
        img,annotations= super().__getitem__(index)
        
        width,height = img.size
        
        instance_masks = [np.zeros((hw,nh))]
        class_masks = [np.zeros((hw,nh))]
        ins_set = set()
        class_set = set()
        for annotation in annotations:
            m = annToMask(annotation, height,
                                width)
            m = cv2.resize(m,(hw,nh))
            
            ins_id = annotation['id']
            class_id = annotation['category_id']
            # The  id  field is a unique identifier assigned to each individual object in the dataset. 
            # On the other hand,  category_id  is the identifier for the category or class that the object belongs to.
            
            instance_masks.append(m*ins_id)
            class_masks.append(m*class_id)
            ins_set.add(ins_id)
            class_set.add(class_id)
        
        # merge the masks
        instance_mask = np.stack(instance_masks).max(0)
        class_mask = np.stack(class_masks).max(0)
        
        return tfms(img), instance_mask.astype(np.int32), class_mask


import numpy as np
min_object = 20
def get_density_histograms(p_sim, mask, bin_edges, hist_intra, hist_inter):
    """
    Computes histograms of intra- and inter-class density values from input similarity data.
    Parameters:
        p_sim (numpy array): similarity data of shape (N, N), where N is the number of instances
        mask (numpy array): mask indicating class labels of each instance
        bin_edges (numpy array): bin edges for histogram computation
    """
    # Get unique values from the mask
    tset = np.unique(mask)
    for mask_id in tset:
        if mask_id == 0:
            continue
        label = mask == mask_id
        # skip small instances
        if label.sum() < min_object:  continue
        l1 = label.reshape(N, 1)
        l2 = label.reshape(1, N)
        l_intra = l1 & l2
        l_inter = l1 & (~l2)
         # Calculate the histogram for the intra-mask elements
        hist, _ = np.histogram(p_sim[l_intra], bin_edges)
        hist_intra += hist
        hist, _ = np.histogram(p_sim[l_inter], bin_edges)
        hist_inter += hist
     # Return the calculated histograms
    return  hist_intra, hist_inter
        
if __name__ == '__main__':
    import sys
    
    args = get_args()
    
    from torchvision import datasets, transforms
    IMAGENET_DEFAULT_MEAN = (0.485, 0.456, 0.406)
    IMAGENET_DEFAULT_STD = (0.229, 0.224, 0.225)
    
    common_tf = transforms.Compose([
        # transforms.CenterCrop(args.image_size),
        transforms.Resize(args.image_size),
        ])
    
    tfms=transforms.Compose([
        common_tf,
        transforms.ToTensor(),
        transforms.Normalize(IMAGENET_DEFAULT_MEAN, IMAGENET_DEFAULT_STD),
    ])
    def denormalize(imagesToPrint):
        imagesToPrint = imagesToPrint.clone()
        imagesToPrint *= torch.Tensor(IMAGENET_DEFAULT_STD,device=imagesToPrint.device).reshape(3,1,1)
        imagesToPrint += torch.Tensor(IMAGENET_DEFAULT_MEAN,device=imagesToPrint.device).reshape(3,1,1)
        return imagesToPrint.clamp(0,1)
    
    data = COCO('/vol/research/datasets/still/MSCOCO/images/train2017','/vol/research/datasets/still/MSCOCO/annotations/instances_train2017.json',)
    #%% ## get representations
    ## load model
    if 'sam' in args.arch:
        import vitookit.models.image_encoder as models
        args.image_size=(1024,1024)
        model = models.__dict__[args.arch](image_size=args.image_size[0] )
        state = torch.load(args.pretrained_weights,"cpu")
        state = {k.replace("image_encoder.","") :v for k,v in state.items() if "image_encoder" in k }
        print(model.load_state_dict(state))
    else:
        model = models.__dict__[args.arch](
            patch_size=args.patch_size, 
            num_classes=0)
        load_pretrained_weights(model, args.pretrained_weights, args.checkpoint_key)
    
    model.eval().requires_grad_(False)
    
    nh,hw = args.image_size[0]//args.patch_size, args.image_size[1]//args.patch_size
       
    
    #%% load 
    import tqdm
    model.cuda()
    
    hist_dict= {'inter_density_class':np.zeros(20),
                'intra_density_class':np.zeros(20),
                'inter_density_ins':np.zeros(20),
                'intra_density_ins':np.zeros(20),
                }
    
    
    sample_idx = args.sample_idx 
    # sample_idx = 46073 # TODO: remove
    # args.threshold = 0.2
    if args.sample_idx < len(data):
        x, instance_mask, class_mask = data[sample_idx]
        
        x = x[None,:] # B, C, H, W
        instance_mask, class_mask = instance_mask[None,:], class_mask[None,:]
        
        ## calculate similarity
        if 'sam' in args.arch:
            patch_tokens = model.forward_features(x.cuda())
        else:
            patch_tokens = model.forward_features(x.cuda())[:,1:] # B, N - 1, C
        ## remove normalize
        # patch_tokens_mean = patch_tokens.mean(1,keepdim=True)
        # patch_tokens_std = patch_tokens.std(1,keepdim=True)
        # patch_tokens = (patch_tokens - patch_tokens_mean) / patch_tokens_std
        
        patch_tokens = nn.functional.normalize(patch_tokens, dim=-1, p=2)
        
        p_sim = patch_tokens.matmul(patch_tokens.transpose(2,1)).cpu() # B, N, N
        B,N,_ = p_sim.shape
        
        bin_edges = np.linspace(p_sim.min(),0.99,21)
        for i in range(B):
            ## hist similarity for class-wise objects      

            masks = class_mask[i].flatten() # N
            get_density_histograms(p_sim[i],masks,bin_edges, 
                                    hist_dict['intra_density_class'],hist_dict['inter_density_class'])
            
            ## hist similarity for instance-wise objects
            masks = instance_mask[i].flatten() # N
            get_density_histograms(p_sim[i],masks,bin_edges, 
                                    hist_dict['intra_density_ins'],hist_dict['inter_density_ins'])
            
    
    #1%% #################### visualization ########################
    # Create a figure with subplots for each cluster
    plt.style.use('seaborn')
    fig,axes = plt.subplots(2,2,figsize=(6.4,6.4))
    axes = axes.flatten()
    bin_centers = (bin_edges[:-1] + bin_edges[1:]) / 2
    step = -(bin_edges[:-1] - bin_edges[1:]).mean()
    # Plot the PDF
    span = (bin_edges.max()-bin_edges.min())
    for k,v in hist_dict.items():
        hist_dict[k] = v.astype(np.float64)/v.sum()/step
    
    overlap_ins = np.stack([hist_dict['intra_density_ins'],hist_dict['inter_density_ins']]).min(0)
            
    overlap_class = np.stack([hist_dict['intra_density_class'],hist_dict['inter_density_class']]).min(0)
            
    ax=axes[0]
    ax.set_title(f'class, T={args.threshold}, O={overlap_class.sum()*step:.2f}')
    ax.set_xlabel('Cosine Similarity')
    ax.set_ylabel('Probability Density')
    ax.plot(bin_centers, hist_dict['inter_density_class'],label='inter')
    ax.plot(bin_centers, hist_dict['intra_density_class'],label='intra')
    ax.legend()
    
    ax=axes[1]
    ax.set_title(f'instance, T={args.threshold}, O={overlap_ins.sum()*step:.2f}')
    
    ax.set_xlabel('Cosine Similarity')
    # ax.set_ylabel('Probability Density')
    ax.plot(bin_centers, hist_dict['inter_density_ins'],label='inter')
    ax.plot(bin_centers, hist_dict['intra_density_ins'],label='intra')
    ax.legend()
    
    
    ########show the original image ######
    ax=axes[2]
    ax.axis('off')
    ax.imshow(denormalize(x[0]).permute(1,2,0),alpha=0.5)
    
    if args.mode=="class":
        masks = class_mask[i].flatten() # N
    elif args.mode=="ins":
        masks = instance_mask[i].flatten() # N
    id_set = np.unique(masks)
    
    mask_choices=[]
    for mask_id in id_set:
        if mask_id==0:continue
        indices = np.where(masks==mask_id)[0]   
        if len(indices)<min_object:continue
        
        mask_choices.append(mask_id)
    
    ####### calculate mIoU ########
    i = 0
    # hist similarity for class-wise objects   

    mask_id = np.random.choice(mask_choices, size=1)[0]    
    mask = masks==mask_id
    indices = np.where(mask)[0]
    random_indices = np.random.choice(len(indices), size=1, replace=True)
    # the index of query feature
    q_idx = indices[random_indices[0]] 
    # q_idx = 20*30+20 # TODO: debug
    
    
    predict_mask = np.zeros_like(mask)
    predict_mask[p_sim[i,q_idx]>args.threshold]=1

    # Calculate the mIoU
    intersection  = (predict_mask & mask) 
    union = (predict_mask| mask)
    mIoU = intersection.sum() / union.sum()
    
    print("mIoU: ",mIoU, "intersection:",intersection.sum(), "union:",union.sum(), "predict_mask:",predict_mask.sum(), "mask:",mask.sum())
    
    # show the selected patch
    ax.imshow(cv2.resize((mask).reshape(nh,hw).astype(float),args.image_size,0)[:,:,None]* np.array([0, 0, 1]).reshape(1, 1, -1) ,alpha=0.5)
    ax.imshow(cv2.resize((predict_mask).reshape(nh,hw).astype(float),args.image_size,0)[:,:,None]* np.array([1, 0, 0]).reshape(1, 1, -1),alpha=0.5)
    
    
    select_points = args.patch_size*np.array([[ q_idx%hw, q_idx//hw]])
    print("select_points:",select_points, "q_idx:",q_idx, )
    show_points(select_points,np.array([1]), ax=ax,marker_size=60)
    ################# show the predicted similarity #################
    ax = axes[3]
    ax.axis('off')
    
    predicted_sim = p_sim[i,q_idx].reshape(nh,hw)
    X = np.linspace(0, hw-1, hw)
    Y = np.linspace(0, nh-1, nh)
    X, Y = np.meshgrid(X,Y)
    
    contours = ax.contour(X, Y, predicted_sim, 5,colors='black',vmax=1)
    plt.clabel(contours, inline=True, fontsize=8,colors='white')
    cm = ax.imshow(predicted_sim,cmap='viridis',vmax=1)
    plt.colorbar(cm,ax=ax)
    #%%
    
    # Distribution of similarity between inter-object patches vs. intra-object patches
    if args.output_dir:
        
        os.makedirs(args.output_dir,exist_ok=True)
        fig.savefig(os.path.join(args.output_dir,'sim_dist.pdf'),pad_inches=0)
        np.save(os.path.join(args.output_dir,'sim_dist'),hist_dict,allow_pickle=True)
        pass
    else:
        import matplotlib.pyplot as plt
        plt.show()
        

# %% show instance mask and class mask
# x, instance_mask, class_mask = data[45]

# masks = instance_mask
# mask_set = np.unique(masks)[1:]
# N = len(mask_set)
# fig,axes = plt.subplots(1,N,squeeze=False,figsize=(N*2,2))
# plt.tight_layout()
# for ax,id in zip(axes.flatten(),mask_set):
#     ax.axis('off')
#     show_img = cv2.resize((masks==id)*1.0,args.image_size)[None,:]
#     ax.imshow(tfF.to_pil_image((x.cpu()/5+0.45)*show_img))
#     print("id:",id, "mask:",(masks==id).sum())
# # %%
# for threshold in np.linspace(-0.1,0.,9):
#     predict_mask = np.zeros_like(mask)
#     predict_mask[p_sim[i,q_idx]>threshold]=1

#     # Calculate the mIoU
#     intersection  = (predict_mask & mask) 
#     union = (predict_mask| mask)
#     mIoU = intersection.sum() / union.sum()
#     print(threshold, mIoU)
# # %%
