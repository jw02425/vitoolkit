from PIL import Image
import argparse
from ffcv import Loader
from ffcv.loader import OrderOption
from ffcv.transforms import ToTensor, ToDevice, ToTorchImage, NormalizeImage,RandomHorizontalFlip, View
from ffcv.fields.decoders import CenterCropRGBImageDecoder, IntDecoder
from torchvision.utils import save_image
import numpy as np
import time
from tqdm import tqdm


IMAGENET_MEAN = np.array([0.485, 0.456, 0.406]) * 255
IMAGENET_STD = np.array([0.229, 0.224, 0.225]) * 255

parser = argparse.ArgumentParser()
parser.add_argument('--data_path', type=str, default='data/train.ffcv')
parser.add_argument('--num_samples', type=int, default=64)
parser.add_argument('--output_path', type=str, default='data.png')
parser.add_argument('--img_size', type=int, default=224)    

args = parser.parse_args()
image_pipeline = [
        CenterCropRGBImageDecoder((args.img_size, args.img_size), 224/256),
        ToTensor(),       
        ToTorchImage(),
        ]
label_pipeline = [IntDecoder(), ToTensor(),View(-1)]
# Pipeline for each data field
pipelines = {
    'image': image_pipeline,
    'label': label_pipeline,
} 
loader = Loader(args.data_path, pipelines=pipelines,
                batch_size=args.num_samples, num_workers=10, 
                        )

start = time.time()
for batch in tqdm(loader):
    pass
print('Time taken', time.time()-start, 's to load', len(loader.indices), 'samples')

nrow=int(np.sqrt(args.num_samples))
for batch in loader:
    x, y  = batch
    x = x.float()/255
    print(x.shape, x.dtype)
    y = y.reshape(nrow,-1)
    print('label', y)
    break

save_image(x,args.output_path,nrow=nrow)