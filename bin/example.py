#!python
import time
import cv2
import argparse
from sklearn.cluster import KMeans
import torch,math,os,torchvision,random, einops
from torch import nn
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import torch.nn.functional as F
import torchvision.transforms as tfms
import torchvision.transforms.functional as tfF
from torchvision.utils import *
from vitookit.utils.helper import load_pretrained_weights
import time
import vitookit.models
from analyze.attention_map.visualize_attention import show_attn_color, show_attn

def get_args(*args,):
    parser = argparse.ArgumentParser('visualize the Attention Map')
    parser.add_argument('--patch_size', default=16, type=int, help='Patch resolution of the model.')
    parser.add_argument('--arch', default='vit_small', type=str, help='Architecture.')
    parser.add_argument('-w', '--pretrained_weights', default='', type=str, help="""Path to pretrained 
        weights to evaluate. Set to `download` to automatically load the pretrained DINO from url.
        Otherwise the model is randomly initialized""")
    parser.add_argument("--checkpoint_key", default=None, type=str,
        help='Key to use in the checkpoint (example: "teacher")')

    parser.add_argument(
        '--img',
        type=str,
        default='./imgs/sample1.JPEG',
        help='Input image path')
    
    parser.add_argument(
        '--output_img',
        type=str,
        default='',
        help='output image path')
    
    
    parser.add_argument('--use-cuda', action='store_true', default=True,
                        help='Use NVIDIA GPU acceleration')
    
    args = parser.parse_args(*args)
    
    args.use_cuda = args.use_cuda and torch.cuda.is_available()
    if args.use_cuda:
        print('Using GPU for acceleration')
    else:
        print('Using CPU for computation')
        
    return args

if __name__ == '__main__':
    
    args = get_args()
    ## load model
    model = models.__dict__[args.arch](
        patch_size=args.patch_size, 
        num_classes=0)
    embed_dim = model.embed_dim
    model.eval()
    
    load_pretrained_weights(model, args.pretrained_weights, args.checkpoint_key)
    ## load data
    from PIL import Image
    img = Image.open(args.img)
    
    from torchvision import datasets, transforms
    IMAGENET_DEFAULT_MEAN = (0.485, 0.456, 0.406)
    IMAGENET_DEFAULT_STD = (0.229, 0.224, 0.225)
    tfms=transforms.Compose([
        transforms.Resize(224),
        transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize(IMAGENET_DEFAULT_MEAN, IMAGENET_DEFAULT_STD),
    ])
    def denormalize(imagesToPrint):
        imagesToPrint = imagesToPrint.clone()
        imagesToPrint *= torch.Tensor(IMAGENET_DEFAULT_STD,device=imagesToPrint.device).reshape(3,1,1)
        imagesToPrint += torch.Tensor(IMAGENET_DEFAULT_MEAN,device=imagesToPrint.device).reshape(3,1,1)
        return imagesToPrint.clamp(0,1)


    ## get representations
    img = tfms(img)
    x = img[None,:]
    
    show_img
    if args.output_img:
        cv2.imwrite(f'{args.output_img}.jpg', show_img)
    else:
        import matplotlib.pyplot as plt
        plt.imshow(show_img)
        plt.show()