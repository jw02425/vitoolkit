####################
#
# Example Job for HTCondor
#
####################

#---------------------------------------------
# Name your batch so it's easy to distinguish in the q.
JobBatchName = "Eval seg $(model_dir)"
getenv = WANDB*
notify_user = jw02425@surrey.ac.uk
max_transfer_input_mb = -1
max_retries = 2
# --------------------------------------------
# Executable

requirements = HasStornext  && (CUDAClockMhz>1530) && (machine!="creative01.eps.surrey.ac.uk") && (machine !="bofur.eps.surrey.ac.uk") && (machine !="cogvis3.eps.surrey.ac.uk")
#&& ( machine != "aisurrey05.surrey.ac.uk")

# -----------------------------------
# File Transfer, Input, Output
should_transfer_files = YES
# preserve_relative_paths = true
transfer_input_files = models, datasets, evaluation, utils, $(model_dir)/checkpoint.pth
# transfer_output_files = outputs

# ---------------------------------------------------
# Universe (vanilla, docker)
universe = vanilla
# universe     = docker
# docker_image = nvidia/cuda:10.1-cudnn7-runtime-ubuntu16.04

# -------------------------------------------------
# Event, out and error logs
log    = outputs/condor_log/c$(cluster).$(process).log
output = outputs/condor_log/c$(cluster).$(process).out
error  = outputs/condor_log/c$(cluster).$(process).error

Rank = CUDAClockMhz * CUDAComputeUnits

# --------------------------------------
# Resources
request_GPUs   = $(nproc_per_node)
# this needs to be specified for the AI@Surrey cluster if requesting a GPU
+GPUMem          = 10000  
request_CPUs   = 4
request_memory = 64G

#This job will complete in less than 1 hour
+JobRunTime = 20

#This job can checkpoint
+CanCheckpoint = false

# ------------------------------------
# Request for guaruanteed run time. 0 means job is happy to checkpoint and move at any time.
# This lets Condor remove our job ASAP if a machine needs rebooting. Useful when we can checkpoint and restore
# Measured in seconds, so it can be changed to match the time it takes for an epoch to run
MaxJobRetirementTime = 0

# -----------------------------------
# Queue commands. We can use variables and flags to launch our command with multiple options (as you would from the command line)
nproc_per_node=4

model_name = $Fn(model_dir)-$(data_set)


output_dir=$ENV(PWD)/$(model_dir)/eval_seg/$(metric)-$(data_set)

executable  = /vol/research/facer2vm_agfr/people/gent/miniconda3/envs/pytorch/bin/python
arguments =  -m torch.distributed.launch \
    --nproc_per_node=$(nproc_per_node) evaluation/semantic_segmentation/train.py \
    evaluation/semantic_segmentation/configs/linear/vit_base_512_$(data_set).py \
    --launcher pytorch  --work_dir $(output_dir) --auto_resume --options model.backbone.pretrained=checkpoint.pth model.backbone.out_with_norm=True data.train.data_root=$(data_location) data.val.data_root=$(data_location) data.test.data_root=$(data_location) data.samples_per_gpu=4 model.backbone.frozen_stages=12

environment = "mount=/vol/research/facer2vm_agfr/people,/vol/research/fmodel_ssl/people PYTHONPATH=evaluation/semantic_segmentation:. "
#########################################   linprob  ################################################
metric=linprob
##################### ade20k #########################
data_set=ade20k_40k
data_location=/vol/research/fmodel_ssl/people/gent/data/ADEChallengeData2016
queue 1

#################### VOC #########################
data_set=voc12aug_40k
data_location=/vol/research/fmodel_ssl/people/gent/data/VOCdevkit/VOC2012
queue 1

# ##################### cityscapes #########################
data_set=cityscapes_40k
data_location=/vol/research/fmodel_ssl/people/gent/data/cityscapes
queue 1

########################################## finetune  ###############################################
metric=finetune
arguments =  -m torch.distributed.launch \
    --nproc_per_node=$(nproc_per_node) evaluation/semantic_segmentation/train.py\
     evaluation/semantic_segmentation/configs/upernet/vit_base_512_$(data_set).py \
    --launcher pytorch  --work_dir $(output_dir) --auto_resume --options model.backbone.pretrained=checkpoint.pth data.train.data_root=$(data_location) data.val.data_root=$(data_location) data.test.data_root=$(data_location) data.samples_per_gpu=4

#################### ade20k #########################
# data_set=ade20k_160k
# data_location=/vol/research/fmodel_ssl/people/gent/data/ADEChallengeData2016
# queue 1

# ################## VOC #########################
# data_set=voc12aug_40k
# data_location=/vol/research/fmodel_ssl/people/gent/data/VOCdevkit/VOC2012
# queue 1

# #################### cityscapes #########################
# data_set=cityscapes_40k
# data_location=/vol/research/fmodel_ssl/people/gent/data/cityscapes
# queue 1



######################################## ade20k ablation ###################################################
data_set=ade20k_40k
data_location=/vol/research/fmodel_ssl/people/gent/data/ADEChallengeData2016
arguments =  -m torch.distributed.launch \
    --nproc_per_node=$(nproc_per_node) \
    evaluation/semantic_segmentation/train.py evaluation/semantic_segmentation/configs/upernet/ \
    --launcher pytorch  --work_dir $(output_dir) --auto_resume --options model.backbone.pretrained=checkpoint.pth data.train.data_root=$(data_location) data.val.data_root=$(data_location) data.test.data_root=$(data_location) data.samples_per_gpu=2 model.backbone.frozen_stages=12 evaluation.interval=60000

# output_dir=$ENV(PWD)/$(model_dir)/eval_mmseg/linprob-$(data_set)-$(abl)
# queue abl in (ab1,ab2,ab3)