####################
#
# Example Job for HTCondor
#
####################

#---------------------------------------------
# Name your batch so it's easy to distinguish in the q.
JobBatchName = "Eval lin. $(model_dir)"
getenv = WANDB*
notify_user = jw02425@surrey.ac.uk
max_transfer_input_mb = -1
max_retries = 2
# --------------------------------------------
# Executable
executable  = /mnt/fast/nobackup/users/jw02425/env/pytorch/bin/torchrun
requirements = HasWeka

# -----------------------------------
# File Transfer, Input, Output
should_transfer_files = YES
# preserve_relative_paths = true
transfer_input_files = models, datasets,evaluation, utils, $(model_dir)/checkpoint.pth
# transfer_output_files = outputs

environment = "PYTHONPATH=. WANDB_NAME=$(model_name)"
# ---------------------------------------------------
# Universe (vanilla, docker)
universe = vanilla
# universe     = docker
# docker_image = nvidia/cuda:10.1-cudnn7-runtime-ubuntu16.04

# -------------------------------------------------
# Event, out and error logs
log    = outputs/condor_log/c$(cluster).$(process).log
output = outputs/condor_log/c$(cluster).$(process).out
error  = outputs/condor_log/c$(cluster).$(process).error

Rank = CUDAClockMhz * CUDAComputeUnits

# --------------------------------------
# Resources
request_GPUs   = $(nproc_per_node)
# this needs to be specified for the AI@Surrey cluster if requesting a GPU
+GPUMem          = 10000  
request_CPUs   = 4
request_memory = 64G

#This job will complete in less than 1 hour
+JobRunTime = 20

#This job can checkpoint
+CanCheckpoint = false

# ------------------------------------
# Request for guaruanteed run time. 0 means job is happy to checkpoint and move at any time.
# This lets Condor remove our job ASAP if a machine needs rebooting. Useful when we can checkpoint and restore
# Measured in seconds, so it can be changed to match the time it takes for an epoch to run
MaxJobRetirementTime = 0

# -----------------------------------
# Queue commands. We can use variables and flags to launch our command with multiple options (as you would from the command line)
nproc_per_node=2

### linear prob
metric=linprob
model_name = $Fn(model_dir)-$(metric)-$(data_set)
##################### Downstream tasks #########################

output_dir=/mnt/fast/nobackup/scratch4weeks/jw02425/vitoolkit/$(model_dir)/eval/$(metric)-$(head_type)-$(data_set)
data_location=/mnt/fast/nobackup/users/jw02425/data
epochs=500

head_type=0
arguments = --nproc_per_node $(nproc_per_node) evaluation/eval_linear.py  --data_location=$(data_location) --data_set=$(data_set) --output_dir=$(output_dir) --pretrained_weights=checkpoint.pth --arch=$(ARCH) --epochs=$(epochs) --batch_size 256 --head_type=$(head_type) --model_name=$(model_name) $(extra_args) --blr=1e-3

# queue data_set in (Aircraft,CIFAR10,CIFAR100,Flowers,Cars,Pets)

##################### ImageNet100 #########################
# output_dir=/mnt/fast/nobackup/scratch4weeks/jw02425/SiT/$(model_dir)/eval/$(metric)-$(head_type)-100
# model_name = $Fn(model_dir)-$(metric)-100
# data_location=/mnt/fast/nobackup/users/jw02425/data/ImageNet100
# data_set = ImageFolder
# epochs = 100

# queue 1
# queue head_type in (0,1)

##################### ImageNet #########################
# output_dir=/mnt/fast/nobackup/users/jw02425/ViTookit/$(model_dir)/eval/$(metric)-$(head_type)-1K
# model_name = $Fn(model_dir)-$(metric)-1K
# data_location=/mnt/fast/nobackup/users/jw02425/data/ImageNet
# data_set = ImageFolder
# epochs = 50

# queue 1

# WANDB_PROJECT=CSSL-evaluation condor_submit condor/eval_weka_cls.submit ARCH=vit_small head_type=2 model_dir=../SiT/outputs/imagenet100/ablation200/CSSL-1-0-0
queue model_dir in (\
    # study/EvaluatingSSL/outputs/base/deit,\
    # study/EvaluatingSSL/outputs/base/MoCo,\
    # study/EvaluatingSSL/outputs/base/iBOT,\
    study/EvaluatingSSL/outputs/base/MSN,\
    # study/EvaluatingSSL/outputs/base/MAE,\
    # study/EvaluatingSSL/outputs/base/DINO\
    )