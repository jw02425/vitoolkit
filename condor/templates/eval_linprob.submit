####################
#
# Example Job for HTCondor
#
####################

#---------------------------------------------
# Name your batch so it's easy to distinguish in the q.
JobBatchName = "Eval Linear Prob"

# --------------------------------------------
# Executable
# executable   = /bin/echo 
# executable   = /vol/research/facer2vm_agfr/people/gent/miniconda3/envs/pytorch/bin/torchrun
executable  = /opt/conda/bin/torchrun

# ---------------------------------------------------
# Universe (vanilla, docker)
# universe = vanilla
universe     = docker
# docker_image =  docker.io/erow/aisurrey:1.13.1-runtime
docker_image = container-registry.surrey.ac.uk/shared-containers/aisurrey:latest

# -------------------------------------------------
# Event, out and error logs

log    = outputs/condor_log/linprob.c$(cluster).log
output = outputs/condor_log/linprob.c$(cluster).out
error  = outputs/condor_log/linprob.c$(cluster).error

# -----------------------------------
# File Transfer, Input, Output
should_transfer_files = YES
preserve_relative_paths = True
transfer_input_files = models, datasets,tasks, configs, main.py,utils.py,  vision_transformer.py, site-packages, $(model_dir)/checkpoint.pth
# transfer_output_files = $(output_dir)

PWD=.
# Mount the project spaces containing the Anaconda environments and the code


# -------------------------------------
# Requirements for the Job 
# CUDAClockMhz
# 1815	 Quadro RTX 5000
# 1695	 GeForce RTX 3090
# 1620	 GeForce GTX 1080 Ti
# 1545	 GeForce RTX 2080 Ti
# 1531	 TITAN X (Pascal)
# requirements = ( HasStornext && \
#             ((machine == "creative01.eps.surrey.ac.uk") ||\
#             (machine == "cogvis2.eps.surrey.ac.uk") ||\
#             (machine == "fili.eps.surrey.ac.uk") ||\
#             (machine == "sounds01.eps.surrey.ac.uk") ||\
#             (machine == "ori.eps.surrey.ac.uk")))
# blacklist
requirements = ((machine != "creative01.eps.surrey.ac.uk")) && HasWeka
Rank = CUDAClockMhz * CUDAComputeUnits

# --------------------------------------
# Resources
request_GPUs   = $(nproc_per_node)
# this needs to be specified for the AI@Surrey cluster if requesting a GPU
+GPUMem          = 10000  
request_CPUs   = 10


#This job will complete in less than 1 hour
+JobRunTime = 2

#This job can checkpoint
+CanCheckpoint = false

# ------------------------------------
# Request for guaruanteed run time. 0 means job is happy to checkpoint and move at any time.
# This lets Condor remove our job ASAP if a machine needs rebooting. Useful when we can checkpoint and restore
# Measured in seconds, so it can be changed to match the time it takes for an epoch to run
MaxJobRetirementTime = 0

# -----------------------------------
# Queue commands. We can use variables and flags to launch our command with multiple options (as you would from the command line)
nproc_per_node=2


environment = "PYTHONPATH=$(PWD):site-packages"

# model
# model_dir=outputs/imagenet100/CSSL-0-0.2-0
# queue model_dir matching (
#     outputs/gpu2/*
# )


#########################Hyper parameters #########################
# request_memory = 48G
# blr = 0.0005
# batch_size = 128
# queue data_set in (Pets, CIFAR10,CIFAR100,Cars,Flowers,Aircraft)

# blr = 0.01
# batch_size = 128
# queue data_set in (Pets, CIFAR10,CIFAR100,Cars,Flowers,Aircraft)

# # request_memory = 32G
# blr = 0.001
# batch_size = 512
# queue data_set in (Pets, CIFAR10,CIFAR100,Cars,Flowers,Aircraft)

######################### eval #########################
arguments = --nproc_per_node $(nproc_per_node) tasks/linearprob.py  --data_location=$(data_location) --data_set=$(data_set) --output_dir=$(output_dir) --epochs=200 --pretrained_weights=$(model_dir)/checkpoint.pth --blr=$(blr)  --model=vit_small

output_dir=/mnt/fast/nobackup/users/jw02425/SiT/$(model_dir)/eval/linprob-$(data_set)
blr = 0.01

request_memory=16G

############################### Ablation Done #########################
# model_dir=outputs/imagenet100/ablation/CSSL-1-0.2-0.2
# queue data_set in (Pets, CIFAR10,CIFAR100,Cars,Flowers,Aircraft)

# model_dir=outputs/imagenet100/ablation/CSSL-0-0.2-0.2
# queue data_set in (Pets, CIFAR10,CIFAR100,Cars,Flowers,Aircraft)

# model_dir=outputs/imagenet100/ablation/CSSL-0-0.2-0
# queue data_set in (Pets, CIFAR10,CIFAR100,Cars,Flowers,Aircraft)

############################### Compared methods ###############################
# model_dir=outputs/imagenet100/mae-500
# queue data_set in (Pets, CIFAR10,CIFAR100,Cars,Flowers,Aircraft)

# model_dir=outputs/imagenet100/ibot-100
# queue data_set in (Pets, CIFAR10,CIFAR100,Cars,Flowers,Aircraft)

# model_dir=outputs/imagenet100/ibot-300
# queue data_set in (Pets, CIFAR10,CIFAR100,Cars,Flowers,Aircraft)

# model_dir=outputs/imagenet100/ibot-500
# queue data_set in (Pets, CIFAR10,CIFAR100,Cars,Flowers,Aircraft)

# model_dir=outputs/imagenet100/cssl-100
# queue data_set in (Pets, CIFAR10,CIFAR100,Cars,Flowers,Aircraft)

# model_dir=outputs/imagenet100/cssl-300
# queue data_set in (Pets, CIFAR10,CIFAR100,Cars,Flowers,Aircraft)
# model_dir=outputs/imagenet100/ablation/CSSL-0-0.2-0
# queue data_set in (Pets, CIFAR10,CIFAR100,Cars,Flowers,Aircraft)

model_dir=outputs/imagenet100/dino-500
queue data_set in (Flowers,Aircraft)

data_set=ImageFolder
data_location=/mnt/fast/nobackup/users/jw02425/data/ImageNet100

queue model_dir in (outputs/imagenet100/dino-500,outputs/imagenet100/ibot-500,outputs/imagenet100/cssl-500,outputs/imagenet100/ablation/CSSL-1-1-1)

# ablation
queue model_dir in (outputs/imagenet100/ablation200/CSSL-0-0-1, outputs/imagenet100/ablation200/CSSL-1-0-0, outputs/imagenet100/ablation200/CSSL-5-1-1)

# hyper-parameter

queue model_dir in (outputs/imagenet100/ablation100/CSSL-0-0.2-0.2, outputs/imagenet100/ablation100/CSSL-0.1-0.2-0.2, outputs/imagenet100/ablation100/CSSL-0.2-0.2-0.2, outputs/imagenet100/ablation100/CSSL-0.5-0.2-0.2, outputs/imagenet100/ablation100/CSSL-1-0.2-0.2)