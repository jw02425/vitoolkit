####################
#
# Example Job for HTCondor
#
####################

#---------------------------------------------
# Name your batch so it's easy to distinguish in the q.
JobBatchName = "mmdet"
+CanCheckpoint=true

# --------------------------------------------
# Executable
executable  = /bin/bash

# ---------------------------------------------------
# Universe (vanilla, docker)
# universe = vanilla
universe     = docker
docker_image =  docker.io/erow/aisurrey:1.13.1-runtime
# docker_image = container-registry.surrey.ac.uk/shared-containers/aisurrey:latest

# -------------------------------------------------
# Event, out and error logs

log    = outputs/condor_log/mmdet.c$(cluster).log
output = outputs/condor_log/mmdet.c$(cluster).out
error  = outputs/condor_log/mmdet.c$(cluster).error

# -----------------------------------
# File Transfer, Input, Output
should_transfer_files = YES
# preserve_relative_paths = True
transfer_input_files = models, datasets,tasks, configs, main.py,utils.py,  vision_transformer.py, mmcv_custom, site-packages, $(model_dir)/checkpoint.pth, data/VOCdevkit
transfer_output_files = outputs

PWD=.

environment = "PYTHONPATH=$(PWD):site-packages"
# -------------------------------------
# Requirements for the Job 
# CUDAClockMhz
# 1815	 Quadro RTX 5000
# 1695	 GeForce RTX 3090
# 1620	 GeForce GTX 1080 Ti
# 1545	 GeForce RTX 2080 Ti
# 1531	 TITAN X (Pascal)
# requirements = HasStornext
# requirements = HasWeka

Rank = CUDAClockMhz * CUDAComputeUnits

# --------------------------------------
# Resources
request_GPUs   = $(nproc_per_node)
# this needs to be specified for the AI@Surrey cluster if requesting a GPU
+GPUMem          = 10000  
request_CPUs   = 8
request_memory = 16G

#This job will complete in less than 1 hour
+JobRunTime = 20

#This job can checkpoint
+CanCheckpoint = false

# ------------------------------------
# Request for guaruanteed run time. 0 means job is happy to checkpoint and move at any time.
# This lets Condor remove our job ASAP if a machine needs rebooting. Useful when we can checkpoint and restore
# Measured in seconds, so it can be changed to match the time it takes for an epoch to run
MaxJobRetirementTime = 2

# -----------------------------------
# Queue commands. We can use variables and flags to launch our command with multiple options (as you would from the command line)
nproc_per_node=2

data_set=PASCAL
output_dir=$(model_dir)/eval/det-$(data_set)

arguments = tasks/mmdet/tools/dist_train.sh configs/cascade_rcnn/vit_small_giou_4conv1f_voc_80k.py $(nproc_per_node) --work-dir $(output_dir) \
    --cfg-options   model.pretrained=checkpoint.pth data.samples_per_gpu=2 \
    data.train.data_root=VOCdevkit data.val.data_root=VOCdevkit data.test.data_root=VOCdevkit

############################### Compared methods ###############################

# model_dir=outputs/imagenet100/mae-500
# 19 hours
queue  model_dir in ( outputs/imagenet100/cssl-500, outputs/imagenet100/mae-500, outputs/imagenet100/ibot-500, outputs/imagenet100/dino-500)
# queue 1 
